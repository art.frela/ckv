.ONESHELL:
.DELETE_ON_ERROR:

APP=ckv

# h - help
h help:
	@echo "h help	- this help"
	@echo "bin	- build local binary"
	@echo "run	- runs local bin"

.PHONY: h

bin:
	go build -o ./bin/$(APP) ./cmd/$(APP)
	go build -o ./bin/cli ./cmd/cli
.PHONY: bin

run: bin
	CKV_CONFIG_FILE=./configs/ckv.yaml ./bin/$(APP)
.PHONY: run


logfile := file_$(shell date +%F_%H-%M-%S_%Z).log

lint:
	golangci-lint run ./...
.PHONY: lint

coverfile := cover_$(shell date +%F_%H-%M-%S_%Z).html

cover:
	go test $$(go list ./... | grep -v /mocks) -race -coverprofile .testCoverage.txt -covermode atomic
	go tool cover -func .testCoverage.txt -html=.testCoverage.txt -o ./tests/cover/all_$(coverfile)
.PHONY: cover

test:
	go test $$(go list ./... | grep -v /mocks) -race -coverprofile .testCoverage.txt -covermode atomic
	go tool cover -func .testCoverage.txt
.PHONY: test

mocks:
	mockery --config ./configs/.mockery.yaml
.PHONY: mocks