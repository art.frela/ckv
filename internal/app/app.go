package app

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/art.frela/ckv/internal/config"
	"gitlab.com/art.frela/ckv/internal/controller/tcp"
	"gitlab.com/art.frela/ckv/internal/usecase"
	"gitlab.com/art.frela/ckv/internal/usecase/compute"
	"gitlab.com/art.frela/ckv/internal/usecase/compute/analyzer"
	"gitlab.com/art.frela/ckv/internal/usecase/compute/parser"
	"gitlab.com/art.frela/ckv/internal/usecase/storage"
	"gitlab.com/art.frela/ckv/internal/usecase/storage/inmemory"
	"gitlab.com/art.frela/ckv/pkg/datasize"
	"gitlab.com/art.frela/ckv/pkg/dotconf"
	"go.uber.org/zap"
)

type App struct {
	tcpServer *tcp.Server
	shutdown  func() error
	log       *zap.SugaredLogger
}

func New(logger *zap.SugaredLogger, cfg *dotconf.DotConf) (*App, error) {
	if logger == nil {
		return nil, errors.New("miss logger")
	}

	if cfg == nil {
		return nil, errors.New("miss config")
	}

	compute, err := setupCompute(logger)
	if err != nil {
		return nil, fmt.Errorf("setupCompute: %w", err)
	}

	kv, err := setupStorage(cfg, logger)
	if err != nil {
		return nil, fmt.Errorf("setupStorage: %w", err)
	}

	uc := setupUsecase(compute, kv)

	srv, err := setupTCPServer(cfg, uc, logger)
	if err != nil {
		return nil, fmt.Errorf("setupTCPServer: %w", err)
	}

	return &App{tcpServer: srv, log: logger}, nil
}

func (a *App) Run(ctx context.Context) error {
	shutdown, err := a.tcpServer.Run(ctx)
	if err != nil {
		return fmt.Errorf("run tcp servr: %w", err)
	}

	a.shutdown = shutdown
	return nil
}

func (a *App) Stop() {
	if a.shutdown != nil {
		if err := a.shutdown(); err != nil {
			a.log.Errorf("shutdowm tcp server: %s", err)
		}
	}
}

func setupCompute(logger *zap.SugaredLogger) (*compute.Compute, error) {
	par, err := parser.New(logger)
	if err != nil {
		return nil, fmt.Errorf("new parser: %w", err)
	}

	analyz, err := analyzer.New(logger)
	if err != nil {
		return nil, fmt.Errorf("new analyzer: %w", err)
	}

	return compute.New(par, analyz)
}

func setupStorage(cfg *dotconf.DotConf, logger *zap.SugaredLogger) (usecase.Storage, error) {
	engine := inmemory.New()
	//nolint:revive,staticcheck
	if cfg.GetStringWithDefault("engine.kind", config.DefEngineKind) != config.DefEngineKind {
		// TODO: fill for other engines
	}

	storage, err := storage.NewStorage(
		storage.WithEngine(engine),
		storage.WithLogger(logger),
	)
	if err != nil {
		return nil, fmt.Errorf("new storage: %w", err)
	}

	return storage, nil
}

func setupUsecase(c *compute.Compute, repo usecase.Storage) usecase.Interface {
	return usecase.New(c, repo)
}

func setupTCPServer(cfg *dotconf.DotConf, uc usecase.Interface, logger *zap.SugaredLogger) (*tcp.Server, error) {
	size, err := datasize.Parse(cfg.GetStringWithDefault("network.max_message_size", config.DefMessageSize))
	if err != nil {
		return nil, fmt.Errorf("parse network.max_message_size: %w", err)
	}

	ops := []tcp.Option{
		tcp.WithLogger(logger),
		tcp.WithUC(uc),
		tcp.WithAddress(cfg.GetStringWithDefault("network.address", config.DefAddress)),
		tcp.WithMessageSize(size),
		tcp.WithMaxConnectionNumber(cfg.GetIntWithDefault("network.max_connections", config.DefMaxConn)),
		tcp.WithTimeout(cfg.GetDurationWithDefault("network.idle_timeout", config.DefTimeout)),
	}

	srv, err := tcp.NewServer(ops...)
	if err != nil {
		return nil, fmt.Errorf("new tcp server: %w", err)
	}

	return srv, nil
}
