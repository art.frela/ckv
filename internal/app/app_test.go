package app

import (
	"context"
	"os"
	"testing"
	"time"

	"gitlab.com/art.frela/ckv/internal/config"
	"gitlab.com/art.frela/ckv/pkg/dotconf"
	"go.uber.org/zap"
)

const testTimeout = time.Second * 30

func TestNew(t *testing.T) {
	cfg, err := config.Read("CKVTEST", "./testdata/config.toml")
	if err != nil {
		t.Fatalf("read config: %s", err)
	}

	tests := []struct {
		name    string
		cfg     *dotconf.DotConf
		logger  *zap.SugaredLogger
		envs    map[string]string
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			cfg:     cfg,
			logger:  zap.NewNop().Sugar(),
			envs:    nil,
			wantErr: false,
		},
		{
			name:    "test.2 err",
			cfg:     cfg,
			logger:  nil,
			envs:    nil,
			wantErr: true,
		},
		{
			name:   "test.3 err tcp server",
			cfg:    cfg,
			logger: zap.NewNop().Sugar(),
			envs: map[string]string{
				"CKVTEST_NETWORK_MAX_MESSAGE_SIZE": "4Zz",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set env
			for k, v := range tt.envs {
				os.Setenv(k, v)
			}
			// unset env
			defer func() {
				for k := range tt.envs {
					os.Unsetenv(k)
				}
			}()

			_, err := New(tt.logger, tt.cfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestApp_Run(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*1200)
	defer cancel()

	cfg, err := config.Read("CKVTEST", "./testdata/config.toml")
	if err != nil {
		t.Fatalf("read config: %s", err)
	}

	srv, err := New(zap.NewNop().Sugar(), cfg)
	if err != nil {
		t.Errorf("New() error = %v", err)
		return
	}

	tests := []struct {
		name string
		app  *App
	}{
		{
			name: "test.1 ok",
			app:  srv,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.app.Run(ctx); err != nil {
				t.Errorf("app.Run() got error: %s", err)
			}

			tt.app.Stop()
		})
	}
	time.Sleep(time.Millisecond * 250)
}
