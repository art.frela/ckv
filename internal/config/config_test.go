// Package config contains wrapper around config, using dotconf package.
package config

import (
	"bytes"
	"testing"

	"gitlab.com/art.frela/ckv/pkg/dotconf"
)

func TestRead(t *testing.T) {
	tests := []struct {
		name      string
		path      string
		schema    string
		key       string
		wantValue string
		wantErr   bool
	}{
		{
			"тест.1 yaml - ok",
			"testdata/config.yaml",
			schema,
			"engine.kind",
			"inmemory",
			false,
		},
		{
			"тест.2 toml - ok",
			"testdata/config.toml",
			schema,
			"network.address",
			"localhost:3000",
			false,
		},
		{
			"тест.3 json - ok",
			"testdata/config.json",
			schema,
			"network.address",
			"localhost:3000",
			false,
		},
		{
			"тест.4 yaml min - ok, несуществующий ключ",
			"testdata/config_min.yaml",
			schema,
			"network.max_message_size",
			"",
			false,
		},
		{
			"тест.5 несуществующий конфиг",
			"testdata/config_notexists.yaml",
			schema,
			"network.max_message_size",
			"",
			true,
		},
		{
			"тест.6 yaml min - невалидный",
			"testdata/config_min_invalid.yaml",
			schema,
			"network.max_message_size",
			"",
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Read("", tt.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantErr {
				return
			}

			if gotValue := got.GetString(tt.key); gotValue != tt.wantValue {
				t.Errorf("got.GetString = %s, wantErr %s", gotValue, tt.wantValue)
			}
		})
	}
}

func TestWriteDefault(t *testing.T) {
	tests := []struct {
		name    string
		format  string
		wantErr bool
	}{
		{
			name:    "test.1 ok toml",
			format:  dotconf.FormatTOML,
			wantErr: false,
		},
		{
			name:    "test.2 ok json",
			format:  dotconf.FormatJSON,
			wantErr: false,
		},
		{
			name:    "test.3 ok yaml",
			format:  dotconf.FormatYAML,
			wantErr: false,
		},
		{
			name:    "test.4 ok yml",
			format:  dotconf.FormatYML,
			wantErr: false,
		},
		{
			name:    "test.5 err csv",
			format:  "csv",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			out := &bytes.Buffer{}

			if err := WriteDefault(out, tt.format); (err != nil) != tt.wantErr {
				t.Errorf("WriteDefault() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestWriteConfig(t *testing.T) {
	tests := []struct {
		name    string
		cfg     interface{}
		format  string
		wantOut string
		wantErr bool
	}{
		{
			name:   "test.1 ok json",
			cfg:    map[string]any{"root": "master"},
			format: dotconf.FormatJSON,
			wantOut: `{
  "root": "master"
}`,
			wantErr: false,
		},
		{
			name:   "test.2 ok yaml",
			cfg:    map[string]any{"root": "master"},
			format: dotconf.FormatYAML,
			wantOut: `root: master
`,
			wantErr: false,
		},
		{
			name:   "test.3 ok toml",
			cfg:    map[string]any{"root": "master"},
			format: dotconf.FormatTOML,
			wantOut: `root = 'master'
`,
			wantErr: false,
		},
		{
			name:    "test.4 err csv",
			cfg:     map[string]any{"root": "master"},
			format:  "csv",
			wantOut: ``,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			out := &bytes.Buffer{}
			if err := WriteConfig(out, tt.cfg, tt.format); (err != nil) != tt.wantErr {
				t.Errorf("WriteConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if gotOut := out.String(); gotOut != tt.wantOut {
				t.Errorf("WriteConfig() = %v, want %v", gotOut, tt.wantOut)
			}
		})
	}
}
