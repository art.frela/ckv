// Package config contains wrapper around config, using dotconf package.
package config

import (
	"fmt"
	"io"
	"slices"
	"time"

	"gitlab.com/art.frela/ckv/pkg/dotconf"
	enc "gitlab.com/art.frela/ckv/pkg/dotconf/encoding"
	"gitlab.com/art.frela/ckv/pkg/dotconf/encoding/json"
	"gitlab.com/art.frela/ckv/pkg/dotconf/encoding/toml"
	"gitlab.com/art.frela/ckv/pkg/dotconf/encoding/yaml"
)

const (
	DefEngineKind  = "inmemory"
	DefLoggerLevel = "info"
	DefDevelopment = false
	DefAddress     = "0.0.0.0:3000"
	DefMaxConn     = 1500
	DefMessageSize = "256B"
	DefTimeout     = time.Minute
	//
	FormatYAML string = "yaml"
	FormatYML  string = "yml"
	FormatJSON string = "json"
	FormatTOML string = "toml"
)

var (
	DefaultConfig = map[string]interface{}{
		"engine": map[string]interface{}{
			"kind": DefEngineKind,
		},
		"logger": map[string]interface{}{
			"level":       DefLoggerLevel,
			"development": DefDevelopment,
		},
		"network": map[string]interface{}{
			"address":          DefAddress,
			"max_connections":  DefMaxConn,
			"max_message_size": DefMessageSize,
			"idle_timeout":     DefTimeout.String(),
		},
	}

	supportedFormats = []string{FormatJSON, FormatTOML, FormatYAML, FormatYML}
)

func Read(envPREFIX, rootConfig string, addConfigs ...string) (*dotconf.DotConf, error) {
	dc := dotconf.NewWithSchema(envPREFIX, schema)
	if err := dc.Read(rootConfig, addConfigs...); err != nil {
		return nil, fmt.Errorf("read config %w", err)
	}

	if err := dc.Validate(); err != nil {
		return dc, fmt.Errorf("validate config: %w", err)
	}

	return dc, nil
}

func WriteDefault(out io.Writer, format string) error {
	if !slices.Contains(supportedFormats, format) {
		return fmt.Errorf("unsupport format %s, allow only %v", format, supportedFormats)
	}

	return WriteConfig(out, DefaultConfig, format)
}

func WriteConfig(out io.Writer, cfg interface{}, format string) error {
	var e enc.Encoder
	switch format {
	case FormatJSON:
		e = json.Codec{}
	case FormatYAML, FormatYML:
		e = yaml.Codec{}
	case FormatTOML:
		e = toml.Codec{}
	default:
		return fmt.Errorf("unsupporter format %s", format)
	}

	bts, err := e.Encode(cfg)
	if err != nil {
		return err
	}
	_, err = out.Write(bts)
	return err
}

//nolint:lll
const schema = `{
    "$id": "https://https://gitlab.com/art.frela/ckv/internal/config/schema.v1.json",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "type": "object",
    "title": "The ckv config schema",
    "description": "Параметры конфигурации приложения",
    "required": [
        "engine",
        "network"
    ],
    "properties": {
        "engine": {
            "type": "object",
            "title": "The database engine schema",
            "description": "Параметры движка базы данных",
            "properties": {
                "kind": {
                    "type": "string",
                    "enum": [
                        "inmemory"
                    ],
                    "description": "Тип движка базы данных"
                }
            },
            "required": [
                "kind"
            ]
        },
        "logger": {
            "type": "object",
            "title": "The logger schema",
            "description": "Параметры логирования",
            "properties": {
                "level": {
                    "type": "string",
                    "enum": [
                        "debug",
                        "info",
                        "warn",
                        "error"
                    ],
                    "title": "The level severity",
                    "description": "Уровень логирования (in order: debug, info, warn, error...)"
                },
                "development": {
                    "type": "boolean",
                    "description": "Флаг, вкл/нет формат в режиме разработки",
                    "default": false
                }
            }
        },
        "network": {
            "type": "object",
            "title": "The tcp server schema",
            "description": "Параметры для настройки tcp сервера",
            "properties": {
                "address": {
                    "type": "string",
                    "description": "host:port для биндинга tcp сервера",
                    "default": "localhost:3000"
                },
                "max_connections": {
                    "type": "integer",
                    "description": "Максимальное число одновременных tcp соединений",
                    "default": 1500
                },
                "max_message_size": {
                    "$ref": "#/definitions/size",
                    "description": "Максимальный размер одного сообщения",
                    "default": "256B"
                },
                "idle_timeout": {
                    "$ref": "#/definitions/duration",
                    "description": "Таймаут на прием/передачу данных по сети",
                    "default": "10s"
                }
            },
            "required": [
                "address"
            ]
        }
    },
    "definitions": {
        "duration": {
            "type": "string",
            "pattern": "^([0-9]+[mnsuµh]*)+$"
        },
        "size": {
            "type": "string",
            "pattern": "^[1-9][0-9]*[GgMmKkBb]?[Bb]?$"
        }
    }
}`
