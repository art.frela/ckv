package entity

import (
	"sync"
	"sync/atomic"
)

var (
	once  sync.Once
	genID *IDGenerator
)

type IDGenerator struct {
	counter atomic.Int64
}

func GetIDGenerator() *IDGenerator {
	once.Do(func() {
		genID = &IDGenerator{}
	})

	return genID
}

func (g *IDGenerator) Generate() int64 {
	old := g.counter.Load()
	for !g.counter.CompareAndSwap(old, old+1) {
		old = g.counter.Load()
	}

	return old + 1
}
