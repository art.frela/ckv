package entity

import (
	"reflect"
	"testing"
)

func TestNewQuery(t *testing.T) {
	tests := []struct {
		name      string
		commandID CommandID
		arguments []string
		want      Query
		wantCID   CommandID
		wantArgs  []string
	}{
		{
			name:      "test.1 ok",
			commandID: GetCommandID,
			arguments: []string{"foo"},
			want:      Query{CID: GetCommandID, Args: []string{"foo"}},
			wantCID:   GetCommandID,
			wantArgs:  []string{"foo"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewQuery(tt.commandID, tt.arguments)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewQuery() = %v, want %v", got, tt.want)
			}

			if !reflect.DeepEqual(got.Arguments(), tt.wantArgs) {
				t.Errorf("NewQuery().Arguments() = %v, want %v", got.Arguments(), tt.wantArgs)
			}

			if !reflect.DeepEqual(got.CommandID(), tt.wantCID) {
				t.Errorf("NewQuery().CommandID() = %v, want %v", got.CommandID(), tt.wantCID)
			}
		})
	}
}
