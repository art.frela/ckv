package entity

type Query struct {
	Args []string
	CID  CommandID
}

func NewQuery(commandID CommandID, arguments []string) Query {
	return Query{
		CID:  commandID,
		Args: arguments,
	}
}

func (c *Query) CommandID() CommandID {
	return c.CID
}

func (c *Query) Arguments() []string {
	return c.Args
}
