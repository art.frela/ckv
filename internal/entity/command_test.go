package entity

import (
	"testing"
)

func TestCommandID_String(t *testing.T) {
	tests := []struct {
		name string
		cid  CommandID
		want string
	}{
		{
			name: "test.1 set",
			cid:  SetCommandID,
			want: SetCommand,
		},
		{
			name: "test.2 get",
			cid:  GetCommandID,
			want: GetCommand,
		},
		{
			name: "test.3 del",
			cid:  DelCommandID,
			want: DelCommand,
		},
		{
			name: "test.4 undefined",
			cid:  7,
			want: UnknownCommand,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.cid.String(); got != tt.want {
				t.Errorf("CommandID.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseCommand(t *testing.T) {
	tests := []struct {
		name    string
		command string
		want    CommandID
	}{
		{
			name:    "test.1 set",
			command: SetCommand,
			want:    SetCommandID,
		},
		{
			name:    "test.2 get",
			command: GetCommand,
			want:    GetCommandID,
		},
		{
			name:    "test.3 del",
			command: DelCommand,
			want:    DelCommandID,
		},
		{
			name:    "test.4 UPD",
			command: "UPD",
			want:    UnknownCommandID,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseCommand(tt.command); got != tt.want {
				t.Errorf("ParseCommand() = %v, want %v", got, tt.want)
			}
		})
	}
}
