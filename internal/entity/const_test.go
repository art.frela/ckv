package entity

import (
	"context"
	"reflect"
	"testing"
)

func TestWithTX(t *testing.T) {
	ctxEmpty := context.Background()
	txID := int64(42)
	ctx42 := context.WithValue(ctxEmpty, trID, txID)
	txID57 := int64(57)
	ctx57 := context.WithValue(ctxEmpty, trID, txID57)

	tests := []struct {
		name string
		ctx  context.Context
		txid int64
		want context.Context
	}{
		{
			name: "test.1 add trID in empty",
			ctx:  ctxEmpty,
			txid: txID,
			want: ctx42,
		},
		{
			name: "test.2 rewrite trID",
			ctx:  ctxEmpty,
			txid: txID57,
			want: ctx57,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := WithTX(tt.ctx, tt.txid); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("WithTX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFromCTX(t *testing.T) {
	ctxEmpty := context.Background()
	txID := int64(42)
	ctx42 := context.WithValue(ctxEmpty, trID, txID)

	tests := []struct {
		name string
		ctx  context.Context
		want int64
	}{
		{
			name: "test.1 from empty",
			ctx:  ctxEmpty,
			want: 1,
		},
		{
			name: "test.2 from exists",
			ctx:  ctx42,
			want: txID,
		},
		{
			name: "test.2 from empty",
			ctx:  ctxEmpty,
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromCTX(tt.ctx); got != tt.want {
				t.Errorf("FromCTX() = %v, want %v", got, tt.want)
			}
		})
	}
}
