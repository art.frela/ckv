package entity

import "context"

type TransactionID string

const trID TransactionID = "txid"

func WithTX(ctx context.Context, txid int64) context.Context {
	return context.WithValue(ctx, trID, txid)
}

func FromCTX(ctx context.Context) int64 {
	if txid, ok := ctx.Value(trID).(int64); ok {
		return txid
	}

	return GetIDGenerator().Generate()
}
