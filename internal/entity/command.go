package entity

type CommandID uint8

const (
	UnknownCommandID CommandID = iota
	SetCommandID
	GetCommandID
	DelCommandID
)

var (
	UnknownCommand = "UNKNOWN"
	SetCommand     = "SET"
	GetCommand     = "GET"
	DelCommand     = "DEL"
)

func (cid CommandID) String() string {
	//nolint:exhaustive
	switch cid {
	case SetCommandID:
		return SetCommand
	case GetCommandID:
		return GetCommand
	case DelCommandID:
		return DelCommand
	default:
		return UnknownCommand
	}
}

var commandNamesToID = map[string]CommandID{
	UnknownCommand: UnknownCommandID,
	SetCommand:     SetCommandID,
	GetCommand:     GetCommandID,
	DelCommand:     DelCommandID,
}

func ParseCommand(command string) CommandID {
	status, found := commandNamesToID[command]
	if !found {
		return UnknownCommandID
	}

	return status
}
