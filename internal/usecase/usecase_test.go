package usecase

import (
	"context"
	"errors"
	"testing"
	"time"

	"gitlab.com/art.frela/ckv/internal/entity"
	umock "gitlab.com/art.frela/ckv/internal/usecase/mocks"
)

const testTimeout = time.Second * 30

func TestUsecase_HandleQuery(t *testing.T) {
	// prepare
	ctx, cancel := context.WithTimeout(context.Background(), testTimeout)
	defer cancel()

	set1ok := "SET foo bar"
	query1ok := entity.Query{
		CID:  entity.SetCommandID,
		Args: []string{"foo", "bar"},
	}
	set2ok := "SET foo2 bar2"
	query2ok := entity.Query{
		CID:  entity.SetCommandID,
		Args: []string{"foo2", "bar2"},
	}
	//
	get1ok := "GET foo"
	queryGet1ok := entity.Query{
		CID:  entity.GetCommandID,
		Args: []string{"foo"},
	}
	get2ok := "GET foo2"
	queryGet2ok := entity.Query{
		CID:  entity.GetCommandID,
		Args: []string{"foo2"},
	}
	//
	del1ok := "DEL foo"
	queryDel1ok := entity.Query{
		CID:  entity.DelCommandID,
		Args: []string{"foo"},
	}
	del2ok := "DEL foo2"
	queryDel2ok := entity.Query{
		CID:  entity.DelCommandID,
		Args: []string{"foo2"},
	}

	undefined1ok := "UPD foo2"
	queryUpd2ok := entity.Query{
		CID:  entity.UnknownCommandID,
		Args: []string{"foo2"},
	}

	set3err := "SET zoo park"

	// setup compute interface
	comp := umock.NewMockCompute(t)
	comp.EXPECT().HandleQuery(ctx, set1ok).Return(query1ok, nil)
	comp.EXPECT().HandleQuery(ctx, set2ok).Return(query2ok, nil)
	comp.EXPECT().HandleQuery(ctx, get1ok).Return(queryGet1ok, nil)
	comp.EXPECT().HandleQuery(ctx, get2ok).Return(queryGet2ok, nil)
	comp.EXPECT().HandleQuery(ctx, del1ok).Return(queryDel1ok, nil)
	comp.EXPECT().HandleQuery(ctx, del2ok).Return(queryDel2ok, nil)
	comp.EXPECT().HandleQuery(ctx, undefined1ok).Return(queryUpd2ok, nil)
	comp.EXPECT().HandleQuery(ctx, set3err).Return(entity.Query{}, errors.New("some error"))

	// setup storage interface
	repo := umock.NewMockStorage(t)
	repo.EXPECT().Set(ctx, "foo", "bar").Return(nil)
	repo.EXPECT().Set(ctx, "foo2", "bar2").Return(errors.New("some error"))
	repo.EXPECT().Get(ctx, "foo").Return("bar", nil)
	repo.EXPECT().Get(ctx, "foo2").Return("", errors.New("some error"))
	repo.EXPECT().Del(ctx, "foo").Return(nil)
	repo.EXPECT().Del(ctx, "foo2").Return(errors.New("some error"))

	tests := []struct {
		name     string
		compute  Compute
		repo     Storage
		queryStr string
		want     string
	}{
		{
			name:     "test.1 set ok",
			compute:  comp,
			repo:     repo,
			queryStr: set1ok,
			want:     "[ok]",
		},
		{
			name:     "test.2 set repo error",
			compute:  comp,
			repo:     repo,
			queryStr: set2ok,
			want:     "[error] some error",
		},
		{
			name:     "test.3 get ok",
			compute:  comp,
			repo:     repo,
			queryStr: get1ok,
			want:     "bar",
		},
		{
			name:     "test.4 get repo error",
			compute:  comp,
			repo:     repo,
			queryStr: get2ok,
			want:     "[error] some error",
		},
		{
			name:     "test.5 del ok",
			compute:  comp,
			repo:     repo,
			queryStr: del1ok,
			want:     "[ok]",
		},
		{
			name:     "test.6 del repo error",
			compute:  comp,
			repo:     repo,
			queryStr: del2ok,
			want:     "[error] some error",
		},
		{
			name:     "test.7 undefinedCMD",
			compute:  comp,
			repo:     repo,
			queryStr: undefined1ok,
			want:     "[error] undefined command UNKNOWN",
		},
		{
			name:     "test.8 comp error",
			compute:  comp,
			repo:     repo,
			queryStr: set3err,
			want:     "[error] some error",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := New(tt.compute, tt.repo)

			if got := uc.HandleQuery(ctx, tt.queryStr); got != tt.want {
				t.Errorf("Usecase.HandleQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}
