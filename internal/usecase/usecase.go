package usecase

import (
	"context"
	"fmt"

	"gitlab.com/art.frela/ckv/internal/entity"
)

const errTmpl = "[error] %s"

type Compute interface {
	HandleQuery(context.Context, string) (entity.Query, error)
}

type Storage interface {
	Set(context.Context, string, string) error
	Get(context.Context, string) (string, error)
	Del(context.Context, string) error
}

type Usecase struct {
	compute Compute
	repo    Storage
}

func New(compute Compute, repo Storage) *Usecase {
	return &Usecase{
		compute: compute,
		repo:    repo,
	}
}

func (uc *Usecase) HandleQuery(ctx context.Context, queryStr string) string {
	query, err := uc.compute.HandleQuery(ctx, queryStr)
	if err != nil {
		return fmt.Sprintf(errTmpl, err.Error())
	}

	//nolint:exhaustive
	switch query.CommandID() {
	case entity.SetCommandID:
		return uc.handleSet(ctx, query)
	case entity.GetCommandID:
		return uc.handleGet(ctx, query)
	case entity.DelCommandID:
		return uc.handleDel(ctx, query)
	default:
		return fmt.Sprintf("[error] undefined command %s", query.CommandID())
	}
}

func (uc *Usecase) handleSet(ctx context.Context, query entity.Query) string {
	args := query.Arguments()
	if err := uc.repo.Set(ctx, args[0], args[1]); err != nil {
		return fmt.Sprintf(errTmpl, err.Error())
	}

	return "[ok]"
}

func (uc *Usecase) handleGet(ctx context.Context, query entity.Query) string {
	args := query.Arguments()
	value, err := uc.repo.Get(ctx, args[0])
	if err != nil {
		return fmt.Sprintf(errTmpl, err.Error())
	}

	return value
}

func (uc *Usecase) handleDel(ctx context.Context, query entity.Query) string {
	args := query.Arguments()
	if err := uc.repo.Del(ctx, args[0]); err != nil {
		return fmt.Sprintf(errTmpl, err.Error())
	}

	return "[ok]"
}
