package storage

import (
	"context"
	"errors"
	"fmt"

	"go.uber.org/zap"
)

type Engine interface {
	Set(context.Context, string, string)
	Get(context.Context, string) (string, bool)
	Del(context.Context, string)
}

type Storage struct {
	engine Engine
	log    *zap.SugaredLogger
}

func NewStorage(opts ...Option) (*Storage, error) {
	ops := options{}
	for _, apply := range opts {
		if err := apply(&ops); err != nil {
			return nil, fmt.Errorf("apply option: %w", err)
		}
	}

	if ops.engine == nil {
		return nil, errors.New("engine is invalid")
	}

	if ops.logger == nil {
		return nil, errors.New("logger is invalid")
	}

	return &Storage{
		engine: ops.engine,
		log:    ops.logger,
	}, nil
}

func (s *Storage) Set(ctx context.Context, key, value string) error {
	s.engine.Set(ctx, key, value)
	return nil
}

func (s *Storage) Del(ctx context.Context, key string) error {
	s.engine.Del(ctx, key)
	return nil
}

func (s *Storage) Get(ctx context.Context, key string) (string, error) {
	value, ok := s.engine.Get(ctx, key)
	if !ok {
		return "", errors.New("not found")
	}

	return value, nil
}
