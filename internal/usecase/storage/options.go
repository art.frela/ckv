package storage

import (
	"errors"

	"go.uber.org/zap"
)

type options struct {
	logger *zap.SugaredLogger
	engine Engine
}

type Option func(*options) error

func WithLogger(log *zap.SugaredLogger) Option {
	return func(o *options) error {
		if log == nil {
			return errors.New("empty logger")
		}

		o.logger = log
		return nil
	}
}

func WithEngine(e Engine) Option {
	return func(o *options) error {
		if e == nil {
			return errors.New("empty engine")
		}

		o.engine = e
		return nil
	}
}
