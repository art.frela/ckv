package storage

import (
	"reflect"
	"testing"

	smock "gitlab.com/art.frela/ckv/internal/usecase/storage/mocks"

	"go.uber.org/zap"
)

func TestWithLogger(t *testing.T) {
	logger := zap.NewNop().Sugar()

	tests := []struct {
		name    string
		logger  *zap.SugaredLogger
		wantErr bool
	}{
		{
			"test.1 success",
			logger,
			false,
		},
		{
			"test.2 error",
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var opts options
			apply := WithLogger(tt.logger)
			if apply == nil {
				t.Error("WithLogger got nil")
				return
			}

			if err := apply(&opts); (err != nil) != tt.wantErr {
				t.Errorf("WithLogger()(opts) error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(opts.logger, tt.logger) {
				t.Errorf("WithLogger()(opts) opts.logger = %v, want %v", opts.logger, tt.logger)
			}
		})
	}
}

func TestWithEngine(t *testing.T) {
	engine := smock.NewMockEngine(t)

	tests := []struct {
		name    string
		engine  Engine
		wantErr bool
	}{
		{
			"test.1 success",
			engine,
			false,
		},
		{
			"test.2 error",
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var opts options
			apply := WithEngine(tt.engine)
			if apply == nil {
				t.Error("WithEngine got nil")
				return
			}

			if err := apply(&opts); (err != nil) != tt.wantErr {
				t.Errorf("WithEngine()(opts) error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(opts.engine, tt.engine) {
				t.Errorf("WithEngine()(opts) opts.engine = %v, want %v", opts.engine, tt.engine)
			}
		})
	}
}
