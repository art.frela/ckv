package inmemory

import (
	"context"
	"testing"
)

func TestEngine_SetGetDel(t *testing.T) {
	ctx := context.Background()
	ctxCancelled, cancel := context.WithCancel(context.Background())
	cancel()

	tests := []struct {
		name    string
		ctx     context.Context
		key     string
		value   string
		wantGot bool
	}{
		{
			name:    "test.1 ok",
			ctx:     ctx,
			key:     "foo",
			value:   "bar",
			wantGot: true,
		},
		{
			name:    "test.2 error, ctc cancelled",
			ctx:     ctxCancelled,
			key:     "foo",
			value:   "bar",
			wantGot: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := New()
			// check not exists
			v1, ok1 := e.Get(tt.ctx, tt.key)
			if ok1 {
				t.Errorf("e.Get() got exists value %v, want false", v1)
				return
			}

			e.Set(tt.ctx, tt.key, tt.value)

			v2, ok2 := e.Get(tt.ctx, tt.key)
			if ok2 != tt.wantGot {
				t.Errorf("e.Get() not exists key %v, want true", tt.key)
				return
			}

			if tt.wantGot && v2 != tt.value {
				t.Errorf("e.Set() e.Get() got value %v, want %v", v2, tt.value)
				return
			}

			e.Del(tt.ctx, tt.key)
			// check not exists
			v3, ok3 := e.Get(tt.ctx, tt.key)
			if ok3 {
				t.Errorf("e.Get() got exists value %v, want false", v3)
				return
			}
		})
	}
}
