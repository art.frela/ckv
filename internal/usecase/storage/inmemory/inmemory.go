package inmemory

import (
	"context"
	"sync"

	"gitlab.com/art.frela/ckv/internal/usecase/storage"
)

var _ storage.Engine = (*Engine)(nil)

type Engine struct {
	mu   *sync.RWMutex
	data map[string]string
}

func New() *Engine {
	return &Engine{
		mu:   &sync.RWMutex{},
		data: make(map[string]string),
	}
}

func (e *Engine) Set(ctx context.Context, key, value string) {
	if ctx.Err() != nil {
		return
	}

	e.mu.Lock()
	defer e.mu.Unlock()

	e.data[key] = value
}

func (e *Engine) Get(ctx context.Context, key string) (string, bool) {
	if ctx.Err() != nil {
		return "", false
	}

	e.mu.RLock()
	defer e.mu.RUnlock()

	value, ok := e.data[key]
	return value, ok
}

func (e *Engine) Del(ctx context.Context, key string) {
	if ctx.Err() != nil {
		return
	}

	e.mu.Lock()
	defer e.mu.Unlock()

	delete(e.data, key)
}
