package storage

import (
	"context"
	"reflect"
	"testing"

	smock "gitlab.com/art.frela/ckv/internal/usecase/storage/mocks"
	"go.uber.org/zap"
)

func TestNewStorage(t *testing.T) {
	log := zap.NewNop().Sugar()
	engine := smock.NewMockEngine(t)

	tests := []struct {
		name    string
		opts    []Option
		want    *Storage
		wantErr bool
	}{
		{
			name: "test.1 ok",
			opts: []Option{
				WithEngine(engine),
				WithLogger(log),
			},
			want: &Storage{
				engine: engine,
				log:    log,
			},
			wantErr: false,
		},
		{
			name: "test.2 err apply",
			opts: []Option{
				WithEngine(nil),
				WithLogger(nil),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "test.3 err miss engine",
			opts: []Option{
				WithLogger(log),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "test.4 err miss log",
			opts: []Option{
				WithEngine(engine),
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewStorage(tt.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewStorage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewStorage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStorage_Set(t *testing.T) {
	ctx := context.Background()
	log := zap.NewNop().Sugar()

	k1, v1 := "foo", "bar"

	engine := smock.NewMockEngine(t)
	engine.EXPECT().Set(ctx, k1, v1).Return()

	tests := []struct {
		name    string
		engine  Engine
		ctx     context.Context
		key     string
		value   string
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			engine:  engine,
			ctx:     ctx,
			key:     k1,
			value:   v1,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := NewStorage(
				WithLogger(log),
				WithEngine(tt.engine),
			)
			if err != nil {
				t.Errorf("NewStorage error %v", err)
				return
			}

			if err := s.Set(tt.ctx, tt.key, tt.value); (err != nil) != tt.wantErr {
				t.Errorf("Storage.Set() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStorage_Get(t *testing.T) {
	ctx := context.Background()
	log := zap.NewNop().Sugar()

	k1, v1 := "foo", "bar"
	k2, v2 := "zoo", ""

	engine := smock.NewMockEngine(t)
	engine.EXPECT().Get(ctx, k1).Return(v1, true)
	engine.EXPECT().Get(ctx, k2).Return(v2, false)

	tests := []struct {
		name      string
		engine    Engine
		ctx       context.Context
		key       string
		wantValue string
		wantErr   bool
	}{
		{
			name:      "test.1 ok",
			engine:    engine,
			ctx:       ctx,
			key:       k1,
			wantValue: v1,
			wantErr:   false,
		},
		{
			name:      "test.2 error not found",
			engine:    engine,
			ctx:       ctx,
			key:       k2,
			wantValue: v2,
			wantErr:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := NewStorage(
				WithLogger(log),
				WithEngine(tt.engine),
			)
			if err != nil {
				t.Errorf("NewStorage error %v", err)
				return
			}

			got, err := s.Get(tt.ctx, tt.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Storage.Get() error = %v, wantErr %v", err, tt.wantErr)
			}

			if got != tt.wantValue {
				t.Errorf("Storage.Get() got = %v, want %v", got, tt.wantValue)
			}
		})
	}
}

func TestStorage_Del(t *testing.T) {
	ctx := context.Background()
	log := zap.NewNop().Sugar()

	k1 := "foo"

	engine := smock.NewMockEngine(t)
	engine.EXPECT().Del(ctx, k1).Return()

	tests := []struct {
		name    string
		engine  Engine
		ctx     context.Context
		key     string
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			engine:  engine,
			ctx:     ctx,
			key:     k1,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := NewStorage(
				WithLogger(log),
				WithEngine(tt.engine),
			)
			if err != nil {
				t.Errorf("NewStorage error %v", err)
				return
			}

			if err := s.Del(tt.ctx, tt.key); (err != nil) != tt.wantErr {
				t.Errorf("Storage.Del() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
