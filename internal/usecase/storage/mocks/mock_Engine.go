// Code generated by mockery v2.42.3. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// MockEngine is an autogenerated mock type for the Engine type
type MockEngine struct {
	mock.Mock
}

type MockEngine_Expecter struct {
	mock *mock.Mock
}

func (_m *MockEngine) EXPECT() *MockEngine_Expecter {
	return &MockEngine_Expecter{mock: &_m.Mock}
}

// Del provides a mock function with given fields: _a0, _a1
func (_m *MockEngine) Del(_a0 context.Context, _a1 string) {
	_m.Called(_a0, _a1)
}

// MockEngine_Del_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Del'
type MockEngine_Del_Call struct {
	*mock.Call
}

// Del is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 string
func (_e *MockEngine_Expecter) Del(_a0 interface{}, _a1 interface{}) *MockEngine_Del_Call {
	return &MockEngine_Del_Call{Call: _e.mock.On("Del", _a0, _a1)}
}

func (_c *MockEngine_Del_Call) Run(run func(_a0 context.Context, _a1 string)) *MockEngine_Del_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *MockEngine_Del_Call) Return() *MockEngine_Del_Call {
	_c.Call.Return()
	return _c
}

func (_c *MockEngine_Del_Call) RunAndReturn(run func(context.Context, string)) *MockEngine_Del_Call {
	_c.Call.Return(run)
	return _c
}

// Get provides a mock function with given fields: _a0, _a1
func (_m *MockEngine) Get(_a0 context.Context, _a1 string) (string, bool) {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for Get")
	}

	var r0 string
	var r1 bool
	if rf, ok := ret.Get(0).(func(context.Context, string) (string, bool)); ok {
		return rf(_a0, _a1)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) string); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Get(0).(string)
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) bool); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Get(1).(bool)
	}

	return r0, r1
}

// MockEngine_Get_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Get'
type MockEngine_Get_Call struct {
	*mock.Call
}

// Get is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 string
func (_e *MockEngine_Expecter) Get(_a0 interface{}, _a1 interface{}) *MockEngine_Get_Call {
	return &MockEngine_Get_Call{Call: _e.mock.On("Get", _a0, _a1)}
}

func (_c *MockEngine_Get_Call) Run(run func(_a0 context.Context, _a1 string)) *MockEngine_Get_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string))
	})
	return _c
}

func (_c *MockEngine_Get_Call) Return(_a0 string, _a1 bool) *MockEngine_Get_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *MockEngine_Get_Call) RunAndReturn(run func(context.Context, string) (string, bool)) *MockEngine_Get_Call {
	_c.Call.Return(run)
	return _c
}

// Set provides a mock function with given fields: _a0, _a1, _a2
func (_m *MockEngine) Set(_a0 context.Context, _a1 string, _a2 string) {
	_m.Called(_a0, _a1, _a2)
}

// MockEngine_Set_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Set'
type MockEngine_Set_Call struct {
	*mock.Call
}

// Set is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 string
//   - _a2 string
func (_e *MockEngine_Expecter) Set(_a0 interface{}, _a1 interface{}, _a2 interface{}) *MockEngine_Set_Call {
	return &MockEngine_Set_Call{Call: _e.mock.On("Set", _a0, _a1, _a2)}
}

func (_c *MockEngine_Set_Call) Run(run func(_a0 context.Context, _a1 string, _a2 string)) *MockEngine_Set_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string), args[2].(string))
	})
	return _c
}

func (_c *MockEngine_Set_Call) Return() *MockEngine_Set_Call {
	_c.Call.Return()
	return _c
}

func (_c *MockEngine_Set_Call) RunAndReturn(run func(context.Context, string, string)) *MockEngine_Set_Call {
	_c.Call.Return(run)
	return _c
}

// NewMockEngine creates a new instance of MockEngine. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMockEngine(t interface {
	mock.TestingT
	Cleanup(func())
}) *MockEngine {
	mock := &MockEngine{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
