package parser

import (
	"context"
	"errors"

	"gitlab.com/art.frela/ckv/internal/usecase/compute"
	"gitlab.com/art.frela/ckv/pkg/fsm"

	"go.uber.org/zap"
)

var _ compute.Parser = (*Parser)(nil)

type Parser struct {
	sm  *fsm.Machine
	log *zap.SugaredLogger
}

func New(logger *zap.SugaredLogger) (*Parser, error) {
	if logger == nil {
		return nil, errors.New("empty logger")
	}

	m := fsm.New(fsm.InitialState)

	// std flow
	m.AddTransition(fsm.InitialState, fsm.LetterEv, fsm.WordState, m.AppendSymbolToToken)
	m.AddTransition(fsm.InitialState, fsm.WhiteSpaceEv, fsm.WhiteSpaceState, nil)

	m.AddTransition(fsm.WordState, fsm.LetterEv, fsm.WordState, m.AppendSymbolToToken)
	m.AddTransition(fsm.WordState, fsm.WhiteSpaceEv, fsm.WhiteSpaceState, m.AppendToken)
	m.AddTransition(fsm.WhiteSpaceState, fsm.LetterEv, fsm.WordState, m.AppendSymbolToToken)
	m.AddTransition(fsm.WhiteSpaceState, fsm.WhiteSpaceEv, fsm.WhiteSpaceState, nil)

	return &Parser{sm: m, log: logger}, nil
}

func (p *Parser) ParseQuery(ctx context.Context, query string) ([]string, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
		return p.sm.Parse(query)
	}
}
