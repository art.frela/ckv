package parser

import (
	"context"
	"reflect"
	"testing"

	"go.uber.org/zap"
)

func TestParser_ParseQuery(t *testing.T) {
	log := zap.NewNop().Sugar()

	ctx := context.Background()
	ctxCancelled, cancel := context.WithCancel(ctx)
	cancel()

	tests := []struct {
		name    string
		ctx     context.Context
		query   string
		want    []string
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			ctx:     ctx,
			query:   "SET foo bar",
			want:    []string{"SET", "foo", "bar"},
			wantErr: false,
		},
		{
			name:    "test.1 ok",
			ctx:     ctxCancelled,
			query:   "SET foo bar",
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p, _ := New(log)

			got, err := p.ParseQuery(tt.ctx, tt.query)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parser.ParseQuery() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parser.ParseQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNew(t *testing.T) {
	tests := []struct {
		name    string
		logger  *zap.SugaredLogger
		want    *Parser
		wantErr bool
	}{
		{
			name:    "test.1 negative",
			logger:  nil,
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.logger)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}
