package compute

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/art.frela/ckv/internal/entity"
	"gitlab.com/art.frela/ckv/internal/usecase"
)

var _ usecase.Compute = (*Compute)(nil)

type Parser interface {
	ParseQuery(ctx context.Context, query string) ([]string, error)
}

type Analyzer interface {
	Analyze(ctx context.Context, tokens []string) (entity.Query, error)
}

type Compute struct {
	parser   Parser
	analyzer Analyzer
}

func New(parser Parser, analyzer Analyzer) (*Compute, error) {
	if parser == nil {
		return nil, errors.New("empty parser")
	}

	if analyzer == nil {
		return nil, errors.New("empty analyzer")
	}

	return &Compute{
		parser:   parser,
		analyzer: analyzer,
	}, nil
}

func (c *Compute) HandleQuery(ctx context.Context, query string) (entity.Query, error) {
	tokens, err := c.parser.ParseQuery(ctx, query)
	if err != nil {
		return entity.Query{}, fmt.Errorf("parseQuery: %w", err)
	}

	q, err := c.analyzer.Analyze(ctx, tokens)
	if err != nil {
		return entity.Query{}, fmt.Errorf("analyze: %w", err)
	}

	return q, nil
}
