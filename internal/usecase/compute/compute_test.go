package compute

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"gitlab.com/art.frela/ckv/internal/entity"
	cmock "gitlab.com/art.frela/ckv/internal/usecase/compute/mocks"
)

func TestNew(t *testing.T) {
	parser := cmock.NewMockParser(t)
	analyzer := cmock.NewMockAnalyzer(t)

	tests := []struct {
		name     string
		parser   Parser
		analyzer Analyzer
		want     *Compute
		wantErr  bool
	}{
		{
			name:     "test.1 ok",
			parser:   parser,
			analyzer: analyzer,
			want: &Compute{
				parser:   parser,
				analyzer: analyzer,
			},
			wantErr: false,
		},
		{
			name:     "test.2 error, nil parser",
			parser:   nil,
			analyzer: analyzer,
			want:     nil,
			wantErr:  true,
		},
		{
			name:     "test.3 error, nil analyzer",
			parser:   parser,
			analyzer: nil,
			want:     nil,
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.parser, tt.analyzer)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCompute_HandleQuery(t *testing.T) {
	ctx := context.Background()

	qOK := "GET foo"
	tokensOK := []string{"GET", "foo"}
	queryOK := entity.Query{
		CID:  entity.GetCommandID,
		Args: []string{"foo"},
	}

	qOK2 := "GET zoo"
	tokensOK2 := []string{"GET", "zoo"}

	qErr := "GET par1"

	parser := cmock.NewMockParser(t)
	parser.EXPECT().ParseQuery(ctx, qOK).Return(tokensOK, nil)
	parser.EXPECT().ParseQuery(ctx, qOK2).Return(tokensOK2, nil)
	parser.EXPECT().ParseQuery(ctx, qErr).Return(nil, errors.New("some error"))

	an := cmock.NewMockAnalyzer(t)
	an.EXPECT().Analyze(ctx, tokensOK).Return(queryOK, nil)
	an.EXPECT().Analyze(ctx, tokensOK2).Return(entity.Query{}, errors.New("some error"))

	tests := []struct {
		name     string
		parser   Parser
		analyzer Analyzer
		query    string
		want     entity.Query
		wantErr  bool
	}{
		{
			name:     "test.1 ok",
			parser:   parser,
			analyzer: an,
			query:    qOK,
			want:     queryOK,
			wantErr:  false,
		},
		{
			name:     "test.2 error analyzer",
			parser:   parser,
			analyzer: an,
			query:    qOK2,
			want:     entity.Query{},
			wantErr:  true,
		},
		{
			name:     "test.3 error parser",
			parser:   parser,
			analyzer: an,
			query:    qErr,
			want:     entity.Query{},
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, _ := New(tt.parser, tt.analyzer)

			got, err := c.HandleQuery(ctx, tt.query)
			if (err != nil) != tt.wantErr {
				t.Errorf("Compute.HandleQuery() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Compute.HandleQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}
