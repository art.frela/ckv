package analyzer

import (
	"context"
	"errors"

	"gitlab.com/art.frela/ckv/internal/entity"
	"gitlab.com/art.frela/ckv/internal/usecase/compute"

	"go.uber.org/zap"
)

var _ compute.Analyzer = (*Analyzer)(nil)

const (
	setQueryArgumentsNumber = 2
	getQueryArgumentsNumber = 1
	delQueryArgumentsNumber = 1
)

var queryArgumentsNumber = map[entity.CommandID]int{
	entity.SetCommandID: setQueryArgumentsNumber,
	entity.GetCommandID: getQueryArgumentsNumber,
	entity.DelCommandID: delQueryArgumentsNumber,
}

var (
	errInvalidCommand   = errors.New("invalid command")
	errInvalidArguments = errors.New("invalid arguments")
)

type Analyzer struct {
	log *zap.SugaredLogger
}

func New(logger *zap.SugaredLogger) (*Analyzer, error) {
	if logger == nil {
		return nil, errors.New("empty logger")
	}

	return &Analyzer{log: logger}, nil
}

func (a *Analyzer) Analyze(ctx context.Context, tokens []string) (entity.Query, error) {
	txID := entity.FromCTX(ctx)

	if len(tokens) <= 1 {
		a.log.With("tx", txID).Debug("invalid query")
		return entity.Query{}, errInvalidCommand
	}

	command := tokens[0]
	commandID := entity.ParseCommand(command)

	if commandID == entity.UnknownCommandID {
		a.log.With("tx", txID, "command", command).Debug("invalid command")
		return entity.Query{}, errInvalidCommand
	}

	query := entity.NewQuery(commandID, tokens[1:])
	argumentsNumber := queryArgumentsNumber[commandID]

	if len(query.Arguments()) != argumentsNumber {
		a.log.With("tx", txID, "args", query.Arguments()).Debug("invalid arguments for query")
		return entity.Query{}, errInvalidArguments
	}

	a.log.With("tx", txID, "query", query).Debug("query analyzed")

	return query, nil
}
