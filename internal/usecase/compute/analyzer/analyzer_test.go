package analyzer

import (
	"context"
	"reflect"
	"testing"

	"gitlab.com/art.frela/ckv/internal/entity"
	"go.uber.org/zap"
)

func TestNew(t *testing.T) {
	log := zap.NewNop().Sugar()

	tests := []struct {
		name    string
		logger  *zap.SugaredLogger
		want    *Analyzer
		wantErr bool
	}{
		{
			name:   "test.1 ok",
			logger: log,
			want: &Analyzer{
				log: log,
			},
			wantErr: false,
		},
		{
			name:    "test.2 error",
			logger:  nil,
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.logger)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAnalyzer_Analyze(t *testing.T) {
	log := zap.NewNop().Sugar()

	tests := []struct {
		name    string
		tokens  []string
		want    entity.Query
		wantErr bool
	}{
		{
			name:   "test.1 ok SET(foo, bar)",
			tokens: []string{"SET", "foo", "bar"},
			want: entity.Query{
				Args: []string{"foo", "bar"},
				CID:  entity.SetCommandID,
			},
			wantErr: false,
		},
		{
			name:    "test.2 err SET(foo) incorrect args",
			tokens:  []string{"SET", "foo"},
			want:    entity.Query{},
			wantErr: true,
		},
		{
			name:    "test.3 err UPD(foo) incorrect command",
			tokens:  []string{"UPD", "foo"},
			want:    entity.Query{},
			wantErr: true,
		},
		{
			name:    "test.4 err UPD() incorrect args length",
			tokens:  []string{"UPD"},
			want:    entity.Query{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a, _ := New(log)

			got, err := a.Analyze(context.Background(), tt.tokens)
			if (err != nil) != tt.wantErr {
				t.Errorf("Analyzer.Analyze() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Analyzer.Analyze() = %v, want %v", got, tt.want)
			}
		})
	}
}
