package usecase

import "context"

type Interface interface {
	HandleQuery(ctx context.Context, queryStr string) string
}
