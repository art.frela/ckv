package tcp

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/art.frela/ckv/internal/usecase"
	umock "gitlab.com/art.frela/ckv/internal/usecase/mocks"
	"go.uber.org/zap"
)

func TestWithLogger(t *testing.T) {
	logger := zap.NewNop().Sugar()

	tests := []struct {
		name    string
		logger  *zap.SugaredLogger
		wantErr bool
	}{
		{
			"test.1 success",
			logger,
			false,
		},
		{
			"test.2 error",
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var opts options
			apply := WithLogger(tt.logger)
			if apply == nil {
				t.Error("WithLogger got nil")
				return
			}

			if err := apply(&opts); (err != nil) != tt.wantErr {
				t.Errorf("WithLogger()(opts) error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(opts.logger, tt.logger) {
				t.Errorf("WithLogger()(opts) opts.logger = %v, want %v", opts.logger, tt.logger)
			}
		})
	}
}

func TestWithUC(t *testing.T) {
	uc := umock.NewMockInterface(t)

	tests := []struct {
		name    string
		uc      usecase.Interface
		wantErr bool
	}{
		{
			"test.1 success",
			uc,
			false,
		},
		{
			"test.2 error",
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var opts options
			apply := WithUC(tt.uc)
			if apply == nil {
				t.Error("WithUC got nil")
				return
			}

			if err := apply(&opts); (err != nil) != tt.wantErr {
				t.Errorf("WithUC()(opts) error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(opts.uc, tt.uc) {
				t.Errorf("WithUC()(opts) opts.uc = %v, want %v", opts.uc, tt.uc)
			}
		})
	}
}

func TestWithMaxConnectionNumber(t *testing.T) {
	tests := []struct {
		name    string
		num     int
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			num:     42,
			wantErr: false,
		},
		{
			name:    "test.2 error",
			num:     -42,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var opts options
			apply := WithMaxConnectionNumber(tt.num)
			if apply == nil {
				t.Error("WithMaxConnectionNumber got nil")
				return
			}

			if err := apply(&opts); (err != nil) != tt.wantErr {
				t.Errorf("WithMaxConnectionNumber()(opts) error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.wantErr {
				return
			}

			if !reflect.DeepEqual(opts.maxConnectionsNumber, tt.num) {
				t.Errorf("WithMaxConnectionNumber()(opts) opts.uc = %v, want %v", opts.maxConnectionsNumber, tt.num)
			}
		})
	}
}

func TestWithAddress(t *testing.T) {
	tests := []struct {
		name    string
		address string
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			address: "host:8080",
			wantErr: false,
		},
		{
			name:    "test.2 err",
			address: "http://host:p",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var opts options
			apply := WithAddress(tt.address)
			if apply == nil {
				t.Error("WithAddress got nil")
				return
			}

			if err := apply(&opts); (err != nil) != tt.wantErr {
				t.Errorf("WithAddress()(opts) error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.wantErr {
				return
			}

			if !reflect.DeepEqual(opts.address, tt.address) {
				t.Errorf("WithAddress()(opts) opts.address = %v, want %v", opts.address, tt.address)
			}
		})
	}
}

func TestWithTimeout(t *testing.T) {
	tests := []struct {
		name    string
		timeout time.Duration
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			timeout: time.Second,
			wantErr: false,
		},
		{
			name:    "test.2 error",
			timeout: -time.Second,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var opts options
			apply := WithTimeout(tt.timeout)
			if apply == nil {
				t.Error("WithTimeout got nil")
				return
			}

			if err := apply(&opts); (err != nil) != tt.wantErr {
				t.Errorf("WithTimeout()(opts) error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.wantErr {
				return
			}

			if !reflect.DeepEqual(opts.idleTimeout, tt.timeout) {
				t.Errorf("WithTimeout()(opts) opts.idleTimeout = %v, want %v", opts.idleTimeout, tt.timeout)
			}
		})
	}
}

func TestWithMessageSize(t *testing.T) {
	tests := []struct {
		name    string
		size    int
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			size:    minMessageSize * 2,
			wantErr: false,
		},
		{
			name:    "test.2 error",
			size:    minMessageSize / 2,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var opts options
			apply := WithMessageSize(tt.size)
			if apply == nil {
				t.Error("WithMessageSize got nil")
				return
			}

			if err := apply(&opts); (err != nil) != tt.wantErr {
				t.Errorf("WithMessageSize()(opts) error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.wantErr {
				return
			}

			if !reflect.DeepEqual(opts.messageSize, tt.size) {
				t.Errorf("WithMessageSize()(opts) opts.messageSize = %v, want %v", opts.messageSize, tt.size)
			}
		})
	}
}

func Test_options_validate(t *testing.T) {
	logger := zap.NewNop().Sugar()
	uc := umock.NewMockInterface(t)

	tests := []struct {
		name    string
		opts    []Option
		wantErr bool
	}{
		{
			name: "test.1 ok",
			opts: []Option{
				WithLogger(logger),
				WithUC(uc),
				WithTimeout(time.Second),
				WithMaxConnectionNumber(42),
			},
			wantErr: false,
		},
		{
			name: "test.2 err miss timeout ",
			opts: []Option{
				WithLogger(logger),
				WithUC(uc),
				WithMaxConnectionNumber(42),
			},
			wantErr: true,
		},
		{
			name: "test.3 err miss uc",
			opts: []Option{
				WithLogger(logger),
				WithMaxConnectionNumber(42),
			},
			wantErr: true,
		},
		{
			name: "test.4 err miss maxConnectionNum",
			opts: []Option{
				WithLogger(logger),
			},
			wantErr: true,
		},
		{
			name:    "test.5 err miss logger",
			opts:    []Option{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := options{}
			for _, apply := range tt.opts {
				if err := apply(&o); err != nil {
					t.Errorf("apply option error: %s", err)
					return
				}
			}

			if err := o.validate(); (err != nil) != tt.wantErr {
				t.Errorf("options.validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
