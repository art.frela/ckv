package tcp

import (
	"context"
	"errors"
	"net"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	tmock "gitlab.com/art.frela/ckv/internal/controller/tcp/mocks"
	umock "gitlab.com/art.frela/ckv/internal/usecase/mocks"

	"go.uber.org/goleak"
	"go.uber.org/zap"
)

const testTimeout = time.Second * 30

func TestNewServer(t *testing.T) {
	defer goleak.VerifyNone(t)

	logger := zap.NewNop().Sugar()
	uc := umock.NewMockInterface(t)

	tests := []struct {
		name    string
		opts    []Option
		wantErr bool
	}{
		{
			name: "test.1 ok",
			opts: []Option{
				WithLogger(logger),
				WithUC(uc),
				WithTimeout(time.Second),
				WithMaxConnectionNumber(42),
			},
			wantErr: false,
		},
		{
			name: "test.2 err apply",
			opts: []Option{
				WithLogger(nil),
				WithUC(nil),
				WithTimeout(-time.Second),
				WithMaxConnectionNumber(-42),
			},
			wantErr: true,
		},
		{
			name: "test.3 err miss logger",
			opts: []Option{
				WithUC(uc),
				WithTimeout(time.Second),
				WithMaxConnectionNumber(42),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewServer(tt.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewServer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestServerClient_Run(t *testing.T) {
	defer goleak.VerifyNone(t)

	ctx, cancel := context.WithTimeout(context.Background(), testTimeout)
	defer cancel()

	logger := zap.NewNop().Sugar()
	maxSize := 4096

	q1ok := "SET foo bar"

	uc := umock.NewMockInterface(t)
	uc.EXPECT().HandleQuery(ctx, q1ok).Return("[ok]")

	tests := []struct {
		name         string
		opts         []Option
		request      []byte
		wantResponse []byte
		wantErrCli   bool
		wantErr      bool
	}{
		{
			name: "test.1 ok",
			opts: []Option{
				WithLogger(logger),
				WithUC(uc),
				WithTimeout(time.Second),
				WithMaxConnectionNumber(42),
				WithMessageSize(maxSize),
			},
			request:      []byte(q1ok),
			wantResponse: []byte("[ok]"),
			wantErrCli:   false,
			wantErr:      false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, _ := NewServer(tt.opts...)

			shutdown, err := s.Run(ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("Server.Run() error = %v, wantErr %v", err, tt.wantErr)
			}
			defer shutdown()

			time.Sleep(time.Millisecond * 500)
			s.mu.Lock()
			listenHostPort := s.address
			s.mu.Unlock()

			connection, err := net.Dial("tcp", listenHostPort)
			if err != nil {
				t.Errorf("failed to dial: %v", err)
				return
			}

			cli, err := NewClient(connection, maxSize, time.Second)
			if (err != nil) != tt.wantErrCli {
				t.Errorf("NewClient() got rror: %s", err)
				return
			}

			resp, err := cli.Send(tt.request)
			if err != nil {
				t.Errorf("NewClient() got rror: %s", err)
				return
			}

			if !reflect.DeepEqual(resp, tt.wantResponse) {
				t.Errorf("cli.Send() got response = %v, want %v", string(resp), string(tt.wantResponse))
			}
		})
	}

	time.Sleep(time.Millisecond * 250) //
}

func TestServer_handleConnection(t *testing.T) {
	defer goleak.VerifyNone(t)

	ctx, cancel := context.WithTimeout(context.Background(), testTimeout)
	defer cancel()

	logger := zap.NewNop().Sugar()
	maxSize := 4096

	// q1ok := "SET foo bar"

	uc := umock.NewMockInterface(t)
	uc.EXPECT().HandleQuery(ctx, mock.AnythingOfType("string")).Return("[ok]")

	connSetDLErr := tmock.NewMockConn(t)
	connSetDLErr.EXPECT().SetDeadline(mock.AnythingOfType("time.Time")).Return(errors.New("some error"))
	connSetDLErr.EXPECT().Close().Return(errors.New("some error"))
	connSetDLErr.EXPECT().RemoteAddr().Return(&net.TCPAddr{})

	connReadErr := tmock.NewMockConn(t)
	connReadErr.EXPECT().SetDeadline(mock.AnythingOfType("time.Time")).Return(nil)
	connReadErr.EXPECT().Read(mock.AnythingOfType("[]uint8")).Return(0, errors.New("some error"))
	connReadErr.EXPECT().Close().Return(nil)
	connReadErr.EXPECT().RemoteAddr().Return(&net.TCPAddr{})

	connWriteErr := tmock.NewMockConn(t)
	connWriteErr.EXPECT().SetDeadline(mock.AnythingOfType("time.Time")).Return(nil)
	connWriteErr.EXPECT().Read(mock.AnythingOfType("[]uint8")).Run(func(b []byte) {
		bar := []byte("bar")
		copy(b, bar)
	}).Return(3, nil)
	connWriteErr.EXPECT().Write([]byte("[ok]")).Return(0, errors.New("some error"))
	connWriteErr.EXPECT().Close().Return(nil)
	connWriteErr.EXPECT().RemoteAddr().Return(&net.TCPAddr{})

	tests := []struct {
		name       string
		opts       []Option
		connection net.Conn
	}{
		{
			name: "test.1 set deadline error and close error",
			opts: []Option{
				WithLogger(logger),
				WithUC(uc),
				WithTimeout(time.Second),
				WithMaxConnectionNumber(42),
				WithMessageSize(maxSize),
			},
			connection: connSetDLErr,
		},
		{
			name: "test.2 read error",
			opts: []Option{
				WithLogger(logger),
				WithUC(uc),
				WithTimeout(time.Second),
				WithMaxConnectionNumber(42),
				WithMessageSize(maxSize),
			},
			connection: connReadErr,
		},
		{
			name: "test.3 write error",
			opts: []Option{
				WithLogger(logger),
				WithUC(uc),
				WithTimeout(time.Second),
				WithMaxConnectionNumber(42),
				WithMessageSize(maxSize),
			},
			connection: connWriteErr,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, _ := NewServer(tt.opts...)

			s.handleConnection(ctx, tt.connection)
		})
	}
}
