package tcp

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"sync"
	"time"

	"gitlab.com/art.frela/ckv/internal/usecase"
	"gitlab.com/art.frela/ckv/pkg/semafor"

	"go.uber.org/zap"
)

const (
	countTCPConnHandlers = 2
	shutdownTimeout      = time.Second * 10
)

type Server struct {
	uc          usecase.Interface
	semaphore   *semafor.Semaphore
	log         *zap.SugaredLogger
	mu          *sync.Mutex
	address     string
	idleTimeout time.Duration
	messageSize int
}

func NewServer(opts ...Option) (*Server, error) {
	ops := options{}
	for _, apply := range opts {
		if err := apply(&ops); err != nil {
			return nil, fmt.Errorf("apply option: %w", err)
		}
	}

	if err := ops.validate(); err != nil {
		return nil, fmt.Errorf("options validate: %w", err)
	}

	ops.logger.Debugf("got options: %+v", ops)

	return &Server{
		uc:          ops.uc,
		mu:          &sync.Mutex{},
		address:     ops.address,
		semaphore:   semafor.New(ops.maxConnectionsNumber),
		idleTimeout: ops.idleTimeout,
		messageSize: ops.messageSize,
		log:         ops.logger,
	}, nil
}

// Run opens tcp port and handle connections,
// returns shutdown func and error.
func (s *Server) Run(ctx context.Context) (func() error, error) {
	listener, err := net.Listen("tcp", s.address)
	if err != nil {
		return nil, fmt.Errorf("failed to listen: %w", err)
	}

	s.mu.Lock()
	s.address = listener.Addr().String()
	s.mu.Unlock()

	s.log.Infof("start tcp server on %s", s.address)

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()

		for {
			connection, err := listener.Accept()
			if err != nil {
				if errors.Is(err, net.ErrClosed) {
					return
				}

				s.log.Error("failed to accept", zap.Error(err))
				continue
			}

			wg.Add(1)
			go func(connection net.Conn) {
				s.semaphore.Acquire()

				defer func() {
					s.semaphore.Release()
					wg.Done()
				}()

				s.handleConnection(ctx, connection)
			}(connection)
		}
	}()

	shutdown := func() error {
		err := listener.Close()

		done := make(chan struct{})

		ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()

		go func() {
			wg.Wait() // wait all connection handlers
			close(done)
		}()

		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-done:
			return err
		}
	}

	return shutdown, nil
}

func (s *Server) handleConnection(ctx context.Context, connection net.Conn) {
	request := make([]byte, s.messageSize)

	for {
		if err := connection.SetDeadline(time.Now().Add(s.idleTimeout)); err != nil {
			s.log.Warn("failed to set read deadline", zap.Error(err))
			break
		}

		count, err := connection.Read(request)
		if err != nil {
			if !errors.Is(err, io.EOF) {
				s.log.Warn("failed to read", err)
			}

			break
		}

		response := s.handler(ctx, request[:count])
		if _, err := connection.Write(response); err != nil {
			s.log.Warn("failed to write", zap.Error(err))
			break
		}
	}

	if err := connection.Close(); err != nil {
		s.log.Warn("failed to close connection", zap.Error(err))
	}

	s.log.Debugf("connection %q closed", connection.RemoteAddr().String())
}

func (s *Server) handler(ctx context.Context, request []byte) []byte {
	response := s.uc.HandleQuery(ctx, string(request))
	return []byte(response)
}
