package tcp

import (
	"errors"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/art.frela/ckv/internal/usecase"

	"go.uber.org/zap"
)

const minMessageSize = 5

type options struct {
	logger               *zap.SugaredLogger
	uc                   usecase.Interface
	address              string
	idleTimeout          time.Duration
	messageSize          int
	maxConnectionsNumber int
}

func (o options) validate() error {
	if o.logger == nil {
		return errors.New("empty logger")
	}

	if o.maxConnectionsNumber <= 0 {
		return errors.New("invalid number of max connections")
	}

	if o.uc == nil {
		return errors.New("empty usecase")
	}

	if o.idleTimeout <= 0 {
		return errors.New("invalid idle timeout")
	}

	return nil
}

type Option func(*options) error

func WithLogger(log *zap.SugaredLogger) Option {
	return func(o *options) error {
		if log == nil {
			return errors.New("empty logger")
		}

		o.logger = log
		return nil
	}
}

func WithUC(uc usecase.Interface) Option {
	return func(o *options) error {
		if uc == nil {
			return errors.New("empty usecase")
		}

		o.uc = uc
		return nil
	}
}

func WithMaxConnectionNumber(num int) Option {
	return func(o *options) error {
		if num <= 0 {
			return errors.New("invalid number of max connections")
		}

		o.maxConnectionsNumber = num
		return nil
	}
}

func WithAddress(address string) Option {
	return func(o *options) error {
		if _, err := url.Parse(address); err != nil {
			return errors.New("incorrect address")
		}

		o.address = address
		return nil
	}
}

func WithTimeout(timeout time.Duration) Option {
	return func(o *options) error {
		if timeout < 0 {
			return errors.New("timout is negative")
		}

		o.idleTimeout = timeout
		return nil
	}
}

func WithMessageSize(size int) Option {
	return func(o *options) error {
		if size < minMessageSize {
			return fmt.Errorf("incorrect size %d, need %d and more", size, minMessageSize)
		}

		o.messageSize = size
		return nil
	}
}
