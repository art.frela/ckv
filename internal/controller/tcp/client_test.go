package tcp

import (
	"context"
	"errors"
	"log"
	"net"
	"reflect"
	"sync"
	"testing"
	"time"

	tmock "gitlab.com/art.frela/ckv/internal/controller/tcp/mocks"

	"github.com/stretchr/testify/mock"
)

type mockTCPServer struct {
	mu       *sync.Mutex
	listener net.Listener
}

func (ms *mockTCPServer) run(ctx context.Context) {
	listener, err := net.Listen("tcp", "") // dynamic
	if err != nil {
		log.Fatalf("listen mock tcp server: %s", err)
	}

	ms.mu.Lock()
	ms.listener = listener
	ms.mu.Unlock()

	connection, err := listener.Accept()
	if err != nil {
		if errors.Is(err, net.ErrClosed) {
			return
		}
		log.Fatalf("accept error: %s", err)
	}
	defer connection.Close()

	<-ctx.Done()
}

func (ms *mockTCPServer) address() string {
	ms.mu.Lock()
	defer ms.mu.Unlock()

	return ms.listener.Addr().String()
}

func TestNewClient(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), testTimeout)
	defer cancel()

	ms := &mockTCPServer{mu: &sync.Mutex{}}
	go func() {
		ms.run(ctx)
	}()
	time.Sleep(time.Millisecond * 250)

	connection, err := net.Dial("tcp", ms.address())
	if err != nil {
		t.Fatalf("failed to dial: %v", err)
	}

	tests := []struct {
		name           string
		conn           net.Conn
		maxMessageSize int
		idleTimeout    time.Duration
		wantErr        bool
	}{
		{
			name:           "test.1 ok dial",
			conn:           connection,
			maxMessageSize: 4096,
			idleTimeout:    time.Second,
			wantErr:        false,
		},
		{
			name:           "test.2 err dial",
			conn:           nil,
			maxMessageSize: 4096,
			idleTimeout:    time.Second,
			wantErr:        true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewClient(tt.conn, tt.maxMessageSize, tt.idleTimeout)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewClient() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestClient_Send(t *testing.T) {
	idleTimeoutOK := time.Second * 7
	idleTimeoutErr := time.Second * 47
	requestOK := []byte("GET foo")
	maxMessageSizeOK := 4096
	responseOK := make([]byte, maxMessageSizeOK)

	requestErr := []byte("SET foo bar")

	conn := tmock.NewMockConn(t)
	conn.EXPECT().SetDeadline(mock.AnythingOfType("time.Time")).Return(nil)
	conn.EXPECT().Write(requestOK).Return(len(requestOK), nil)
	conn.EXPECT().Read(responseOK).Run(func(b []byte) {
		bar := []byte("bar")
		copy(b, bar)
	}).Return(3, nil)
	conn.EXPECT().Write(requestErr).Return(0, errors.New("some error"))

	connErrSetDL := tmock.NewMockConn(t)
	connErrSetDL.EXPECT().SetDeadline(mock.AnythingOfType("time.Time")).Return(errors.New("some error"))

	connReadErr := tmock.NewMockConn(t)
	connReadErr.EXPECT().SetDeadline(mock.AnythingOfType("time.Time")).Return(nil)
	connReadErr.EXPECT().Write(requestOK).Return(len(requestOK), nil)
	connReadErr.EXPECT().Read(responseOK).Return(0, errors.New("some error"))

	tests := []struct {
		name           string
		connection     net.Conn
		maxMessageSize int
		idleTimeout    time.Duration
		request        []byte
		want           []byte
		wantErr        bool
	}{
		{
			name:           "test.1 ok",
			connection:     conn,
			maxMessageSize: maxMessageSizeOK,
			idleTimeout:    idleTimeoutOK,
			request:        requestOK,
			want:           []byte("bar"),
			wantErr:        false,
		},
		{
			name:           "test.2 set deadline error",
			connection:     connErrSetDL,
			maxMessageSize: maxMessageSizeOK,
			idleTimeout:    idleTimeoutErr,
			request:        requestOK,
			want:           nil,
			wantErr:        true,
		},
		{
			name:           "test.3 write error",
			connection:     conn,
			maxMessageSize: maxMessageSizeOK,
			idleTimeout:    idleTimeoutErr,
			request:        requestErr,
			want:           nil,
			wantErr:        true,
		},
		{
			name:           "test.4 read error",
			connection:     connReadErr,
			maxMessageSize: maxMessageSizeOK,
			idleTimeout:    idleTimeoutErr,
			request:        requestOK,
			want:           nil,
			wantErr:        true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Client{
				connection:     tt.connection,
				maxMessageSize: tt.maxMessageSize,
				idleTimeout:    tt.idleTimeout,
			}

			got, err := c.Send(tt.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("Client.Send() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Client.Send() = %v, want %v", got, tt.want)
			}
		})
	}
}
