# ckv - inmemoy k/v database

> Developed as homework by [Balun.Courses.Go.Concurrency](https://balun.courses/courses/concurrency)

![schema layers](assets/img/layers_01.png)

request syntax

```ebnf
query = set_command | get_command | del_command

set_command = "SET" argument argument
get_command = "GET" argument
del_command = "DEL" argument
argument    = punctuation | letter | digit { punctuation | letter | digit }

punctuation = "*" | "/" | "_"
letter      = "a" | ... | "z" | "A" | ... | "Z"
digit       = "0" | ... | "9"
```

request example

```bash
SET weather_2_pm cold_moscow_weather
GET /etc/nginx/config.yaml
DEL user_****
```

## status

**WIP**

## getting started

* [Install go >=1.22](https://go.dev/doc/install)
* Install git
* clone repo `git clone git@gitlab.com:art.frela/ckv.git`
* cd to ckv `cd /path/to/ckv`
* and run test `make test`

## build

`make bin`

makes bin file `./bin/ckv` - database server and `./bin/cli` - cli client

## run

server:

```bash
CKV_CONFIG_FILE=./configs/ckv.yaml ./bin/ckv
# logs output
{"severity":"DEBUG","timestamp":"2024-05-10T23:53:02.121998+03:00","caller":"tcp/server.go:42","message":"got options: {logger:0xc00011e198 uc:0xc000044060 address:localhost:3000 idleTimeout:600000000000 messageSize:4096 maxConnectionsNumber:100}"}
{"severity":"INFO","timestamp":"2024-05-10T23:53:02.124699+03:00","caller":"tcp/server.go:66","message":"start tcp server on 127.0.0.1:3000"}
```

client:

```bash
./bin/cli
[ckv] > SET foo bar
# output
[ok]
[ckv] > GET foo
# output
bar
[ckv] > ^C
```

## environment

only OS

## config

see JSONSchema `internal/config/schema.v1.json`

example:

```toml
[engine]
  kind = "inmemory"       
[logger]
  level = "debug"
  development = false
[network]
  address = "localhost:3000"
  max_connections = 100
  max_message_size = "4KB"
  idle_timeout = "10m"
```

