// Package fsm implements final state machine mechanism.
package fsm

import (
	"reflect"
	"testing"
	"unicode/utf8"
)

func TestMachine_Parse(t *testing.T) {
	m := New(InitialState)

	// std flow
	m.AddTransition(InitialState, LetterEv, WordState, m.AppendSymbolToToken)
	m.AddTransition(InitialState, WhiteSpaceEv, WhiteSpaceState, nil)

	m.AddTransition(WordState, LetterEv, WordState, m.AppendSymbolToToken)
	m.AddTransition(WordState, WhiteSpaceEv, WhiteSpaceState, m.AppendToken)
	m.AddTransition(WhiteSpaceState, LetterEv, WordState, m.AppendSymbolToToken)
	m.AddTransition(WhiteSpaceState, WhiteSpaceEv, WhiteSpaceState, nil)

	tests := []struct {
		name    string
		query   string
		want    []string
		wantErr bool
	}{
		{
			name:    "test.1 empty string",
			query:   "",
			want:    []string{},
			wantErr: false,
		},
		{
			name:    "test.2 tree english words with digits",
			query:   "set key123 value456value",
			want:    []string{"set", "key123", "value456value"},
			wantErr: false,
		},
		{
			name:    "test.3 tree russian words with digits",
			query:   "установить ключ321 значение42",
			want:    []string{"установить", "ключ321", "значение42"},
			wantErr: false,
		},
		{
			name:    "test.4 11 mix ru/en words with digits",
			query:   "SET key1 значение1 key2 значение2 key3 значение3 key4 значение4 key5 значение5",
			want:    []string{"SET", "key1", "значение1", "key2", "значение2", "key3", "значение3", "key4", "значение4", "key5", "значение5"},
			wantErr: false,
		},
		{
			name:    "test.5 2 word with undefined symbol",
			query:   "SET Key✈⌘",
			want:    []string{"SET"},
			wantErr: true,
		},
		{
			name:    "test.6 1 word with undefined symbol",
			query:   "Key✈⌘",
			want:    []string{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m.tokens = make([]string, 0)
			m.sb.Reset()

			got, err := m.Parse(tt.query)
			if (err != nil) != tt.wantErr {
				t.Errorf("Machine.Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Machine.Parse() = %+#v, want %+#v", got, tt.want)
			}
		})
	}
}

func BenchmarkParse(b *testing.B) {
	m := New(InitialState)

	m.AddTransition(InitialState, LetterEv, WordState, m.AppendSymbolToToken)
	m.AddTransition(InitialState, WhiteSpaceEv, WhiteSpaceState, nil)

	m.AddTransition(WordState, LetterEv, WordState, m.AppendSymbolToToken)
	m.AddTransition(WordState, WhiteSpaceEv, WhiteSpaceState, m.AppendToken)
	m.AddTransition(WhiteSpaceState, LetterEv, WordState, m.AppendSymbolToToken)
	m.AddTransition(WhiteSpaceState, WhiteSpaceEv, WhiteSpaceState, nil)

	b.ResetTimer()

	query := `word1 word1 word1 word1 word1 word1	word1	word1	word1
	word1
	word1
	word1 word1word1word1 word1 word1 word1 word1 
	word1 word1 word1 word1 word1 word1	word1	word1	word1
	word1
	word1
	word1 word1word1word1 word1 word1 word1 word1 
	word1 word1 word1 word1 word1 word1	word1	word1	word1
	word1
	word1
	word1 word1word1word1 word1 word1 word1 word1 
	word1 word1 word1 word1 word1 word1	word1	word1	word1
	word1
	word1
	word1 word1word1word1 word1 word1 word1 word1   
	`

	for i := 0; i < b.N; i++ {
		m.tokens = make([]string, 0)
		m.sb.Reset()
		_, err := m.Parse(query)
		if err != nil {
			b.Errorf("Machine.Parse() error = %v, wantErr nil", err)
			return
		}
	}
}

/*
my:
BenchmarkParse-4   	   42198	     28063 ns/op	    2768 B/op	      79 allocs/op
v.balun
BenchmarkParse-4   	  155598	      7575 ns/op	    2768 B/op	      79 allocs/op
*/

func Test_isLetter(t *testing.T) {
	ra, _ := utf8.DecodeRuneInString("a")
	rA, _ := utf8.DecodeRuneInString("A")
	rz, _ := utf8.DecodeRuneInString("z")
	rZ, _ := utf8.DecodeRuneInString("Z")
	r0, _ := utf8.DecodeRuneInString("0")
	rRUa, _ := utf8.DecodeRuneInString("а")
	rRUA, _ := utf8.DecodeRuneInString("А")
	rAsterisk, _ := utf8.DecodeRuneInString("*")
	rSlash, _ := utf8.DecodeRuneInString("/")
	rUnderscore, _ := utf8.DecodeRuneInString("_")
	rAT, _ := utf8.DecodeRuneInString("@")
	rYo, _ := utf8.DecodeRuneInString("!")
	rSpace, _ := utf8.DecodeRuneInString(" ")

	tests := []struct {
		name string
		r    rune
		want bool
	}{
		{
			"test.1 a",
			ra,
			true,
		},
		{
			"test.2 A",
			rA,
			true,
		},
		{
			"test.3 z",
			rz,
			true,
		},
		{
			"test.4 Z",
			rZ,
			true,
		},
		{
			"test.5 0",
			r0,
			true,
		},
		{
			"test.6 RU-a",
			rRUa,
			true,
		},
		{
			"test.7 RU A",
			rRUA,
			true,
		},
		{
			"test.8 *",
			rAsterisk,
			true,
		},
		{
			"test.9 /",
			rSlash,
			true,
		},
		{
			"test.10 _",
			rUnderscore,
			true,
		},
		{
			"test.11 @",
			rAT,
			false,
		},
		{
			"test.12 !",
			rYo,
			false,
		},
		{
			"test.13 whiteSpace",
			rSpace,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isLetter(tt.r); got != tt.want {
				t.Errorf("isLetter() = %v, want %v", got, tt.want)
			}
		})
	}
}
