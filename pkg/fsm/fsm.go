// Package fsm implements final state machine mechanism.
package fsm

import (
	"errors"
	"strings"
)

type State uint8

const (
	InitialState State = iota
	WordState
	WhiteSpaceState
	InvalidState
)

type Event uint8

const (
	LetterEv Event = iota
	WhiteSpaceEv
	UndefinedEv
)

type Transition struct {
	Action    func(rune)
	NextState State
}

type Machine struct {
	tokens       []string
	transitions  map[State]map[Event]Transition
	sb           strings.Builder
	currentState State
}

func New(initState State) *Machine {
	return &Machine{
		tokens:       make([]string, 0),
		transitions:  make(map[State]map[Event]Transition),
		currentState: initState,
	}
}

func (m *Machine) AddTransition(fromState State, e Event, toState State, action func(r rune)) {
	fs, ok := m.transitions[fromState]
	if ok {
		fs[e] = Transition{NextState: toState, Action: action}
		m.transitions[fromState] = fs
		return
	}

	newT := map[Event]Transition{
		e: {
			NextState: toState,
			Action:    action,
		},
	}

	m.transitions[fromState] = newT
}

const spaceRune rune = 0x20

func (m *Machine) Parse(query string) ([]string, error) {
	for _, r := range query {
		e := parseRune(r)

		tokens, err := m.eventProcess(e, r)
		if err != nil {
			return tokens, err
		}
	}

	tokens, err := m.eventProcess(WhiteSpaceEv, spaceRune)
	m.sb.Reset()
	m.tokens = make([]string, 0)

	return tokens, err
}

func (m *Machine) eventProcess(e Event, r rune) ([]string, error) {
	et, ok := m.transitions[m.currentState]
	if !ok {
		return m.tokens, errors.New("undefined state")
	}

	t, ok := et[e]
	if !ok {
		return m.tokens, errors.New("undefined event")
	}

	if t.Action != nil {
		t.Action(r)
	}

	m.currentState = t.NextState

	return m.tokens, nil
}

func (m *Machine) AppendSymbolToToken(r rune) {
	//nolint:errcheck
	m.sb.WriteRune(r)
}

func (m *Machine) AppendToken(_ rune) {
	m.tokens = append(m.tokens, m.sb.String())
	m.sb.Reset()
}

func parseRune(r rune) Event {
	switch {
	case isWhiteSpace(r):
		return WhiteSpaceEv
	case isLetter(r):
		return LetterEv
	default:
		return UndefinedEv
	}
}

func isWhiteSpace(r rune) bool {
	return r == '\t' || r == '\n' || r == ' '
}

// isLetter pass en, ru letters and */_ symbols.
func isLetter(r rune) bool {
	return (r >= 'a' && r <= 'z') ||
		(r >= 'A' && r <= 'Z') ||
		(r >= 'а' && r <= 'я') ||
		(r >= 'А' && r <= 'Я') ||
		(r >= '0' && r <= '9') ||
		(r == '_') ||
		(r == '*') ||
		(r == '/')
}
