package datasize

import "testing"

func TestParse(t *testing.T) {
	tests := []struct {
		name    string
		text    string
		want    int
		wantErr bool
	}{
		{
			name:    "test.1 ok 1GB",
			text:    "1GB",
			want:    1_073_741_824,
			wantErr: false,
		},
		{
			name:    "test.2 ok 1Mb",
			text:    "1Mb",
			want:    1_048_576,
			wantErr: false,
		},
		{
			name:    "test.3 ok 1kb",
			text:    "1kb",
			want:    1_024,
			wantErr: false,
		},
		{
			name:    "test.4 ok 1",
			text:    "1",
			want:    1,
			wantErr: false,
		},
		{
			name:    "test.5 err 1Zz",
			text:    "1Xyz",
			want:    0,
			wantErr: true,
		},
		{
			name:    "test.6 err empty",
			text:    "",
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Parse(tt.text)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Parse() = %v, want %v", got, tt.want)
			}
		})
	}
}
