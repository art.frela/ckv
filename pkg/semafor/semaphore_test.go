package semafor

import (
	"sync"
	"testing"
	"time"
)

func TestSemaphore_Acquire(t *testing.T) {
	tests := []struct {
		name    string
		tickets int
	}{
		{
			name:    "test.1 ok 1",
			tickets: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(tt.tickets)
			counter := 1
			for i := 0; i < tt.tickets; i++ {
				s.Acquire() // acquire all slots
			}

			waiter := make(chan struct{})
			// checks action exec after release
			wg := &sync.WaitGroup{}
			wg.Add(1)
			go func() {
				defer wg.Done()

				// wait for goroutine execute
				<-waiter

				s.WithSemaphore(func() {
					counter++
				})
			}()

			waiter <- struct{}{}

			time.Sleep(time.Millisecond * 500)

			//
			if counter != 1 {
				t.Errorf("s.WithSemaphore execute before release, counter = %d, want 1", counter)
			}

			s.Release()

			wg.Wait()

			if counter != 2 {
				t.Errorf("s.WithSemaphore execute after release, counter = %d, want 2", counter)
			}
		})
	}
}

func TestSemaphore_WithSemaphore(t *testing.T) {
	tests := []struct {
		name   string
		action func()
	}{
		{
			name: "test.1 ok",
			action: func() {
				println("ok")
			},
		},
		{
			name:   "test.2 ok action nil",
			action: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(1)

			s.WithSemaphore(tt.action)
		})
	}
}
