package dotconf

import (
	"os"
	"reflect"
	"testing"
)

func TestDotConf_Set(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathPrefix   string
		pathToConfig string
		k            string
		env          map[string]string
		want         interface{}
	}{
		{"yaml_short", "", "", "testdata/yaml/config_short_2.yaml", "app.env1", nil, "info"},
		{"yaml_short_env_vvv", "", "", "testdata/yaml/config_short_2.yaml", "env.vvv", nil, "example"},
		{"yaml_short_object", "", "", "testdata/yaml/config_short_2.yaml", "properties.prop1", nil, map[string]interface{}{"a": "va"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)
			dc.SetPathPrefix(tt.pathPrefix)
			dc.Set(tt.k, tt.want)
			if got := dc.Get(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.Get() = %v, want %v", got, tt.want)
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func Test_makeMap(t *testing.T) {
	type args struct {
		key   string
		value interface{}
	}
	tests := []struct {
		name  string
		key   string
		value interface{}
		want  map[string]interface{}
	}{
		{
			"тест 1, a.b.c",
			"a.b.c",
			44,
			map[string]interface{}{
				"a": map[string]interface{}{
					"b": map[string]interface{}{
						"c": 44,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := makeMap(tt.key, tt.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("makeMap() = %v, want %v", got, tt.want)
			}
		})
	}
}
