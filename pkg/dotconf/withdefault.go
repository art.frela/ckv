package dotconf

import (
	"os"
	"strconv"
	"strings"
	"time"
)

// GetStringWithDefault extracts k-path value,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetStringWithDefault(k, def string) string {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		if !dc.cfg.Get(k).Exists() {
			return def
		}
		return os.ExpandEnv(dc.cfg.Get(k).String())
	}
	return os.ExpandEnv(envValue)
}

// GetIntWithDefault extracts k-path value as Int,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetIntWithDefault(k string, def int) int {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		if !dc.cfg.Get(k).Exists() {
			return def
		}
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.Atoi(os.ExpandEnv(v))
			return vi
		default:
			return int(dc.cfg.Get(k).Int())
		}
	}
	//nolint:errcheck
	vi, _ := strconv.Atoi(os.ExpandEnv(envValue))
	return vi
}

// GetInt32WithDefault extracts k-path value as Int32,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetInt32WithDefault(k string, def int32) int32 {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		if !dc.cfg.Get(k).Exists() {
			return def
		}
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.ParseInt(os.ExpandEnv(v), dec, x64)
			return int32(vi)
		default:
			return int32(dc.cfg.Get(k).Int())
		}
	}
	//nolint:errcheck
	vi, _ := strconv.ParseInt(os.ExpandEnv(envValue), dec, x64)
	return int32(vi)
}

// GetInt64WithDefault extracts k-path value as Int64,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetInt64WithDefault(k string, def int64) int64 {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		if !dc.cfg.Get(k).Exists() {
			return def
		}
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.ParseInt(os.ExpandEnv(v), dec, x64)
			return vi
		default:
			return dc.cfg.Get(k).Int()
		}
	}
	//nolint:errcheck
	vi, _ := strconv.ParseInt(os.ExpandEnv(envValue), dec, x64)
	return vi
}

// GetBoolWithDefault extracts k-path value as bool,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetBoolWithDefault(k string, def bool) bool {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		if !dc.cfg.Get(k).Exists() {
			return def
		}
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			bv, _ := strconv.ParseBool(os.ExpandEnv(v))
			return bv
		default:
			return dc.cfg.Get(k).Bool()
		}
	}
	//nolint:errcheck
	bv, _ := strconv.ParseBool(os.ExpandEnv(envValue))
	return bv
}

// GetDurationWithDefault extracts k-path value as time.Duration,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetDurationWithDefault(k string, def time.Duration) time.Duration {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		if !dc.cfg.Get(k).Exists() {
			return def
		}
		envValue = dc.cfg.Get(k).String()
	}
	//nolint:errcheck
	dur, err := time.ParseDuration(os.ExpandEnv(envValue))
	if err != nil {
		if strings.Contains(err.Error(), "missing unit in duration") {
			envValue += "ns"

			durenv, er := time.ParseDuration(envValue)
			if er != nil {
				return def
			}

			return durenv
		}

		return def
	}

	return dur
}

// GetFloat64WithDefault extracts k-path value as float64,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetFloat64WithDefault(k string, def float64) float64 {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		if !dc.cfg.Get(k).Exists() {
			return def
		}
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			fv, _ := strconv.ParseFloat(os.ExpandEnv(v), x64)
			return fv
		default:
			return dc.cfg.Get(k).Float()
		}
	}
	//nolint:errcheck
	fv, _ := strconv.ParseFloat(envValue, x64)
	return fv
}

// GetIntSliceWithDefault extracts k-path value as []int,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetIntSliceWithDefault(k string, def []int) []int {
	k = dc.pathPrefix + k
	if !dc.cfg.Get(k).Exists() {
		return def
	}
	src := dc.cfg.Get(k).Array()
	res := make([]int, len(src))
	for i, x := range src {
		switch v := x.Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.Atoi(os.ExpandEnv(v))
			res[i] = vi
		default:
			res[i] = int(x.Int())
		}
	}
	return res
}

// GetStringMapWithDefault extracts k-path value as map[string]interface{},
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetStringMapWithDefault(k string, def map[string]interface{}) map[string]interface{} {
	k = dc.pathPrefix + k
	if !dc.cfg.Get(k).Exists() {
		return def
	}
	src := dc.cfg.Get(k).Map()
	res := make(map[string]interface{}, len(src))
	for k, x := range src {
		switch v := x.Value().(type) {
		case string:
			res[k] = os.ExpandEnv(v)
		default:
			res[k] = x.Value()
		}
	}
	return res
}

// GetStringMapStringWithDefault extracts k-path value as map[string]string,
// values ${val} or $val will be replaced according to the values current environment variables,
// if k not exists returns def value.
func (dc *DotConf) GetStringMapStringWithDefault(k string, def map[string]string) map[string]string {
	k = dc.pathPrefix + k
	if !dc.cfg.Get(k).Exists() {
		return def
	}
	src := dc.cfg.Get(k).Map()
	res := make(map[string]string, len(src))
	for k, v := range src {
		res[k] = os.ExpandEnv(v.String())
	}
	return res
}
