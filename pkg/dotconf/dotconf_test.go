package dotconf

import (
	"errors"
	"math"
	"os"
	"reflect"
	"testing"
	"time"
)

func Test_readConfigFileToMAP(t *testing.T) {
	jsonJSON := map[string]interface{}{
		"logger": map[string]interface{}{
			"level":          "bug",
			"enable":         true,
			"view_timestamp": false,
			"view_level":     true,
		},
	}
	tomlJSON := map[string]interface{}{
		"app": []interface{}{
			map[string]interface{}{
				"command": "./bin/crash3",
				"name":    "crash3",
			},
		},
		"logger": map[string]interface{}{
			"level": "info",
		},
	}
	yamlJSON := map[string]interface{}{
		"app": []interface{}{
			map[string]interface{}{
				"command": "./bin/crash3 -p=7s",
				"name":    "crash3",
			},
		},
	}
	tests := []struct {
		name    string
		fn      string
		want    map[string]interface{}
		wantErr bool
	}{
		{".yaml", "testdata/yaml/config_short.yaml", yamlJSON, false},
		{".json", "testdata/json/config_invalid.json", jsonJSON, false},
		{".toml", "testdata/toml/config_short.toml", tomlJSON, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := New("")
			got, err := dc.readConfigFileToMAP(tt.fn)
			if (err != nil) != tt.wantErr {
				t.Errorf("readConfigFileToMAP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readConfigFileToMAP()\n%+#v\nwant\n%+#v\n", got, tt.want)
			}
		})
	}
}

func Test_mergeMAPs(t *testing.T) {
	m11 := map[string]interface{}{
		"a": "m11",
	}
	m21 := map[string]interface{}{
		"a": "m21",
	}
	w11 := map[string]interface{}{
		"a": "m21",
	}
	m12 := map[string]interface{}{
		"a": "m12",
		"b": "m12",
		"c": map[string]interface{}{
			"aa": "m12",
		},
	}
	m22 := map[string]interface{}{
		"d": "m22",
		"c": map[string]interface{}{
			"aa": "m22",
			"bb": "m22",
		},
	}
	w12 := map[string]interface{}{
		"a": "m12",
		"b": "m12",
		"d": "m22",
		"c": map[string]interface{}{
			"aa": "m22",
			"bb": "m22",
		},
	}
	//
	m13 := map[string]interface{}{
		"a": "m13",
		"b": 0.35,
		"c": "5s",
		"d": 999999,
		"e": -22,
		"f": true,
	}
	m23 := map[string]interface{}{
		"a": "m23",
		"c": map[string]interface{}{
			"aa": "m22",
			"bb": "m22",
		},
		"g": map[string]interface{}{
			"gaa": "m23",
			"gbb": "m23",
		},
		"f": false,
	}
	w13 := map[string]interface{}{
		"a": "m23",
		"b": 0.35,
		"c": map[string]interface{}{
			"aa": "m22",
			"bb": "m22",
		},
		"d": 999999,
		"e": -22,
		"f": false,
		"g": map[string]interface{}{
			"gaa": "m23",
			"gbb": "m23",
		},
	}
	tests := []struct {
		name string
		m1   map[string]interface{}
		m2   map[string]interface{}
		want map[string]interface{}
	}{
		{"simple_srtring", m11, m21, w11},
		{"simple_interface", m12, m22, w12},
		{"variants_datatypes", m13, m23, w13},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mergeMAPs(tt.m1, tt.m2)
			if !reflect.DeepEqual(tt.m1, tt.want) {
				t.Errorf("mergeMAPs(m1, m2)\nm1=%+v\nm2=%+v\nw1=%+v\n", tt.m1, tt.m2, tt.want)
			}
		})
	}
}

func TestRead(t *testing.T) {
	dc := New("")
	tests := []struct {
		name       string
		rootConfig string
		appConfigs []string
		wantPath   string
		wantValue  interface{}
		wantErr    bool
	}{
		{"only_root_json_short", "./testdata/json/config_short.json", nil, "app.0.name", "sh", false},
		{"not_exists", "/config_short_not_exists.json", nil, "app.0.name", "sh", true},
		// {"root", "/starter_config.toml", nil, "app.name", "sh", false},
		{"root_json_short_toml_app", "./testdata/json/config_short.json", []string{"testdata/toml/config_short.toml"}, "app.0.name", "crash3", false},
		{"root_json_short_toml+yaml_app", "./testdata/json/config_short.json", []string{"testdata/toml/config_short.toml", "testdata/yaml/config_short.yaml"}, "app.0.command", "./bin/crash3 -p=7s", false},
		{"not exists app configs", "./testdata/json/config_short.json", []string{"testdata/toml/not_exists_27405.toml"}, "app.0.name", "sh", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := dc.Read(tt.rootConfig, tt.appConfigs...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantErr {
				return
			}
			v := dc.Get(tt.wantPath)
			if interface{}(v) != interface{}(tt.wantValue) {
				t.Errorf("Read() error, for key=%s gotValue=%v (%T) != wantValue=%v (%T)",
					tt.wantPath, v, v, tt.wantValue, tt.wantValue)
			}
			// t.Logf("key=%s, value=%v", tt.wantPath, v)
		})
	}
}

func TestReadWithDefaults(t *testing.T) {
	tests := []struct {
		name       string
		rootConfig string
		envPrefix  string
		defaults   []map[string]interface{}
		key        string
		wantValue  interface{}
		wantErr    bool
	}{
		{
			"not exists config, use default app.name",
			"./testdata/json/not_exists_really.json",
			"",
			[]map[string]interface{}{
				{
					"app": map[string]interface{}{
						"name": "default.Name",
					},
				},
			},
			"app.name",
			"default.Name",
			false,
		},
		{
			"not exists config, use default app.0.name",
			"./testdata/json/not_exists_really.json",
			"",
			[]map[string]interface{}{
				{
					"app": []map[string]interface{}{
						{
							"name": "default.Name",
						},
					},
				},
				{
					"app": []map[string]interface{}{
						{
							"name": "default.Name2",
						},
					},
				},
			},
			"app.0.name",
			"default.Name2",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := New(tt.envPrefix, tt.defaults...)

			err := dc.Read(tt.rootConfig)
			if (err != nil) != tt.wantErr {
				if errors.Is(err, os.ErrNotExist) {
					if len(tt.defaults) > 0 {

						v := dc.Get(tt.key)
						if interface{}(v) != interface{}(tt.wantValue) {
							t.Errorf("Read() error, for key=%s gotValue=%v (%T) != wantValue=%v (%T)",
								tt.key, v, v, tt.wantValue, tt.wantValue)
						}
						return
					}
				}
				t.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			v := dc.Get(tt.key)
			if interface{}(v) != interface{}(tt.wantValue) {
				t.Errorf("Read() error, for key=%s gotValue=%v (%T) != wantValue=%v (%T)",
					tt.key, v, v, tt.wantValue, tt.wantValue)
			}
			// t.Logf("key=%s, value=%v", tt.wantPath, v)
		})
	}
}

const schemaStarter = `{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "https://gitlab.tp.sblogistica.ru/platform2.0/tools/starter/configs/schema.json",
    "type": "object",
    "title": "The stater config schema",
    "description": "The root schema comprises the starter config.",
    "required": [
        "sync",
        "app"
    ],
    "properties": {
        "logger": {
            "type": "object",
            "title": "The logger schema",
            "description": "General logger settings for application",
            "properties": {
                "level": {
                    "type": "string",
                    "enum": [
                        "debug",
                        "info",
                        "warn",
                        "error"
                    ],
                    "title": "The level severity",
                    "description": "Logging severity (in order: debug, info, warn, error...)"
                },
                "enable": {
                    "type": "boolean",
                    "title": "The enable schema",
                    "description": "Flag on/off logging"
                },
                "view_timestamp": {
                    "type": "boolean",
                    "title": "The view_timestamp schema",
                    "description": "Flag print date-time of the log message or not."
                },
                "view_level": {
                    "type": "boolean",
                    "title": "The view_level schema",
                    "description": "Flag print severity level of the log message or not."
                }
            }
        },
        "sync": {
            "type": "object",
            "title": "The sync schema",
            "description": "Parameters for the syncing with central config server.",
            "required": [
                "enable",
                "critical",
                "targets",
                "retry",
                "update",
                "registration"
            ],
            "properties": {
                "enable": {
                    "type": "boolean",
                    "title": "The enable schema",
                    "description": "Enable or not syncing."
                },
                "critical": {
                    "type": "boolean",
                    "title": "The critical schema",
                    "description": "Flag critical or not app. Run app or not if first sync failed."
                },
                "targets": {
                    "type": "string",
                    "title": "The targets schema",
                    "description": "Mapping paths configs on the server and on the client/host.",
                    "minLength": 3
                },
                "retry": {
                    "type": "object",
                    "title": "The retry schema",
                    "description": "Sync retry parameters.",
                    "required": [
                        "limit"
                    ],
                    "properties": {
                        "limit": {
                            "type": "integer",
                            "title": "The limit schema",
                            "description": "How many failed attempts to syncing.",
                            "minimum": 0,
                            "maximum": 100
                        },
                        "interval": {
                            "$ref": "#/definitions/duration",
                            "title": "The interval sync retry schema",
                            "description": "Delay before attempts to syncing."
                        }
                    }
                },
                "update": {
                    "type": "object",
                    "title": "The update schema",
                    "description": "Update parameters.",
                    "required": [
                        "enable"
                    ],
                    "properties": {
                        "enable": {
                            "type": "boolean",
                            "title": "The enable schema",
                            "description": "Flag updating enable or not"
                        },
                        "interval": {
                            "$ref": "#/definitions/duration",
                            "title": "The interval of the syncing",
                            "description": "Delay before start new syncing."
                        }
                    }
                },
                "registration": {
                    "type": "object",
                    "title": "The registration schema",
                    "description": "Parameters for getting auth creds for syncing.",
                    "required": [
                        "name",
                        "group",
                        "properties",
                        "connection"
                    ],
                    "properties": {
                        "name": {
                            "type": "string",
                            "title": "The name schema",
                            "description": "The name of the service."
                        },
                        "group": {
                            "type": "string",
                            "title": "The group schema",
                            "description": "The name of the group services."
                        },
                        "properties": {
                            "type": "object",
                            "title": "The properties schema",
                            "description": "An addition properties"
                        },
                        "connection": {
                            "type": "object",
                            "title": "The connection schema",
                            "description": "The connection parameters to the central config server.",
                            "required": [
                                "url",
                                "timeout",
                                "auth"
                            ],
                            "properties": {
                                "url": {
                                    "type": "string",
                                    "format": "uri",
                                    "title": "The url schema",
                                    "description": "Connection string in url format."
                                },
                                "timeout": {
                                    "$ref": "#/definitions/duration",
                                    "title": "The timeout of the connection",
                                    "description": "Timeout connection."
                                },
                                "auth": {
                                    "type": "object",
                                    "title": "The auth schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "required": [
                                        "private_token"
                                    ],
                                    "properties": {
                                        "private_token": {
                                            "type": "string",
                                            "title": "The private_token schema",
                                            "description": "A private token for auth."
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "app": {
            "type": "object",
            "title": "The app schema",
            "description": "Parameters for run application.",
            "required": [
                "cwd",
                "command"
            ],
            "properties": {
                "cwd": {
                    "type": "string",
                    "title": "The cwd schema",
                    "description": "The directory for execute appplication."
                },
                "command": {
                    "type": "string",
                    "title": "The command schema",
                    "description": "The command for execute application."
                },
                "restart_delay": {
                    "$ref": "#/definitions/duration",
                    "title": "The restart_delay schema",
                    "description": "Delay before run application."
                },
                "env": {
                    "type": "object",
                    "title": "The env schema",
                    "description": "Set of the ENV variables for application.",
                    "properties": {
                        "include_regexp": {
                            "type": "string",
                            "title": "The include_regexp schema",
                            "description": "Regexp for filtering including ENV VARS."
                        },
                        "exclude_regexp": {
                            "type": "string",
                            "title": "The exclude_regexp schema",
                            "description": "Regexp for filtering excluding ENV VARS."
                        },
                        "keys": {
                            "type": "object",
                            "title": "The keys schema",
                            "description": "Set of the Key/Value pairs ENV name and ENV value."
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "duration": {
            "type": "string",
            "pattern": "^([0-9]+[mnsuµh]*)+$"
        }
    }
}`

func TestReadWithSchema(t *testing.T) {
	dc := NewWithSchema("starter", schemaStarter)
	if dc == nil {
		t.Fatal("NewWithSchema() error, unexpected nil pointer")
	}
	tests := []struct {
		name       string
		rootConfig string
		appConfigs []string
		envs       map[string]string
		wantPath   string
		wantValue  interface{}
		wantErr    bool
	}{
		{
			"starter_with_empty_url",
			"testdata/toml/config_starter_empty_url.toml",
			nil,
			map[string]string{
				"STARTER_SYNC_REGISTRATION_CONNECTION_URL": "http://localhost:3000",
			},
			"sync.registration.connection.url",
			"http://localhost:3000",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.envs {
				os.Setenv(ek, ev)
			}
			err := dc.Read(tt.rootConfig, tt.appConfigs...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			v := dc.Get(tt.wantPath)
			if interface{}(v) != interface{}(tt.wantValue) {
				t.Errorf("Read() error, for key=%s gotValue=%v (%T) != wantValue=%v (%T)",
					tt.wantPath, v, v, tt.wantValue, tt.wantValue)
			}
			if err := dc.Validate(); err != nil {
				t.Errorf("dc.Validate() error %s", err)
			}
			// t.Logf("key=%s, value=%v", tt.wantPath, v)
			for ek := range tt.envs {
				os.Unsetenv(ek)
			}
		})
	}
}

const cfgSchemaJSON = `{
    "$id": "https://https://gitlab.com/art.frela/ckv/internal/config/schema.v1.json",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "type": "object",
    "title": "The ckv config schema",
    "description": "Параметры конфигурации приложения",
    "required": [
        "engine",
        "network"
    ],
    "properties": {
        "engine": {
            "type": "object",
            "title": "The database engine schema",
            "description": "Параметры движка базы данных",
            "properties": {
                "kind": {
                    "type": "string",
                    "enum": [
                        "inmemory"
                    ],
                    "description": "Тип движка базы данных"
                }
            },
            "required": [
                "kind"
            ]
        },
        "logger": {
            "type": "object",
            "title": "The logger schema",
            "description": "Параметры логирования",
            "properties": {
                "level": {
                    "type": "string",
                    "enum": [
                        "debug",
                        "info",
                        "warn",
                        "error"
                    ],
                    "title": "The level severity",
                    "description": "Уровень логирования (in order: debug, info, warn, error...)"
                },
                "development": {
                    "type": "boolean",
                    "description": "Флаг, вкл/нет формат в режиме разработки",
                    "default": false
                }
            }
        },
        "network": {
            "type": "object",
            "title": "The tcp server schema",
            "description": "Параметры для настройки tcp сервера",
            "properties": {
                "address": {
                    "type": "string",
                    "description": "host:port для биндинга tcp сервера",
                    "default": "localhost:3000"
                },
                "max_connections": {
                    "type": "integer",
                    "description": "Максимальное число одновременных tcp соединений",
                    "default": 1500
                },
                "max_message_size": {
                    "$ref": "#/definitions/size",
                    "description": "Максимальный размер одного сообщения",
                    "default": "256B"
                },
                "idle_timeout": {
                    "$ref": "#/definitions/duration",
                    "description": "Таймаут на прием/передачу данных по сети",
                    "default": "10s"
                }
            },
            "required": [
                "address"
            ]
        }
    },
    "definitions": {
        "duration": {
            "type": "string",
            "pattern": "^([0-9]+[mnsuµh]*)+$"
        },
        "size": {
            "type": "string",
            "pattern": "^[1-9][0-9]*[GgMmKkBb]?[Bb]?$"
        }
    }
}`

func TestNewWithSchema(t *testing.T) {
	def1 := map[string]interface{}{
		"engine": map[string]interface{}{
			"kind": "inmemory",
		},
		"logger": map[string]interface{}{
			"level":       "info",
			"development": false,
		},
		"network": map[string]interface{}{
			"address":          "localhost:3000",
			"max_connections":  1500,
			"max_message_size": "4Kb",
			"idle_timeout":     "10s",
		},
	}

	def2 := map[string]interface{}{
		"engine": map[string]interface{}{
			"kind": "inmemory",
		},
		"logger": map[string]interface{}{
			"level":       "debug",
			"development": false,
		},
		"network": map[string]interface{}{
			"address":          "localhost:3000",
			"max_connections":  100,
			"max_message_size": "8Kb",
			"idle_timeout":     "30s",
		},
	}
	tests := []struct {
		name     string
		schema   string
		defaults []map[string]interface{}
		wantNil  bool
	}{
		{
			name:   "test.1 valid schema with 2 defaults",
			schema: cfgSchemaJSON,
			defaults: []map[string]interface{}{
				def1,
				def2,
			},
			wantNil: false,
		},
		{
			name:     "test.2 invalid schema",
			schema:   `{..`,
			defaults: nil,
			wantNil:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewWithSchema("", tt.schema, tt.defaults...)
			if (got == nil) != tt.wantNil {
				t.Errorf("NewWithSchema = %v, want %v", got, tt.wantNil)
			}
		})
	}
}

func TestDotConf_env(t *testing.T) {
	tests := []struct {
		name      string
		envPrefix string
		k         string
		env       map[string]string
		want      string
	}{
		{"MY_APP_par1.par2", "MY_APP", "par1.par2", map[string]string{"MY_APP_PAR1_PAR2": "val1"}, "val1"},
		{"MY_APP_par1-par2", "MY_APP", "par1-par2", map[string]string{"MY_APP_PAR1-PAR2": "val2"}, "val2"},
		{"par1.par2", "", "par1.par2", nil, ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := &DotConf{
				envPrefix: tt.envPrefix,
			}
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}
			if got := dc.env(tt.k); got != tt.want {
				t.Errorf("DotConf.env() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_Get(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathToConfig string
		k            string
		env          map[string]string
		want         interface{}
	}{
		{
			"yaml_log_app",
			"CRASH3",
			"testdata/yaml/config_log_app.yaml",
			"logger.level",
			nil,
			"info",
		},
		{
			"yaml_log_app incorrect prefix ",
			"CRASH3",
			"testdata/yaml/config_log_app.yaml",
			"logger.level",
			nil,
			"info",
		},
		{"yaml_log_app_env", "CRASH3", "testdata/yaml/config_log_app.yaml", "logger.level", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, "warn"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}
			if got := dc.Get(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.Get() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetString(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathPrefix   string
		pathToConfig string
		k            string
		env          map[string]string
		want         string
	}{
		{"yaml_log_app", "CRASH3", "", "testdata/yaml/config_log_app.yaml", "logger.level", nil, "info"},
		{"yaml_log_app_env", "CRASH3", "", "testdata/yaml/config_log_app.yaml", "logger.level", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, "warn"},
		{"yaml_env_value-env", "MY_APP", "", "testdata/yaml/config_env.yaml", "app.0.name", map[string]string{"MY_APP_NAME": "superApp"}, "superApp"},
		{"yaml_log_app_prefix-logger", "CRASH3", "logger.", "testdata/yaml/config_log_app.yaml", "level", nil, "info"},
		{"yaml_log_app_env_prefix-logger", "CRASH3", "logger.", "testdata/yaml/config_log_app.yaml", "level", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, "warn"},
		{"yaml_env_value-env_prefix-app", "MY_APP", "app.", "testdata/yaml/config_env.yaml", "0.name", map[string]string{"MY_APP_NAME": "superApp"}, "superApp"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)
			dc.SetPathPrefix(tt.pathPrefix)
			if got := dc.GetString(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetString() = %v, want %v", got, tt.want)
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetSliceString(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathPrefix   string
		pathToConfig string
		k            string
		env          map[string]string
		want         []string
	}{
		{"yaml_log_app", "PPP", "", "testdata/yaml/config_log_app.yaml", "my.locales", nil, []string{"ru", "en"}},
		{"yaml_log_app_env", "CRASH3", "", "testdata/yaml/config_log_app.yaml", "logger.level", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, []string{"warn"}},
		{"config_slice_str-env", "DOTCONF", "", "testdata/yaml/config_slice_str.yaml", "two.envs", map[string]string{"DOTCONF_TWO_ONE": "first"}, []string{"first", "second"}},
		{"config_slice_str-notexists-key", "DOTCONF", "", "testdata/yaml/config_slice_str.yaml", "notexists.envs", nil, []string{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)
			dc.SetPathPrefix(tt.pathPrefix)
			if got := dc.GetSliceString(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetSliceString() = %v, want %v", got, tt.want)
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetBool(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathToConfig string
		k            string
		env          map[string]string
		want         bool
	}{
		{"yaml_log_app", "CRASH3", "testdata/yaml/config_log_app.yaml", "logger.enable", nil, true},
		{"yaml_log_app_env", "CRASH3", "testdata/yaml/config_log_app.yaml", "logger.view_timestamp", map[string]string{"CRASH3_LOGGER_VIEW_TIMESTAMP": "true"}, true},
		{"yaml_env_logger_vts_true", "CRASH3", "testdata/yaml/config_env.yaml", "logger.view_timestamp", map[string]string{"BOOL_TRUE": "true"}, true},
		{"yaml_env_logger_vl_false", "CRASH3", "testdata/yaml/config_env.yaml", "logger.view_level", map[string]string{"BOOL_FALSE": "false"}, false},
		{"yaml_env_logger_vl_true", "CRASH3", "testdata/yaml/config_env.yaml", "logger.view_level", map[string]string{"BOOL_FALSE": "false", "CRASH3_LOGGER_VIEW_LEVEL": "true"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetBool(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetBool() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetDuration(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathToConfig string
		k            string
		env          map[string]string
		want         time.Duration
	}{
		{"yaml_log_app", "CRASH3", "testdata/yaml/config_log_app.yaml", "app.first_run_delay", nil, time.Second},
		{"yaml_log_app_env", "CRASH3", "testdata/yaml/config_log_app.yaml", "app.first_run_delay", map[string]string{"CRASH3_APP_FIRST_RUN_DELAY": "1m30s"}, 90 * time.Second},
		{"yaml_env_db_timeout", "CRASH3", "testdata/yaml/config_env.yaml", "db.timeout", nil, 0},
		{"yaml_env_db_period", "CRASH3", "testdata/yaml/config_env.yaml", "db.period", map[string]string{"DB_PERIOD": "30s"}, 30 * time.Second},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetDuration(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetDuration() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetFloat64(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathToConfig string
		k            string
		env          map[string]string
		want         float64
	}{
		{"yaml_log_app", "CRASH3", "testdata/yaml/config_log_app.yaml", "math.pi", nil, 3.14},
		{"yaml_log_app_env", "CRASH3", "testdata/yaml/config_log_app.yaml", "math.pi", map[string]string{"CRASH3_MATH_PI": "5.555"}, 5.555},
		{"yaml_env_exists_env", "CRASH3", "testdata/yaml/config_env.yaml", "math.root", map[string]string{"ROOT_PI": "1.77245385091"}, 1.77245385091},
		{"yaml_env_not_exists_env", "CRASH3", "testdata/yaml/config_env.yaml", "math.root", nil, 0.0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetFloat64(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetFloat64() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetInt(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathToConfig string
		k            string
		env          map[string]string
		want         int
		want32       int32
		want64       int64
	}{
		{"yaml_log_app", "CRASH3", "testdata/yaml/config_log_app.yaml", "math.pi", nil, 3, 3, 3},
		{"yaml_log_app_env", "CRASH3", "testdata/yaml/config_log_app.yaml", "math.pi", map[string]string{"CRASH3_MATH_PI": "5"}, 5, 5, 5},
		{"yaml_env_exists_env_1.7", "CRASH3", "testdata/yaml/config_env.yaml", "math.root", map[string]string{"ROOT_PI": "1.77245385091"}, 0, 0, 0},
		{"yaml_env_exists_env_2", "CRASH3", "testdata/yaml/config_env.yaml", "math.root", map[string]string{"ROOT_PI": "2"}, 2, 2, 2},
		{"yaml_env_not_exists_env", "CRASH3", "testdata/yaml/config_env.yaml", "math.root", nil, 0, 0, 0},
		{"yaml_env_lim_zero", "CRASH3", "testdata/yaml/config_env.yaml", "lim.zero", map[string]string{"LIM_ZERO": "90071992547409929007199254740992"}, math.MaxInt64, -1, math.MaxInt64},
		{"yaml_env_lim_max", "CRASH3", "testdata/yaml/config_env.yaml", "lim.max", nil, 9007199254740992, 0, 9007199254740992},
		{"yaml_env_lim_min", "CRASH3", "testdata/yaml/config_env.yaml", "lim.min", nil, -9007199254740992, 0, -9007199254740992},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetInt(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetInt() = %v, want %v", got, tt.want)
			}
			if got32 := dc.GetInt32(tt.k); !reflect.DeepEqual(got32, tt.want32) {
				t.Errorf("DotConf.GetInt32() = %v, want %v", got32, tt.want32)
			}
			if got64 := dc.GetInt64(tt.k); !reflect.DeepEqual(got64, tt.want64) {
				t.Errorf("DotConf.GetInt64() = %v, want %v", got64, tt.want64)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetStringMap(t *testing.T) {
	ts := float64(123456789)
	tests := []struct {
		name         string
		envPrefix    string
		pathToConfig string
		k            string
		env          map[string]string
		want         map[string]interface{}
	}{
		{"yaml_log_app", "CRASH3", "testdata/yaml/config_log_app.yaml", "math", nil, map[string]interface{}{"pi": 3.14, "ts": ts}},
		{"yaml_log_app_env", "CRASH3", "testdata/yaml/config_log_app.yaml", "app.env.keys", nil, map[string]interface{}{"CRASH3_PERIOD": "15s"}},
		{
			"yaml_env_math_with_env", "CRASH3", "testdata/yaml/config_env.yaml", "math",
			map[string]string{"ROOT_PI": "1.77245"},
			map[string]interface{}{"pi": 3.14, "root": "1.77245", "ts": ts},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetStringMap(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetStringMap() = %+#v, want %+#v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetStringMapString(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathToConfig string
		k            string
		env          map[string]string
		want         map[string]string
	}{
		{"yaml_log_app", "CRASH3", "testdata/yaml/config_log_app.yaml", "math", nil, map[string]string{"pi": "3.14", "ts": "123456789"}},
		{"yaml_log_app_env", "CRASH3", "testdata/yaml/config_log_app.yaml", "app.env.keys", nil, map[string]string{"CRASH3_PERIOD": "15s"}},
		{
			"yaml_env_math_with_env", "CRASH3", "testdata/yaml/config_env.yaml", "math",
			map[string]string{"ROOT_PI": "1.77245"},
			map[string]string{"pi": "3.14", "root": "1.77245", "ts": "123456789"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetStringMapString(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetStringMapString() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetSliceInterface(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathPrefix   string
		pathToConfig string
		k            string
		env          map[string]string
		want         []interface{}
	}{
		{
			"yaml_app_slice",
			"",
			"",
			"testdata/yaml/config_env.yaml",
			"app",
			map[string]string{"ROOT_PI": "1.77245", "MY_APP_NAME": "myapp"},
			[]interface{}{
				map[string]interface{}{
					"name":    "myapp",
					"command": "./bin/crash3 -p=7s",
				},
				map[string]interface{}{"name": "pingcmd", "command": "ping"},
			},
		},
		{
			"yaml_env_ints",
			"",
			"",
			"testdata/yaml/config_env.yaml",
			"ints",
			nil,
			[]interface{}{float64(1), float64(2), float64(3)},
		},
		{
			"yaml_app_config_env_short",
			"",
			"",
			"testdata/yaml/config_env_short.yaml",
			"app",
			map[string]string{"XYZ_APP_NAME": "myapp"},
			[]interface{}{
				map[string]interface{}{
					"name":    "myapp",
					"command": "./bin/crash3 -p=7s",
					"enable":  true,
				},
				map[string]interface{}{
					"name":    "pingcmd",
					"command": "ping",
					"enable":  "",
				},
			},
		},
		{
			"json slice.float64",
			"",
			"ru.spinosa.",
			"testdata/json/config_float64.json",
			"core.metrics.buckets",
			nil,
			[]interface{}{
				1.1,
				2.0,
				3.0,
				4.4,
				5.55555,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix).SetPathPrefix(tt.pathPrefix)
			if err := dc.Read(tt.pathToConfig); err != nil {
				t.Errorf("read config %s error: %s", tt.pathToConfig, err)
				return
			}

			got := dc.GetSliceInterface(tt.k)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetSliceInterface()\n%+#v\n%+#v\n", got, tt.want)
				for _, gv := range got {
					t.Logf("got value %v has type %T", gv, gv)
				}
			}

			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

// benchmarks

func BenchmarkGetInt64(b *testing.B) {
	dc := New("")
	dc.Read("testdata/yaml/config_log_app.yaml")
	for i := 0; i < b.N; i++ {
		dc.GetInt64("math.ts")
	}
}

func BenchmarkGetString(b *testing.B) {
	dc := New("")
	dc.Read("testdata/yaml/config_log_app.yaml")
	for i := 0; i < b.N; i++ {
		dc.GetString("app.name")
	}
}

func BenchmarkGetStringAsIs(b *testing.B) {
	dc := New("")
	dc.Read("testdata/yaml/config_log_app.yaml")
	for i := 0; i < b.N; i++ {
		dc.GetStringAsIs("app.name")
	}
}

/*
goos: darwin
goarch: amd64
pkg: gitlab.tp.sblogistica.ru/gopkg/dotconf
cpu: Intel(R) Core(TM) i5-3470S CPU @ 2.90GHz
BenchmarkGetInt64-4   	  919182	      1305 ns/op	      40 B/op	       5 allocs/op
BenchmarkGetInt64alt-4   1237107	      869.5 ns/op	       0 B/op	       0 allocs/op
//
BenchmarkGetString-4   	 1371975	       873.7 ns/op	      48 B/op	       5 allocs/op
BenchmarkGetString-4   	 1359276	       883.4 ns/op	      48 B/op	       5 allocs/op
BenchmarkGetStringAlt-4  2579922	       451.2 ns/op	       0 B/op	       0 allocs/op
PASS
ok  	gitlab.tp.sblogistica.ru/gopkg/dotconf	1.500s


goos: linux
goarch: amd64
pkg: gitlab.tp.sblogistica.ru/gopkg/dotconf
cpu: Intel(R) Core(TM) i7-10510U CPU @ 1.80GHz
BenchmarkGetString-8   	 2201494	       544.5 ns/op	      48 B/op	       5 allocs/op
BenchmarkGetStringAsIs-8 2141245	       546.4 ns/op	      48 B/op	       5 allocs/op
*/

func TestDotConf_SetPathPrefix(t *testing.T) {
	tests := []struct {
		name       string
		pathPrefix string
		want       string
	}{
		{"тест с точкой", "ru.spinosa.", "ru.spinosa."},
		{"тест без точки", "ru.spinosa", "ru.spinosa."},
		{"тест пустой строки", "", ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := New("")
			dc.SetPathPrefix(tt.pathPrefix)
			if dc.pathPrefix != tt.want {
				t.Errorf("DotConf.SetPathPrefix() = %s, want %s", dc.pathPrefix, tt.want)
			}
		})
	}
}

func TestDotConf_GetStringAsIs(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathPrefix   string
		pathToConfig string
		k            string
		env          map[string]string
		want         string
	}{
		{"yaml_log_app", "CRASH3", "", "testdata/yaml/config_log_app.yaml", "logger.level", nil, "info"},
		{"yaml_log_app_env", "CRASH3", "", "testdata/yaml/config_log_app.yaml", "logger.level", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, "warn"},
		{"yaml_env_value-env", "MY_APP", "", "testdata/yaml/config_env.yaml", "app.0.name", map[string]string{"MY_APP_NAME": "superApp"}, "superApp"},
		{"yaml_log_app_prefix-logger", "CRASH3", "logger.", "testdata/yaml/config_log_app.yaml", "level", nil, "info"},
		{"yaml_log_app_env_prefix-logger", "CRASH3", "logger.", "testdata/yaml/config_log_app.yaml", "level", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, "warn"},
		{"yaml_env_value-env_prefix-app", "MY_APP", "app.", "testdata/yaml/config_env.yaml", "0.name", map[string]string{"MY_APP_NAME": "superApp"}, "superApp"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			dc.SetPathPrefix(tt.pathPrefix)
			if got := dc.GetStringAsIs(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetString() = %v, want %v", got, tt.want)
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetIntSlice(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		want         []int
	}{
		{"yaml_env_ints", "testdata/yaml/config_env.yaml", "APP1", "", nil, "ints", []int{1, 2, 3}},
		{"yaml_env_ints_def", "testdata/yaml/config_env.yaml", "APP1", "", nil, "not.ints", []int{}},
		{
			"yaml_env_intswithstr",
			"testdata/yaml/config_env.yaml",
			"DOTCONF",
			"",
			map[string]string{"DOTCONF_ITERATION": "1999"},
			"intswithstr",
			[]int{1, 2, 1999},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetIntSlice(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetIntSlice() = %v, want %v", got, tt.want)
			}

			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetSliceMap(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		want         []map[string]interface{}
	}{
		{"toml", "testdata/toml/config_slice_map.toml", "APP1", "", nil, "output", []map[string]interface{}{{"type": "rabbitmq"}, {"type": "nats"}}},
		{
			"toml_env", "testdata/toml/config_slice_map_env.toml", "APP1", "",
			map[string]string{"OUTPUTHOST": "rabbit.mq.com"},
			"output",
			[]map[string]interface{}{
				{
					"type": "rabbitmq",
					"host": "localhost",
				},
				{
					"type": "nats",
					"host": "rabbit.mq.com",
				},
			},
		},
		{
			"config_slice_str.yaml",
			"testdata/yaml/config_slice_str.yaml",
			"APP1", "", nil,
			"complex",
			[]map[string]interface{}{
				{"part": map[string]interface{}{
					"foo": "bar",
					"zoo": map[string]interface{}{
						"cat": map[string]interface{}{
							"name": "murzik",
							"age":  3.0,
						},
					},
				}},
				{"dog": map[string]interface{}{
					"name":    "muhtar",
					"age":     5.0,
					"aliases": []interface{}{"muha", "pulya"},
				}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}
			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)
			if got := dc.GetSliceMap(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetSliceMap()\n%+#v\nwant\n%+#v\n", got, tt.want)
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

const schemaEnvShort = `{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "gitlab.tp.sblogistica.ru/gopkg/dotconf/temp/short_env_schema.json",
    "type": "object",
    "title": "The config schema",
    "description": "The config schema.",
    "properties": {
        "app": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "command": {
                        "type": "string"
                    },
                    "enable": {
                        "type": "boolean"
                    }
                }
            }   
        },
        "bool": {
            "type": "object",
            "properties": {
                "vtrue": {
                    "type": "boolean"
                },
                "vfalse": {
                    "type": "boolean"
                },
                "env": {
                    "type": "boolean"
                }
            }
        }
    },
    "definitions": {
        "duration": {
            "type": "string"
        },
        "counter": {
            "type":"integer"
        }
    }
}`

func TestDotConf_expandSlice(t *testing.T) {
	tests := []struct {
		name       string
		configPath string
		schema     string
		key        string
		envPrefix  string
		env        map[string]string
		want       []interface{}
	}{
		{
			"тест. testdata/yaml/config_env_short.yaml раздел app",
			"testdata/yaml/config_env_short.yaml",
			schemaEnvShort,
			"app",
			"",
			map[string]string{
				"XYZ_APP_NAME":     "myapp",
				"WSX_APP_1_ENABLE": "true",
			},
			[]interface{}{
				map[string]interface{}{
					"name":    "myapp",
					"command": "./bin/crash3 -p=7s",
					"enable":  true,
				},
				map[string]interface{}{
					"name":    "pingcmd",
					"command": "ping",
					"enable":  true,
				},
			},
		},
		{
			"тест. testdata/yaml/config_env_short.yaml раздел bool",
			"testdata/yaml/config_env_short.yaml",
			schemaEnvShort,
			"bool",
			"",
			map[string]string{
				"GEC_BOOL_ENV": "true",
			},
			[]interface{}{
				map[string]interface{}{
					"vtrue":  true,
					"vfalse": false,
					"env":    true,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}
			dc := NewWithSchema(tt.envPrefix, tt.schema)
			dc.Read(tt.configPath)
			//
			slice := dc.GetSliceInterface(tt.key)
			if got := dc.expandSlice(tt.key, slice); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.expandSlice() = %v, want %v", got, tt.want)
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

const schemaEnvExp = `{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "gitlab.tp.sblogistica.ru/gopkg/dotconf/temp/short_env_schema.json",
    "type": "object",
    "title": "The config schema",
    "description": "The config schema.",
    "properties": {
        "root": {
            "type": "object",
            "properties": {
                "app": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/appItem"
                    }
                }
            }
        },
        "next": {
            "type": "object",
            "properties": {
                "sub": {
                    "type": "object",
                    "properties": {
                        "env": {
                            "type": "boolean"
                        },
                        "foo": {
                            "type": "object",
                            "properties": {
                                "bar": {
                                    "type": "number"
                                },
                                "car": {
                                    "type": "integer"
                                },
                                "kx" : {
                                    "type": "number"
                                }
                            }
                        }
                    }
                }
            }
        }
    },
	"required": [
        "root",
        "next"
    ],
    "definitions": {
        "appItem": {
            "type":"object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "command": {
                    "type": "string"
                },
                "enable": {
                    "type": "boolean"
                }
            }
        }
    }
}`

func TestDotConf_expandMap(t *testing.T) {
	tests := []struct {
		name       string
		configPath string
		schema     string
		key        string
		envPrefix  string
		env        map[string]string
		want       map[string]interface{}
	}{
		{
			"тест testdata/yaml/config_env_ext.yaml root",
			"testdata/yaml/config_env_ext.yaml",
			schemaEnvExp,
			"root",
			"",
			map[string]string{
				"XYZ_APP_NAME":  "myapp777",
				"WSX_APP_1_CMD": "ping -c=6",
			},
			map[string]interface{}{
				"app": []interface{}{
					map[string]interface{}{
						"name":    "myapp777",
						"command": "./bin/crash3 -p=7s",
						"enable":  true,
					},
					map[string]interface{}{
						"name":    "pingcmd",
						"command": "ping -c=6",
						"enable":  true,
					},
				},
			},
		},
		{
			"тест testdata/yaml/config_env_ext.yaml next.sub",
			"testdata/yaml/config_env_ext.yaml",
			schemaEnvExp,
			"next.sub",
			"",
			map[string]string{
				"GEC_BOOL_ENV":    "1",
				"GEC_INT_CAR":     "7",
				"GEC_NEXT_FOO_KX": "3",
			},
			map[string]interface{}{
				"env": true,
				"foo": map[string]interface{}{
					"bar": 3.14,
					"car": float64(7),
					"kx":  float64(3),
				},
			},
		},
		{
			"тест testdata/yaml/config_env_ext.yaml next.sub.foo",
			"testdata/yaml/config_env_ext.yaml",
			schemaEnvExp,
			"next.sub.foo",
			"",
			map[string]string{
				"GEC_INT_CAR":     "777",
				"GEC_NEXT_FOO_KX": "5",
			},
			map[string]interface{}{
				"bar": 3.14,
				"car": float64(777),
				"kx":  float64(5),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}
			dc := NewWithSchema(tt.envPrefix, tt.schema)
			dc.Read(tt.configPath)

			kMap := dc.GetStringMap(tt.key)
			if dc.expandMap(tt.key, kMap); !reflect.DeepEqual(kMap, tt.want) {
				t.Errorf("DotConf.expandMap()\n%v\nwant\n%v", kMap, tt.want)
				for k, v := range kMap {
					t.Logf("%s - %v %T", k, v, v)
					if wv, ok := tt.want[k]; ok {
						t.Logf("%s - %v %T", k, wv, wv)
					}
				}
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_DeepCopy(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		env          map[string]string
		key          string
	}{
		{
			"config_slice_str.yaml key1",
			"testdata/yaml/config_slice_str.yaml",
			"E",
			nil,
			"key1",
		},
		{
			"config_slice_str.yaml key2",
			"testdata/yaml/config_slice_str.yaml",
			"DOTCONF",
			map[string]string{"DOTCONF_KEY2_VALUE": "fooBAR"},
			"key2",
		},
		{
			"config_slice_str.yaml notexists key",
			"testdata/yaml/config_slice_str.yaml",
			"DOTCONF",
			map[string]string{"DOTCONF_KEY2_VALUE": "fooBAR"},
			"key_notexists",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			got1 := dc.GetString(tt.key)

			dc2 := dc.DeepCopy()

			got2 := dc2.GetString(tt.key)

			if got1 != got2 {
				t.Errorf("clonedCFG.%s = %s, want %s", tt.key, got2, got1)
			}

			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func Test_formatValidate(t *testing.T) {
	tests := []struct {
		name    string
		format  string
		wantErr bool
	}{
		{
			"json - ok",
			"json",
			false,
		},
		{
			"yaml - ok",
			"yaml",
			false,
		},
		{
			"YML - ok",
			"YML",
			false,
		},
		{
			"TOML - ok",
			"TOML",
			false,
		},
		{
			"xml - err",
			"xml",
			true,
		},
		{
			"CSV - err",
			"CSV",
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := formatValidate(tt.format); (err != nil) != tt.wantErr {
				t.Errorf("formatValidate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_envSanitizer(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			"rest-proxy",
			"REST_PROXY",
		},
		{
			"rest-proxy--",
			"REST_PROXY",
		},
		{
			" rest-proxy-- ",
			"REST_PROXY",
		},
		{
			" -- rest-proxy -- ",
			"REST_PROXY",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := envSanitizer(tt.name); got != tt.want {
				t.Errorf("envSanitizer() = %v, want %v", got, tt.want)
			}
		})
	}
}
