package dotconf

import (
	"os"
	"testing"

	"github.com/tidwall/gjson"
	"github.com/xeipuuv/gojsonschema"
)

// nolint:lll
const schemaRabbit = `{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "gitlab.tp.sblogistica.ru/platform2.0/tools/rabbitmq-healthchecker/config.json",
    "type": "object",
    "title": "The config schema",
    "description": "The config schema comprises the entire config document.",
    "properties": {
        "logger": {
            "type": "object",
            "title": "The logger schema",
            "description": "General logger settings for application",
            "properties": {
                "level": {
                    "type": "string",
                    "enum": [
                        "debug",
                        "info",
                        "warn",
                        "error"
                    ],
                    "title": "The level severity",
                    "description": "Logging severity (in order: debug, info, warn, error...)"
                },
                "enable": {
                    "type": "boolean",
                    "title": "The enable schema",
                    "description": "Flag, on/off logging"
                },
                "view_timestamp": {
                    "type": "boolean",
                    "title": "The view_timestamp schema",
                    "description": "Flag, print date-time of the log message or not."
                },
                "view_level": {
                    "type": "boolean",
                    "title": "The view_level schema",
                    "description": "Flag, print severity level of the log message or not."
                }
            }
        },
        "api": {
            "type": "object",
            "title": "The api schema",
            "description": "HTTP API parameters.",
            "required": [
                "listening"
            ],
            "properties": {
                "listening": {
                    "type": "string",
                    "pattern": "^.*:[0-9]{2,4}$",
                    "title": "The listening schema",
                    "description": "Host:port for binding http server."
                },
                "prefix": {
                    "type": "string",
                    "pattern": "(\/{0,1}[a-z]*[0-9]*)*",
                    "title": "The prefix schema",
                    "description": "Resource path before main paths."
                }
            }
        },
        "rabbitmq": {
            "type": "object",
            "title": "The rabbitmq schema",
            "description": "RabbitMQ parameters.",
            "required": [
                "url"
            ],
            "properties": {
                "reconnect_interval": {
                    "$ref": "#/definitions/duration",
                    "title": "The reconnect_interval schema",
                    "description": "Reconnect duration."
                },
                "ping_interval": {
                    "$ref": "#/definitions/duration",
                    "title": "The ping_interval schema",
                    "description": "Ping interval."
                },
                "url": {
                    "type": "string",
                    "pattern": "^amqp:\/\/.*@{0,1}.*",
                    "title": "The url schema",
                    "description": "Connection string to rabbitMQ server."
                },
                "queue": {
                    "type": "string",
                    "pattern": ".{1,255}",
                    "title": "The queue schema",
                    "description": "Queue name for send/receive test messages."
                }
            }
        }
    },
    "definitions": {
        "duration": {
            "type": "string",
            "pattern": "^([0-9]+[mnsuµh]*)+$"
        }
    }
}`

// nolint:lll
const schemaAPP = `{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "gitlab.tp.sblogistica.ru/platform2.0/tools/app-spawner/config.json",
    "type": "object",
    "title": "The config file schema",
    "description": "The root schema comprises the entire JSON document.",
    "required": [
        "app"
    ],
    "properties": {
        "app": {
            "type": "array",
            "minItems": 1,
            "title": "The app schema",
            "description": "Array of the parameters for some application running",
            "items": {
                "type": "object",
                "title": "The application schema",
                "description": "Parameters for some application running.",
                "required": [
                    "name",
                    "command"
                ],
                "properties": {
                    "name": {
                        "type": "string",
                        "title": "The name schema",
                        "description": "An application name."
                    },
                    "cwd": {
                        "type": "string",
                        "title": "The cwd schema",
                        "description": "A application execution directory."
                    },
                    "command": {
                        "type": "string",
                        "title": "The command schema",
                        "description": "Command for running application with flags and arguments."
                    },
                    "first_run_delay": {
                        "$ref": "#/definitions/duration",
                        "title": "The first_run_delay schema",
                        "description": "Delay for first running an application, in golang time.Duration format"
                    },
                    "critical": {
                        "type": "boolean",
                        "title": "The critical schema",
                        "description": "Flag need total shutdown if that application interrupted."
                    },
                    "restart": {
                        "type": "boolean",
                        "title": "The restart schema",
                        "description": "Flag, need or not restart it if that application has interrupted."
                    },
                    "restart_interval": {
                        "$ref": "#/definitions/duration",
                        "title": "The restart_interval schema",
                        "description": "Delay before restart an interrupted application."
                    },
                    "env": {
                        "type": "object",
                        "title": "The env schema",
                        "description": "Block about environment variables.",
                        "required": [
                            "scope",
                            "include_regexp"
                        ],
                        "properties": {
                            "scope": {
                                "type": "string",
                                "enum": [
                                    "none",
                                    "app",
                                    "os",
                                    "all"
                                ],
                                "title": "The scope schema",
                                "description": "About scope envs: none - envs not pass, app - pass only described envs, os - pass only envs from operation system, all - pass os+app"
                            },
                            "include_regexp": {
                                "type": "string",
                                "title": "The include_regexp schema",
                                "description": "RegExp for filtering pass envs."
                            },
                            "keys": {
                                "type": "object",
                                "title": "The keys schema",
                                "description": "Envs in key:value format."
                            }
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "duration": {
            "type": "string",
            "pattern": "^([0-9]+[mnsuµh]*)+$"
        }
    }
}`

const regexpSchema = `{
    "$id": "https://jsonschema.sandbox.p2.sblogistica.ru/schemas/schema.json.v8",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "type": "object",
    "required": [
        "app"
    ],
    "properties": {
        "app": {
            "type": "object",
            "properties": {
                "localspace": {
                    "type": "object",
                    "properties": {
                        "namespaces": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        },
                        "gateway": {
                            "type": "string",
                            "pattern": "^[a-zA-Z]{1}[-0-9a-zA-Z]{0,30}[a-zA-Z0-9]{0,1}:(0{1}|[1-9][0-9]*)$"
                        }
                    },
                    "required": [
                        "gateway"
                    ]
                }
            }
        }
    }
}`

func TestDotConf_Validate(t *testing.T) {
	// testdata/toml/config_rabbit_correct.toml
	tests := []struct {
		name           string
		pathConfig     string
		envPrefix      string
		pathPrefix     string
		schema         string
		envs           [][2]string
		setSchemaToNil bool
		wantErr        bool
	}{
		{"config_rabbit_correct.toml", "testdata/toml/config_rabbit_correct.toml", "", "", schemaRabbit, nil, false, false},
		{"config_rabbit_correct.toml+incorrect_env", "testdata/toml/config_rabbit_correct.toml", "TESTDOT", "", schemaRabbit, [][2]string{{"TESTDOT_LOGGER_LEVEL", "PANIC"}}, false, true},
		{"config_rabbit_correct.toml_env_view_level_incorrect", "testdata/toml/config_rabbit_correct.toml", "TESTDOT", "", schemaRabbit, [][2]string{{"TESTDOT_LOGGER_VIEW_LEVEL", "77"}}, false, true},
		{"config_rabbit_incorrect.toml", "testdata/toml/config_rabbit_incorrect.toml", "", "", schemaRabbit, nil, false, true},
		{"json/config.json", "testdata/json/config.json", "", "", schemaAPP, nil, false, false},
		{"json/config_invalid_first_run_delay.json", "testdata/json/config_invalid_first_run_delay.json", "", "", schemaAPP, nil, false, true},
		{"toml/config.toml", "testdata/toml/config.toml", "", "", schemaAPP, nil, false, false},
		{"toml/config_short_invalid.toml", "testdata/toml/config_short_invalid.toml", "", "", schemaAPP, nil, false, true},
		{"yaml/config.yaml", "testdata/yaml/config.yaml", "", "", schemaAPP, nil, false, false},
		{"yaml/config_invalid_duration.yaml", "testdata/yaml/config_invalid_duration.yaml", "", "", schemaAPP, nil, false, true},
		{"json/config.json set schema to nil", "testdata/json/config.json", "", "", schemaAPP, nil, true, true},
		{
			"тестируем заполнение значениями переменных перед валидацией testdata/yaml/config_env_ext.yaml",
			"testdata/yaml/config_env_ext.yaml",
			"",
			"",
			schemaEnvExp,
			[][2]string{
				{"XYZ_APP_NAME", "app777"},
				{"WSX_APP_1_CMD", "ping"},
				{"GEC_BOOL_ENV", "true"},
				{"GEC_INT_CAR", "33"},
				{"GEC_NEXT_FOO_KX", "45"},
			},
			false,
			false,
		},
		{
			"testdata/toml/config_invalid_regexp.toml",
			"testdata/toml/config_invalid_regexp.toml",
			"",
			"",
			regexpSchema,
			nil,
			false,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for _, kv := range tt.envs {
				os.Setenv(kv[0], kv[1])
			}
			//
			dc := NewWithSchema(tt.envPrefix, tt.schema)
			// t.Logf("envs: %+v\n", dc.envValues())
			dc.Read(tt.pathConfig)
			// spew.Dump("dc.cfg.Value()", dc.cfg.Value())
			// spew.Dump("dc.ref", dc.refDataType)
			if tt.setSchemaToNil {
				dc.schema = nil
			}

			if err := dc.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("DotConf.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
			for _, kv := range tt.envs {
				os.Unsetenv(kv[0])
			}
		})
	}
}

func TestDotConf_SetSchema(t *testing.T) {
	type fields struct {
		envPrefix   string
		pathPrefix  string
		cfg         *gjson.Result
		schema      gojsonschema.JSONLoader
		refDataType map[string]jsonType
	}
	type args struct {
		schema string
	}
	tests := []struct {
		name         string
		schema       string
		minRefLength int
	}{
		{"schema_1", schemaRabbit, 3},
		{"schema_empty", "", 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := New("")
			dc.SetSchema(tt.schema)
			if len(tt.schema) > 0 {
				if len(dc.refDataType) < tt.minRefLength {
					t.Errorf("dc.SetSchema().refDataType.length=%d, wanr < %d", len(dc.refDataType), tt.minRefLength)
				}
			}
		})
	}
}
