package dotconf

import (
	"math"
	"os"
	"reflect"
	"testing"
	"time"
)

func TestDotConf_GetStringWithDefault(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathPrefix   string
		pathToConfig string
		k            string
		def          string
		env          map[string]string
		want         string
	}{
		{"yaml_log_app", "CRASH3", "", "testdata/yaml/config_log_app.yaml", "logger.level", "", nil, "info"},
		{"yaml_log_app_env", "CRASH3", "", "testdata/yaml/config_log_app.yaml", "logger.level", "", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, "warn"},
		{"yaml_env_value-env", "MY_APP", "", "testdata/yaml/config_env.yaml", "app.0.name", "", map[string]string{"MY_APP_NAME": "superApp"}, "superApp"},
		{"yaml_log_app_prefix-logger", "CRASH3", "logger.", "testdata/yaml/config_log_app.yaml", "level", "", nil, "info"},
		{"yaml_log_app_env_prefix-logger", "CRASH3", "logger.", "testdata/yaml/config_log_app.yaml", "level", "", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, "warn"},
		{"yaml_env_value-env_prefix-app", "MY_APP", "app.", "testdata/yaml/config_env.yaml", "0.name", "", map[string]string{"MY_APP_NAME": "superApp"}, "superApp"},
		{"yaml_log_app_prefix-logger_def", "CRASH3", "logger.", "testdata/yaml/config_log_app.yaml", "logger.level", "panic", nil, "panic"},
		{"yaml_log_app_env_prefix-logger_def", "CRASH3", "logger.", "testdata/yaml/config_log_app.yaml", "app.logger.level", "fatal", map[string]string{"CRASH3_LOGGER_LEVEL": "warn"}, "fatal"},
		{"yaml_env_value-env_prefix-app_def", "MY_APP", "app.", "testdata/yaml/config_env.yaml", "not.exists.0.name", "defaulAppName", map[string]string{"MY_APP_NAME": "superApp"}, "defaulAppName"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			dc.SetPathPrefix(tt.pathPrefix)
			if got := dc.GetStringWithDefault(tt.k, tt.def); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetString() = %v, want %v", got, tt.want)
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetIntWithDefault(t *testing.T) {
	tests := []struct {
		name         string
		envPrefix    string
		pathToConfig string
		k            string
		env          map[string]string
		defInt       int
		defInt32     int32
		defInt64     int64
		want         int
		want32       int32
		want64       int64
	}{
		{"yaml_log_app", "CRASH3", "testdata/yaml/config_log_app.yaml", "math.pi", nil, 33, 33, 33, 3, 3, 3},
		{"yaml_log_app_def", "CRASH3", "testdata/yaml/config_log_app.yaml", "math.foo.bar", nil, 33, 33, 33, 33, 33, 33},
		{"yaml_log_app_env", "CRASH3", "testdata/yaml/config_log_app.yaml", "math.pi", map[string]string{"CRASH3_MATH_PI": "5"}, 5, 5, 5, 5, 5, 5},
		{"yaml_log_app_env_def", "CRASH3", "testdata/yaml/config_log_app.yaml", "foo.bar", map[string]string{"CRASH3_MATH_PI": "5"}, 55, 55, 55, 55, 55, 55},
		{"yaml_env_exists_env_1.7", "CRASH3", "testdata/yaml/config_env.yaml", "math.root", map[string]string{"ROOT_PI": "1.77245385091"}, 0, 0, 0, 0, 0, 0},
		{"yaml_env_exists_env_1.7_def", "CRASH3", "testdata/yaml/config_env.yaml", "foo.bar", map[string]string{"ROOT_PI": "1.77245385091"}, 2, 2, 2, 2, 2, 2},
		{"yaml_env_exists_env_2", "CRASH3", "testdata/yaml/config_env.yaml", "math.root", map[string]string{"ROOT_PI": "2"}, 2, 2, 2, 2, 2, 2},
		{"yaml_env_not_exists_env", "CRASH3", "testdata/yaml/config_env.yaml", "math.root", nil, 0, 0, 0, 0, 0, 0},
		{"yaml_env_lim_zero", "CRASH3", "testdata/yaml/config_env.yaml", "lim.zero", map[string]string{"LIM_ZERO": "90071992547409929007199254740992"}, math.MaxInt64, -1, math.MaxInt64, math.MaxInt64, -1, math.MaxInt64},
		{"yaml_env_lim_zero_def", "CRASH3", "testdata/yaml/config_env.yaml", "lim.zero.not.exists", map[string]string{"LIM_ZERO": "90071992547409929007199254740992"}, -4, -1, -5, -4, -1, -5},
		{"yaml_env_lim_max", "CRASH3", "testdata/yaml/config_env.yaml", "lim.max", nil, 9007199254740992, 0, 9007199254740992, 9007199254740992, 0, 9007199254740992},
		{"yaml_env_lim_min", "CRASH3", "testdata/yaml/config_env.yaml", "lim.min", nil, -9007199254740992, 0, -9007199254740992, -9007199254740992, 0, -9007199254740992},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetIntWithDefault(tt.k, tt.defInt); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetIntWithDefault() = %v, want %v", got, tt.want)
			}
			if got32 := dc.GetInt32WithDefault(tt.k, tt.defInt32); !reflect.DeepEqual(got32, tt.want32) {
				t.Errorf("DotConf.GetInt32WithDefault() = %v, want %v", got32, tt.want32)
			}
			if got64 := dc.GetInt64WithDefault(tt.k, tt.defInt64); !reflect.DeepEqual(got64, tt.want64) {
				t.Errorf("DotConf.GetInt64WithDefault() = %v, want %v", got64, tt.want64)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetBoolWithDefault(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		def          bool
		want         bool
	}{
		{"yaml_env_bool_vtrue", "testdata/yaml/config_env.yaml", "APP1", "", nil, "bool.vtrue", false, true},
		{"yaml_env_not_exists_def", "testdata/yaml/config_env.yaml", "APP1", "", nil, "bool.vtrue.vfalse", true, true},
		{"yaml_env_bool_vfalse", "testdata/yaml/config_env.yaml", "APP1", "", nil, "bool.vfalse", true, false},
		{"yaml_env_bool_env", "testdata/yaml/config_env.yaml", "APP1", "", map[string]string{"APP1_BOOL_VFALSE": "true"}, "bool.vfalse", true, true},
		{"yaml_env_bool_env_env1", "testdata/yaml/config_env.yaml", "APP1", "", map[string]string{"BOOL_ENV": "true"}, "bool.env", true, true},
		{"yaml_env_bool_env_env0", "testdata/yaml/config_env.yaml", "APP1", "", map[string]string{"BOOL_ENV": "false"}, "bool.env", true, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetBoolWithDefault(tt.k, tt.def); got != tt.want {
				t.Errorf("DotConf.GetBoolWithDefault() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetDurationWithDefault(t *testing.T) {
	const defaultMetricInterval = time.Duration(15000) // 15

	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		def          time.Duration
		want         time.Duration
	}{
		{"yaml_env_time_sec15", "testdata/yaml/config_env.yaml", "APP1", "", nil, "time.sec15", time.Second, 15 * time.Second},
		{"yaml_env_time_sec15_env", "testdata/yaml/config_env.yaml", "APP1", "", map[string]string{"APP1_TIME_SEC15": "33s"}, "time.sec15", time.Second, 33 * time.Second},
		{"yaml_env_time_sec155_def", "testdata/yaml/config_env.yaml", "APP1", "", nil, "time.sec155", time.Second, time.Second},
		{"yaml_env_time_min30", "testdata/yaml/config_env.yaml", "APP1", "", nil, "time.min30", time.Second, 60 * 30 * time.Second},
		{"yaml_env_time_min30_env", "testdata/yaml/config_env.yaml", "APP1", "", map[string]string{"APP1_TIME_MIN30": "33m"}, "time.min30", time.Second, 33 * time.Minute},
		{"yaml_env_time_min30_def", "testdata/yaml/config_env.yaml", "APP1", "", nil, "time.min300", time.Minute, time.Minute},
		{"yaml_env_time_env", "testdata/yaml/config_env.yaml", "APP1", "", map[string]string{"TIME_ENV": "2m5s"}, "time.env", time.Second, 125 * time.Second},
		{"yaml_env_time_num_notexists", "testdata/yaml/config_env.yaml", "APP1", "", nil, "time.num.notexists", defaultMetricInterval, defaultMetricInterval},
		{"yaml_env_time_num", "testdata/yaml/config_env.yaml", "APP1", "", nil, "time.num", defaultMetricInterval * 5, defaultMetricInterval},
		{"yaml_env_time_numenv", "testdata/yaml/config_env.yaml", "APP1", "", nil, "time.numenv", defaultMetricInterval * 4, defaultMetricInterval * 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetDurationWithDefault(tt.k, tt.def); got != tt.want {
				t.Errorf("DotConf.GetDurationWithDefault() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetFloat64WithDefault(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		def          float64
		want         float64
	}{
		{"yaml_env_float_v7", "testdata/yaml/config_env.yaml", "APP1", "", nil, "float.v7", 1.1, 7.8765},
		{"yaml_env_float_v7_env", "testdata/yaml/config_env.yaml", "APP1", "", map[string]string{"APP1_FLOAT_V7": "3.33"}, "float.v7", 1.1, 3.33},
		{"yaml_env_float_v7_def", "testdata/yaml/config_env.yaml", "APP1", "", nil, "float.v8", 1.1, 1.1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetFloat64WithDefault(tt.k, tt.def); got != tt.want {
				t.Errorf("DotConf.GetFloat64WithDefault() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetIntSliceWithDefault(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		def          []int
		want         []int
	}{
		{"yaml_env_ints", "testdata/yaml/config_env.yaml", "APP1", "", nil, "ints", nil, []int{1, 2, 3}},
		{"yaml_env_ints_def", "testdata/yaml/config_env.yaml", "APP1", "", nil, "not.ints", []int{4, 5, 6}, []int{4, 5, 6}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)
			if got := dc.GetIntSliceWithDefault(tt.k, tt.def); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetIntSliceWithDefault() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDotConf_GetStringMapWithDefault(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		def          map[string]interface{}
		want         map[string]interface{}
	}{
		{"yaml_env_logger", "testdata/yaml/config_env.yaml", "", "", nil, "map", nil, map[string]interface{}{
			"v1": true,
			"v2": "3s",
			"v3": 33.44,
			"v4": map[string]interface{}{"v44": "string"},
		}},
		{"yaml_env_logger_def", "testdata/yaml/config_env.yaml", "", "", nil, "blogger", map[string]interface{}{
			"enable":         true,
			"view_timestamp": false,
			"view_level":     true,
		}, map[string]interface{}{
			"enable":         true,
			"view_timestamp": false,
			"view_level":     true,
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetStringMapWithDefault(tt.k, tt.def); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetStringMapWithDefault() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}

func TestDotConf_GetStringMapStringWithDefault(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		def          map[string]string
		want         map[string]string
	}{
		{"yaml_env_logger", "testdata/yaml/config_env.yaml", "", "", nil, "mapstr", nil, map[string]string{
			"v1": "true",
			"v2": "3s",
			"v3": "33.44",
		}},
		{"yaml_env_logger_def", "testdata/yaml/config_env.yaml", "", "", nil, "blogger", map[string]string{
			"enable":         "true",
			"view_timestamp": "false",
			"view_level":     "true",
		}, map[string]string{
			"enable":         "true",
			"view_timestamp": "false",
			"view_level":     "true",
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}

			dc := New(tt.envPrefix)
			dc.Read(tt.pathToConfig)

			if got := dc.GetStringMapStringWithDefault(tt.k, tt.def); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.GetStringMapStringWithDefault() = %v, want %v", got, tt.want)
			}
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}
