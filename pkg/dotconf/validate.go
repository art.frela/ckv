package dotconf

import (
	"errors"
	"fmt"

	"github.com/xeipuuv/gojsonschema"
)

// ErrNoSchema no schema error.
var ErrNoSchema = errors.New("empty schema")

// Validate validates config with early passed jsonSchema.
func (dc *DotConf) Validate() error {
	return dc.jsonValidateXEI()
}

// SetSchema sets jsonSchema.
func (dc *DotConf) SetSchema(schema string) {
	schemaLoader := gojsonschema.NewStringLoader(schema)
	dc.schema = schemaLoader
	props, err := schemaLoader.LoadJSON()
	if err != nil {
		return
	}
	ref := make(map[string]jsonType)
	if m, ok := props.(map[string]interface{}); ok {
		ref = makeRef(m)
	}
	dc.refDataType = ref
}

func (dc *DotConf) jsonValidateXEI() error {
	if dc.schema == nil {
		return ErrNoSchema
	}
	documentLoader := gojsonschema.NewGoLoader(dc.cfg.Value())
	result, err := gojsonschema.Validate(dc.schema, documentLoader)
	if err != nil {
		return fmt.Errorf("validate err, %w", err)
	}
	errSlice := result.Errors()
	errMsg := ""
	for i := range errSlice {
		errMsg += errSlice[i].String() + "; "
	}
	if len(errMsg) > 0 {
		return errors.New(errMsg)
	}
	return nil
}
