package dotconf

import (
	"os"
	"reflect"
	"testing"
)

func TestDotConf_env2key(t *testing.T) {
	tests := []struct {
		name      string
		envPrefix string
		envName   string
		envs      [][2]string
		want      string
	}{
		{"app1_exists", "app1", "APP1_LOGGER_ENABLE", [][2]string{{"APP1_LOGGER_ENABLE", "true"}}, "logger.enable"},
		{"app1x2_exists", "app1", "APP1_LOGGER_APP1_ENABLE", [][2]string{{"APP1_LOGGER_APP1_ENABLE", "true"}}, "logger.app1.enable"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup env
			for _, kv := range tt.envs {
				os.Setenv(kv[0], kv[1])
			}
			dc := New(tt.envPrefix)
			if got := dc.env2key(tt.envName); got != tt.want {
				t.Errorf("DotConf.env2key() = %v, want %v", got, tt.want)
			}
			// unset envs
			for _, kv := range tt.envs {
				os.Unsetenv(kv[0])
			}
		})
	}
}

func TestDotConf_envValues(t *testing.T) {
	tests := []struct {
		name      string
		envPrefix string
		envs      [][2]string
		want      []envValue
	}{
		{
			"simple_1env",
			"app1",
			[][2]string{{"APP1_LOGGER_ENABLE", "true"}},
			[]envValue{
				{
					key:   []string{"logger", "enable"},
					value: "true",
				},
			},
		},
		{
			"simple_2env",
			"app1",
			[][2]string{{"APP1_LOGGER_ENABLE", "true"}, {"APP1_APP_CHECK", "1"}, {"APP2_LOGGER_VIEW", "true"}},
			[]envValue{
				{
					key:   []string{"logger", "enable"},
					value: "true",
				},
				{
					key:   []string{"app", "check"},
					value: "1",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// setup env
			for _, kv := range tt.envs {
				os.Setenv(kv[0], kv[1])
			}
			dc := New(tt.envPrefix)
			if got := dc.envValues(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DotConf.envValues() = %v, want %v", got, tt.want)
			}
			// unset envs
			for _, kv := range tt.envs {
				os.Unsetenv(kv[0])
			}
		})
	}
}

func Test_envValue_shift(t *testing.T) {
	tests := []struct {
		name string
		key  []string
		want []string
	}{
		{
			"simple_2to1",
			[]string{"debug", "logger"},
			[]string{"logger"},
		},
		{
			"simple_1to0",
			[]string{"debug"},
			[]string{},
		},
		{
			"simple_0to0",
			[]string{},
			[]string{},
		},
		{
			"simple_4to3",
			[]string{"debug", "logger", "view", "1"},
			[]string{"logger", "view", "1"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ev := &envValue{
				key: tt.key,
			}
			ev.shift()
			if !reflect.DeepEqual(ev.key, tt.want) {
				t.Errorf("ev.shift() key=%v, want %v", ev.key, tt.want)
			}
		})
	}
}

const schemaFoo = `{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "gitlab.tp.sblogistica.ru/platform2.0/tools/rabbitmq-healthchecker/config.logger.json",
    "type": "object",
    "title": "The config schema",
    "description": "The config schema comprises the entire config document.",
    "properties": {
        "logger": {
            "type": "object",
            "properties": {
                "level": {
                    "type": "string"
                },
                "enable": {
                    "type": "boolean"
                },
                "view_timestamp": {
                    "type": "boolean"
                },
                "view_level": {
                    "type": "boolean"
                },
				"period": {
                    "$ref": "#/definitions/duration"
                }
            }
        },
        "amqps": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/duration"
            }
        },
        "foo": {
            "type": "object",
            "properties": {
                "bar":{
                    "$ref": "#/definitions/counter"
                }
            }
        }
    },
    "definitions": {
        "duration": {
            "type": "string"
        },
        "counter": {
            "type":"number"
        }
    }
}`

func Test_mergeMapEnv(t *testing.T) {
	dc := NewWithSchema("app1", schemaFoo)
	tests := []struct {
		name string
		m    map[string]interface{}
		ev   envValue
		want map[string]interface{}
	}{
		{
			"logger.level_debug2error",
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "debug",
				},
			},
			envValue{
				key:   []string{"logger", "level"},
				value: "error",
			},
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "error",
				},
			},
		},
		{
			"logger.view_level_add_true",
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "debug",
				},
			},
			envValue{
				key:   []string{"logger", "view_level"},
				value: "true",
			},
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level":      "debug",
					"view_level": true,
				},
			},
		},
		{
			"logger_add_app_check_true",
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "debug",
				},
			},
			envValue{
				key:   []string{"app", "check"},
				value: "true",
			},
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "debug",
				},
				"app": map[string]interface{}{
					"check": "true",
				},
			},
		},
		{
			"add_foo-bar_integer",
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "debug",
				},
			},
			envValue{
				key:   []string{"foo", "bar"},
				value: "77",
			},
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "debug",
				},
				"foo": map[string]interface{}{
					"bar": int64(77),
				},
			},
		},
		{
			"add_foo-bar_float",
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "debug",
				},
			},
			envValue{
				key:   []string{"foo", "bar"},
				value: "3.14",
			},
			map[string]interface{}{
				"logger": map[string]interface{}{
					"level": "debug",
				},
				"foo": map[string]interface{}{
					"bar": 3.14,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc.mergeMapEnv(tt.m, tt.ev, "")
			if !reflect.DeepEqual(tt.m, tt.want) {
				t.Errorf("\nmergeMapEnv()\n%#+v\nwant\n%#+v\n", tt.m, tt.want)
			}
		})
	}
}

// func Test_envValue_iface(t *testing.T) {
// 	type fields struct {
// 		key   []string
// 		value string
// 	}
// 	tests := []struct {
// 		name  string
// 		key   []string
// 		value string
// 		want  interface{}
// 	}{
// 		{
// 			"simple_string",
// 			[]string{"single"},
// 			"value",
// 			map[string]interface{}{
// 				"single": "value",
// 			},
// 		},
// 		{
// 			"simple_map.1",
// 			[]string{"first", "second"},
// 			"value",
// 			map[string]interface{}{
// 				"first": map[string]interface{}{
// 					"second": "value",
// 				},
// 			},
// 		},
// 		{
// 			"simple_0key",
// 			[]string{},
// 			"value",
// 			"value",
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			ev := &envValue{
// 				key:   tt.key,
// 				value: tt.value,
// 			}
// 			if got := ev.iface(); !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("envValue.iface() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// nolint:lll
const schemaLogger = `{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "gitlab.tp.sblogistica.ru/platform2.0/tools/rabbitmq-healthchecker/config.logger.json",
    "type": "object",
    "title": "The config schema",
    "description": "The config schema comprises the entire config document.",
    "properties": {
        "logger": {
            "type": "object",
            "properties": {
                "level": {
                    "type": "string"
                },
                "enable": {
                    "type": "boolean"
                },
                "view_timestamp": {
                    "type": "boolean"
                },
                "view_level": {
                    "type": "boolean"
                },
				"period": {
                    "$ref": "#/definitions/duration"
                }
            }
        }
    },
    "definitions": {
        "duration": {
            "type": "string"
        }
    }
}`

func Test_fillProps(t *testing.T) {
	dc := NewWithSchema("app1", schemaLogger)
	props, _ := dc.schema.LoadJSON()
	dcr := NewWithSchema("app2", schemaRabbit)
	propsr, _ := dcr.schema.LoadJSON()
	dcf := NewWithSchema("app3", schemaFoo)
	propsf, _ := dcf.schema.LoadJSON()
	// spew.Dump(props)
	tests := []struct {
		name    string
		isProps bool
		props   map[string]interface{}
		propKey string
		ref     map[string]jsonType
		defs    map[string]jsonType
		want    map[string]jsonType
	}{
		{
			"schemaLogger_1",
			true,
			props.(map[string]interface{})["properties"].(map[string]interface{}),
			"",
			map[string]jsonType{},
			map[string]jsonType{
				"duration": "string",
			},
			map[string]jsonType{
				"logger.level":          "string",
				"logger.enable":         "boolean",
				"logger.view_timestamp": "boolean",
				"logger.view_level":     "boolean",
				"logger.period":         "string",
			},
		},
		{
			"definitions_schemalLogger_1",
			false,
			props.(map[string]interface{})["definitions"].(map[string]interface{}),
			"",
			map[string]jsonType{},
			map[string]jsonType{},
			map[string]jsonType{
				"duration": "string",
			},
		},
		{
			"schemaRabbit_1",
			true,
			propsr.(map[string]interface{})["properties"].(map[string]interface{}),
			"",
			map[string]jsonType{},
			map[string]jsonType{
				"duration": "string",
			},
			map[string]jsonType{
				"logger.level":                "string",
				"logger.enable":               "boolean",
				"logger.view_timestamp":       "boolean",
				"logger.view_level":           "boolean",
				"api.listening":               "string",
				"api.prefix":                  "string",
				"rabbitmq.reconnect_interval": "string",
				"rabbitmq.ping_interval":      "string",
				"rabbitmq.url":                "string",
				"rabbitmq.queue":              "string",
			},
		},
		{
			"schemaFoo_1",
			true,
			propsf.(map[string]interface{})["properties"].(map[string]interface{}),
			"",
			map[string]jsonType{},
			map[string]jsonType{
				"duration": "string",
				"counter":  "integer",
			},
			map[string]jsonType{
				"logger.level":          "string",
				"logger.enable":         "boolean",
				"logger.view_timestamp": "boolean",
				"logger.view_level":     "boolean",
				"logger.period":         "string",
				"amqps":                 "string",
				"foo.bar":               "integer",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.isProps {
				fillProps(tt.propKey, tt.props, tt.ref, tt.defs)
			} else {
				fillProps(tt.propKey, tt.props, tt.ref, tt.ref)
			}

			if !reflect.DeepEqual(tt.ref, tt.want) {
				t.Errorf("fillProps()\n%#+v\n%#+v\n", tt.ref, tt.want)
			}
		})
	}
}

func Test_makeRef(t *testing.T) {
	dc := NewWithSchema("app1", schemaLogger)
	props, _ := dc.schema.LoadJSON()
	dcr := NewWithSchema("app2", schemaRabbit)
	propsr, _ := dcr.schema.LoadJSON()
	dcf := NewWithSchema("app3", schemaFoo)
	propsf, _ := dcf.schema.LoadJSON()
	dca := NewWithSchema("app4", schemaAPP)
	propsa, _ := dca.schema.LoadJSON()
	type args struct {
		schema map[string]interface{}
	}
	tests := []struct {
		name   string
		schema map[string]interface{}
		want   map[string]jsonType
	}{
		{
			"schemaLogger_1",
			props.(map[string]interface{}),
			map[string]jsonType{
				"logger.level":          "string",
				"logger.enable":         "boolean",
				"logger.view_timestamp": "boolean",
				"logger.view_level":     "boolean",
				"logger.period":         "string",
			},
		},
		{
			"schemaRabbit_1",
			propsr.(map[string]interface{}),
			map[string]jsonType{
				"logger.level":                "string",
				"logger.enable":               "boolean",
				"logger.view_timestamp":       "boolean",
				"logger.view_level":           "boolean",
				"api.listening":               "string",
				"api.prefix":                  "string",
				"rabbitmq.reconnect_interval": "string",
				"rabbitmq.ping_interval":      "string",
				"rabbitmq.url":                "string",
				"rabbitmq.queue":              "string",
			},
		},
		{
			"schemaFoo_1",
			propsf.(map[string]interface{}),
			map[string]jsonType{
				"logger.level":          "string",
				"logger.enable":         "boolean",
				"logger.view_timestamp": "boolean",
				"logger.view_level":     "boolean",
				"logger.period":         "string",
				"amqps":                 "string",
				"foo.bar":               "number",
			},
		},
		{
			"schemaAPP_1",
			propsa.(map[string]interface{}),
			map[string]jsonType{
				"app.command":            "string",
				"app.critical":           "boolean",
				"app.cwd":                "string",
				"app.env.include_regexp": "string",
				"app.env.scope":          "string",
				"app.first_run_delay":    "string",
				"app.name":               "string",
				"app.restart":            "boolean",
				"app.restart_interval":   "string",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := makeRef(tt.schema); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("makeRef()\n%#+v\nwant\n%#+v\n", got, tt.want)
			}
		})
	}
}
