package dotconf

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	minEnvParts = 2
)

// envValue is env var presentation,
// where key is slice env name elements: ENV_NAME_PART1_PART2 -> ["env","name","part1","part2"],
// value common string.
type envValue struct {
	value string
	key   []string
}

func (ev *envValue) shift() {
	if len(ev.key) == 0 {
		return
	}
	if len(ev.key) == 1 {
		ev.key = make([]string, 0)
		return
	}
	ev.key = ev.key[1:]
}

func (ev *envValue) iface(ref map[string]jsonType, prefix string) interface{} {
	if len(ev.key) == 0 {
		if kind, ok := ref[prefix]; ok {
			return kind.iface(ev.value)
		}
		return ev.value
	}
	k := ev.key[0]
	newPrefix := k
	if prefix != "" {
		newPrefix = prefix + envParseSeparator + k
	}
	if len(ev.key) == 1 {
		if kind, ok := ref[newPrefix]; ok {
			return map[string]interface{}{k: kind.iface(ev.value)}
		}
		return map[string]interface{}{ev.key[0]: ev.value}
	}
	res := make(map[string]interface{})
	ev.shift()
	res[k] = ev.iface(ref, newPrefix)
	return res
}

func (dc *DotConf) mergeManyMapEnv(m map[string]interface{}, evs []envValue, prefix string) {
	for i := range evs {
		dc.mergeMapEnv(m, evs[i], prefix)
	}
}

//nolint:gocognit
func (dc *DotConf) mergeMapEnv(m map[string]interface{}, ev envValue, prefix string) {
	if len(ev.key) == 0 {
		return
	}

	evKey := ev.key[0]

	newPrefix := prefix + envParseSeparator + evKey
	if prefix == "" {
		newPrefix = evKey
	}

	for k, v := range m {
		if k == evKey {
			switch t := v.(type) {
			case map[string]interface{}:
				ev.shift()
				dc.mergeMapEnv(t, ev, newPrefix)
				m[k] = t
				return
			case []interface{}: // TODO: make correct tests
				for i, vitem := range t {
					if tt, assertOK := vitem.(map[string]interface{}); assertOK {
						newPrefix = fmt.Sprintf("%s%s%d", newPrefix, envParseSeparator, i)
						dc.mergeMapEnv(tt, ev, newPrefix)
						t[i] = tt
					}
				}
				m[k] = t
				return
			default:
				if kind, ok := dc.refDataType[newPrefix]; ok {
					m[k] = kind.iface(ev.value)
					return
				}
				m[k] = ev.value
				return
			}
		}
	}
	ev.shift()
	m[evKey] = ev.iface(dc.refDataType, newPrefix)
}

func (dc *DotConf) envValues() []envValue {
	res := make([]envValue, 0)
	for _, env := range os.Environ() {
		if strings.HasPrefix(env, dc.envPrefix) {
			ekv := strings.Split(env, "=")
			if len(ekv) < minEnvParts {
				continue
			}
			ekv[1] = strings.Join(ekv[1:], "=")
			key := dc.env2key(ekv[0])
			if key == "" {
				continue
			}
			res = append(res, envValue{key: strings.Split(key, envParseSeparator), value: ekv[1]})
		}
	}
	return res
}

// env2key compares env with config paths,
//
// by trimming env prefix if exists,
// iterates on reference of keys (filled by json scheme),
// checks regexp match and returns matched reference key.
func (dc *DotConf) env2key(envName string) string {
	prefix := ""
	if dc.envPrefix != "" {
		prefix = dc.envPrefix + envSeparator
	}
	k := strings.Replace(envName, prefix, "", 1) // TODO: replace to regexp!
	for rk := range dc.refDataType {             // corresponds to the config with the scheme
		re := makeRegexp(rk)
		if re.MatchString(k) {
			return rk
		}
	}
	//
	if len(dc.refDataType) > 0 { // corresponds to the config with the scheme
		return ""
	}
	return strings.ReplaceAll(strings.ToLower(k), envSeparator, envParseSeparator)
}

func makeRegexp(key string) *regexp.Regexp {
	parts := strings.Split(strings.ToUpper(key), envParseSeparator)
	reg := fmt.Sprintf("(?m)^(%s)$", strings.Join(parts, ")_("))
	return regexp.MustCompile(reg)
}

type jsonType string

const base10 = 10

func (jt jsonType) iface(str string) interface{} {
	switch jt {
	case "string":
		return str
	case "array":
		return []interface{}{str}
	case "integer", "number":
		if strings.Contains(str, ".") {
			// float
			v, err := strconv.ParseFloat(str, x64)
			if err != nil {
				return str
			}
			return v
		}
		v, err := strconv.ParseInt(str, base10, x64)
		if err != nil {
			return str
		}
		return v
	case "boolean":
		v, err := strconv.ParseBool(str)
		if err != nil {
			return str
		}
		return v
	}
	return str
}

// helpers

// makeRef processes jsonSchema and makes reference with exists keys in dot notation,
// embedded fields will be dot separated string, example:
//
// json config: {"app":{"check":{"enabled": true, "period": "5s"}}}
//
// will be presented as two records:
//
// app.check.enabled = boolean;
// app.check.period = string.
func makeRef(schema map[string]interface{}) map[string]jsonType {
	mapType := make(map[string]jsonType)
	defType := make(map[string]jsonType)
	if defs, defOK := schema["definitions"]; defOK {
		fillProps("", defs.(map[string]interface{}), defType, defType)
	}
	if props, ok := schema["properties"]; ok {
		fillProps("", props.(map[string]interface{}), mapType, defType)
	}
	return mapType
}

// fillProps iterates on the map[string]interface{}
//
// when interface{} is string then add value to map,
// else call fillObjectProps.
func fillProps(propsKey string,
	propsForFill map[string]interface{}, refForFill, refDefinitions map[string]jsonType,
) {
	//
	for propName, propValue := range propsForFill {
		newPropKey := propsKey + envParseSeparator + propName
		if propsKey == "" {
			newPropKey = propName
		}
		switch propValueType := propValue.(type) {
		case map[string]interface{}:
			fillObjectProps(newPropKey, propValueType, refForFill, refDefinitions)
		case string:
			refForFill[newPropKey] = jsonType(propValueType)
		}
	}
}

// fillObjectProps processes properties as json object = map[string]interface{}.
func fillObjectProps(propsKey string, objectProps map[string]interface{},
	refForFill, refDefinitions map[string]jsonType,
) {
	//
	for propName, propObject := range objectProps {
		if propName != "type" {
			if propName == "$ref" {
				refFollower(propObject.(string), propsKey, refForFill, refDefinitions)
			}
			continue
		}
		switch propObject.(string) {
		case "object":
			if embedProps, ok := objectProps["properties"].(map[string]interface{}); ok {
				fillProps(propsKey, embedProps, refForFill, refDefinitions)
			}
		case "array":
			// propsKey += envParseSeparator + "0"
			if itemProps, ok := objectProps["items"].(map[string]interface{}); ok {
				fillObjectProps(propsKey, itemProps, refForFill, refDefinitions)
			}
		default:
			refForFill[propsKey] = jsonType(propObject.(string))
		}
	}
}

const (
	minPathEl = 3
)

func refFollower(keyPath, key string, ref, defs map[string]jsonType) {
	parts := strings.Split(keyPath, "/")
	if len(parts) < minPathEl {
		return
	}
	refKey := strings.Join(parts[minPathEl-1:], envParseSeparator)
	if defVal, ok := defs[refKey]; ok {
		ref[key] = defVal
	}
}
