// Code generated by mockery v2.42.3. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// MockEncoder is an autogenerated mock type for the Encoder type
type MockEncoder struct {
	mock.Mock
}

type MockEncoder_Expecter struct {
	mock *mock.Mock
}

func (_m *MockEncoder) EXPECT() *MockEncoder_Expecter {
	return &MockEncoder_Expecter{mock: &_m.Mock}
}

// Encode provides a mock function with given fields: v
func (_m *MockEncoder) Encode(v interface{}) ([]byte, error) {
	ret := _m.Called(v)

	if len(ret) == 0 {
		panic("no return value specified for Encode")
	}

	var r0 []byte
	var r1 error
	if rf, ok := ret.Get(0).(func(interface{}) ([]byte, error)); ok {
		return rf(v)
	}
	if rf, ok := ret.Get(0).(func(interface{}) []byte); ok {
		r0 = rf(v)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	if rf, ok := ret.Get(1).(func(interface{}) error); ok {
		r1 = rf(v)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// MockEncoder_Encode_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Encode'
type MockEncoder_Encode_Call struct {
	*mock.Call
}

// Encode is a helper method to define mock.On call
//   - v interface{}
func (_e *MockEncoder_Expecter) Encode(v interface{}) *MockEncoder_Encode_Call {
	return &MockEncoder_Encode_Call{Call: _e.mock.On("Encode", v)}
}

func (_c *MockEncoder_Encode_Call) Run(run func(v interface{})) *MockEncoder_Encode_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(interface{}))
	})
	return _c
}

func (_c *MockEncoder_Encode_Call) Return(_a0 []byte, _a1 error) *MockEncoder_Encode_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *MockEncoder_Encode_Call) RunAndReturn(run func(interface{}) ([]byte, error)) *MockEncoder_Encode_Call {
	_c.Call.Return(run)
	return _c
}

// NewMockEncoder creates a new instance of MockEncoder. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMockEncoder(t interface {
	mock.TestingT
	Cleanup(func())
}) *MockEncoder {
	mock := &MockEncoder{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
