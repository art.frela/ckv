package encoding

import (
	"reflect"
	"testing"

	emock "gitlab.com/art.frela/ckv/pkg/dotconf/encoding/mocks"
)

func TestEncoderRegistry_RegisterEncoder(t *testing.T) {
	enc := emock.NewMockEncoder(t)

	tests := []struct {
		name     string
		encoders map[string]Encoder
		format   string
		enc      Encoder
		wantErr  bool
	}{
		{
			name:     "test.1 ok",
			encoders: map[string]Encoder{},
			format:   "fmt",
			enc:      enc,
			wantErr:  false,
		},
		{
			name:     "test.1 err not found",
			encoders: map[string]Encoder{"fmt": enc},
			format:   "fmt",
			enc:      enc,
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &EncoderRegistry{
				encoders: tt.encoders,
			}

			if err := e.RegisterEncoder(tt.format, tt.enc); (err != nil) != tt.wantErr {
				t.Errorf("EncoderRegistry.RegisterEncoder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestEncoderRegistry_Encode(t *testing.T) {
	btsOK := []byte(`{"root": "master"}`)
	strOK := struct{ Root string }{}

	enc := emock.NewMockEncoder(t)
	enc.EXPECT().Encode(strOK).Return(btsOK, nil)

	tests := []struct {
		name     string
		encoders map[string]Encoder
		format   string
		v        interface{}
		want     []byte
		wantErr  bool
	}{
		{
			name:     "test.1 ok",
			encoders: map[string]Encoder{"fmt": enc},
			format:   "fmt",
			v:        strOK,
			want:     btsOK,
			wantErr:  false,
		},
		{
			name:     "test.2 err not found",
			encoders: map[string]Encoder{},
			format:   "fmt",
			v:        nil,
			want:     nil,
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := NewEncoderRegistry()
			e.encoders = tt.encoders

			got, err := e.Encode(tt.format, tt.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncoderRegistry.Encode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EncoderRegistry.Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}
