package encoding

import (
	"testing"

	emock "gitlab.com/art.frela/ckv/pkg/dotconf/encoding/mocks"
)

func TestDecoderRegistry_Decode(t *testing.T) {
	btsOK := []byte(`{"root": "master"}`)
	strOK := struct{ Root string }{}

	dec := emock.NewMockDecoder(t)
	dec.EXPECT().Decode(btsOK, strOK).Return(nil)

	tests := []struct {
		name       string
		formatReg  string
		formatDec  string
		decoder    Decoder
		b          []byte
		v          interface{}
		wantErrDec bool
		wantErr    bool
	}{
		{
			name:       "test.1 ok",
			formatReg:  "myfmt",
			formatDec:  "myfmt",
			decoder:    dec,
			b:          btsOK,
			v:          strOK,
			wantErrDec: false,
			wantErr:    false,
		},
		{
			name:       "test.2 err not found",
			formatReg:  "myfmt",
			formatDec:  "myfmt2",
			decoder:    dec,
			b:          btsOK,
			v:          strOK,
			wantErrDec: false,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dr := NewDecoderRegistry()
			if err := dr.RegisterDecoder(tt.formatReg, tt.decoder); (err != nil) != tt.wantErrDec {
				t.Errorf("DecoderRegistry.RegisterDecoder() error = %v, wantErr %v", err, tt.wantErrDec)
			}

			if err := dr.Decode(tt.formatDec, tt.b, tt.v); (err != nil) != tt.wantErr {
				t.Errorf("DecoderRegistry.Decode() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDecoderRegistry_RegisterDecoder(t *testing.T) {
	dec := emock.NewMockDecoder(t)

	tests := []struct {
		name     string
		decoders map[string]Decoder
		format   string
		dec      Decoder
		wantErr  bool
	}{
		{
			name:     "test.1 ok",
			decoders: map[string]Decoder{},
			format:   "fmt",
			dec:      dec,
			wantErr:  false,
		},
		{
			name:     "test.1 err not found",
			decoders: map[string]Decoder{"fmt": dec},
			format:   "fmt",
			dec:      dec,
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &DecoderRegistry{
				decoders: tt.decoders,
			}

			if err := e.RegisterDecoder(tt.format, tt.dec); (err != nil) != tt.wantErr {
				t.Errorf("DecoderRegistry.RegisterDecoder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
