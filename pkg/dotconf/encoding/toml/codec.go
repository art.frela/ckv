package toml

import (
	"github.com/pelletier/go-toml/v2"
)

// Codec implements the encoding.Encoder and encoding.Decoder interfaces for TOML encoding.
type Codec struct{}

func (c Codec) Encode(v interface{}) ([]byte, error) {
	return toml.Marshal(v)
}

func (c Codec) Decode(b []byte, v interface{}) error {
	return toml.Unmarshal(b, v)
}
