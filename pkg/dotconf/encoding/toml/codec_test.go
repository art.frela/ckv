package toml

import (
	"reflect"
	"testing"
)

func TestCodec_Encode(t *testing.T) {
	btsOK := []byte(`root = 'master'
`)
	strOK := struct {
		Root string `toml:"root"`
	}{Root: "master"}

	tests := []struct {
		name    string
		v       interface{}
		want    []byte
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			v:       strOK,
			want:    btsOK,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Codec{}

			got, err := c.Encode(tt.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("Codec.Encode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Codec.Encode() = %v, want %v", string(got), string(tt.want))
			}
		})
	}
}

func TestCodec_DecodeStruct(t *testing.T) {
	btsOK := []byte(`root = 'master'
`)
	strOK := struct {
		Root string `toml:"root"`
	}{}
	strOKwant := struct {
		Root string `toml:"root"`
	}{Root: "master"}

	tests := []struct {
		name string
		b    []byte
		v    struct {
			Root string `toml:"root"`
		}
		want struct {
			Root string `toml:"root"`
		}
		wantErr bool
	}{
		{
			name:    "test.1 ok struct",
			b:       btsOK,
			v:       strOK,
			want:    strOKwant,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Codec{}

			if err := c.Decode(tt.b, &tt.v); (err != nil) != tt.wantErr {
				t.Errorf("Codec.Decode() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(tt.v, tt.want) {
				t.Errorf("Codec.Decode() = %+#v, want %+#v", tt.v, tt.want)
			}
		})
	}
}

func TestCodec_DecodeMap(t *testing.T) {
	btsOK := []byte(`root = "master"
`)

	mOK := map[string]interface{}{}
	mOKwant := map[string]interface{}{"root": "master"}

	tests := []struct {
		name    string
		b       []byte
		v       interface{}
		want    interface{}
		wantErr bool
	}{
		{
			name:    "test.1 ok map",
			b:       btsOK,
			v:       mOK,
			want:    mOKwant,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Codec{}

			if err := c.Decode(tt.b, &tt.v); (err != nil) != tt.wantErr {
				t.Errorf("Codec.Decode() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(tt.v, tt.want) {
				t.Errorf("Codec.Decode() = %+#v, want %+#v", tt.v, tt.want)
			}
		})
	}
}
