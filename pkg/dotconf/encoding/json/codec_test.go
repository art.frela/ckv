package json

import (
	"reflect"
	"testing"
)

func TestCodec_Encode(t *testing.T) {
	btsOK := []byte(`{
  "root": "master"
}`)
	strOK := struct {
		Root string `json:"root"`
	}{Root: "master"}

	tests := []struct {
		name    string
		v       interface{}
		want    []byte
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			v:       strOK,
			want:    btsOK,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Codec{}

			got, err := c.Encode(tt.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("Codec.Encode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Codec.Encode() = %v, want %v", string(got), string(tt.want))
			}
		})
	}
}

func TestCodec_Decode(t *testing.T) {
	btsOK := []byte(`{
		"root": "master"
	  }`)
	strOK := struct {
		Root string `json:"root"`
	}{}

	tests := []struct {
		name    string
		b       []byte
		v       interface{}
		wantErr bool
	}{
		{
			name:    "test.1 ok",
			b:       btsOK,
			v:       strOK,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Codec{}

			if err := c.Decode(tt.b, &tt.v); (err != nil) != tt.wantErr {
				t.Errorf("Codec.Decode() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
