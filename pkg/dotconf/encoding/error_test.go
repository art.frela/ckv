package encoding

import "testing"

func Test_encodingError_Error(t *testing.T) {
	tests := []struct {
		name string
		e    encodingError
		want string
	}{
		{
			name: "test.1 ok",
			e:    "some error",
			want: "some error",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.e.Error(); got != tt.want {
				t.Errorf("encodingError.Error() = %v, want %v", got, tt.want)
			}
		})
	}
}
