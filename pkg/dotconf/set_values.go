package dotconf

import (
	"strings"

	"github.com/tidwall/gjson"
)

const (
	dotSeparator = "."
)

func (dc *DotConf) Set(key string, value interface{}) {
	cfg, ok := dc.Value().(map[string]interface{})
	if !ok {
		return
	}
	mergeMAPs(cfg, makeMap(key, value))
	bts, err := encoderRegistry.Encode(FormatJSON, cfg)
	if err != nil {
		return
	}
	res := gjson.ParseBytes(bts)
	dc.cfg = &res
}

func makeMap(key string, value interface{}) map[string]interface{} {
	keys := strings.Split(key, dotSeparator)
	if len(keys) == 1 {
		return map[string]interface{}{key: value}
	}
	shiftKey := strings.Join(keys[1:], dotSeparator)
	return map[string]interface{}{keys[0]: makeMap(shiftKey, value)}
}
