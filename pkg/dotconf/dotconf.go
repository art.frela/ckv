/*
Package dotconf contains methods for working with configs in json, yaml, toml formats
and access values by json dot notation.
*/
package dotconf

import (
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	enc "gitlab.com/art.frela/ckv/pkg/dotconf/encoding"
	"gitlab.com/art.frela/ckv/pkg/dotconf/encoding/json"
	"gitlab.com/art.frela/ckv/pkg/dotconf/encoding/toml"
	"gitlab.com/art.frela/ckv/pkg/dotconf/encoding/yaml"

	"github.com/tidwall/gjson"
	"github.com/xeipuuv/gojsonschema"
)

const (
	FormatYAML        string = "yaml"
	FormatYML         string = "yml"
	FormatJSON        string = "json"
	FormatTOML        string = "toml"
	envSeparator      string = "_"
	envParseSeparator string = "."
)

var (
	encoderRegistry  = enc.NewEncoderRegistry()
	decoderRegistry  = enc.NewDecoderRegistry()
	supportedFormats = []string{FormatJSON, FormatTOML, FormatYAML, FormatYML}
)

// nolint:errcheck
func init() {
	{
		codec := yaml.Codec{}
		encoderRegistry.RegisterEncoder(FormatYAML, codec)
		encoderRegistry.RegisterEncoder(FormatYML, codec)
		decoderRegistry.RegisterDecoder(FormatYAML, codec)
		decoderRegistry.RegisterDecoder(FormatYML, codec)
	}
	{
		codec := json.Codec{}
		encoderRegistry.RegisterEncoder(FormatJSON, codec)
		decoderRegistry.RegisterDecoder(FormatJSON, codec)
	}
	{
		codec := toml.Codec{}
		encoderRegistry.RegisterEncoder(FormatTOML, codec)
		decoderRegistry.RegisterDecoder(FormatTOML, codec)
	}
}

// DotConf wrapper for gjson.Result.
type DotConf struct {
	cfg           *gjson.Result
	schema        gojsonschema.JSONLoader
	refDataType   map[string]jsonType
	defaultConfig map[string]interface{}
	envPrefix     string
	pathPrefix    string
}

// New constructor for DotConf, sets private structure's fields.
func New(prefix string, defaults ...map[string]interface{}) *DotConf {
	ref := make(map[string]jsonType)
	dc := &DotConf{
		envPrefix:   envSanitizer(prefix),
		refDataType: ref,
	}

	if len(defaults) == 0 {
		return dc
	}

	for i, c := range defaults {
		if i == 0 {
			continue
		}
		mergeMAPs(defaults[0], c)
	}
	dc.defaultConfig = defaults[0]

	return dc
}

// NewWithSchema builder for DotConf with jsonSchema, sets unexporter structure's fields,
// jsonSchema requirements:
//
// references only in $ref = "#/definitions/*" format.
func NewWithSchema(prefix, schema string, defaults ...map[string]interface{}) *DotConf {
	schemaLoader := gojsonschema.NewStringLoader(schema)
	props, err := schemaLoader.LoadJSON()
	if err != nil {
		return nil
	}
	ref := make(map[string]jsonType)
	if m, ok := props.(map[string]interface{}); ok {
		ref = makeRef(m)
	}
	dc := &DotConf{
		envPrefix:   strings.ToUpper(prefix),
		schema:      schemaLoader,
		refDataType: ref,
	}

	if len(defaults) == 0 {
		return dc
	}

	for i, c := range defaults {
		if i == 0 {
			continue
		}
		mergeMAPs(defaults[0], c)
	}
	dc.defaultConfig = defaults[0]

	return dc
}

// SetPathPrefix set prefix for key search,
// if set prefix ru.spinosa., and Get(scope), then real search key will be "ru.spinosa.scope".
func (dc *DotConf) SetPathPrefix(prefix string) *DotConf {
	if len(prefix) == 0 {
		return dc
	}
	prefix = strings.TrimRight(prefix, ".") + "."
	dc.pathPrefix = prefix
	return dc
}

// Read reads files by passed paths (rootConfig, appConfigs...),
// merge them, equal keys will be replaced without inner structure comparison
// and fills DotConf fields.
func (dc *DotConf) Read(rootConfig string, appConfigs ...string) error {
	// init defaults
	//nolint:errcheck
	dc.readWithDefaults()
	//
	rm, err := dc.readConfigFileToMAP(rootConfig)
	if err != nil {
		return fmt.Errorf("read root config error: %w", err)
	}
	for i := range appConfigs {
		appM, errM := dc.readConfigFileToMAP(appConfigs[i])
		if errM != nil {
			return fmt.Errorf("read app config %s error: %w", appConfigs[i], errM)
		}
		mergeMAPs(rm, appM)
	}
	// merge readen config with envs
	dc.mergeManyMapEnv(rm, dc.envValues(), "")
	//
	dc.expandMap("", rm)
	bts, err := encoderRegistry.Encode(FormatJSON, rm)
	if err != nil {
		return fmt.Errorf("encode config to JSON error: %w", err)
	}
	res := gjson.ParseBytes(bts)
	dc.cfg = &res
	return nil
}

func (dc *DotConf) readWithDefaults() error {
	if len(dc.defaultConfig) == 0 {
		return nil
	}

	bts, err := encoderRegistry.Encode(FormatJSON, dc.defaultConfig)
	if err != nil {
		return fmt.Errorf("encode config to JSON error: %w", err)
	}
	res := gjson.ParseBytes(bts)
	dc.cfg = &res

	return nil
}

// DeepCopy creates new DotConf and copy all fields to it.
func (dc *DotConf) DeepCopy() *DotConf {
	if dc == nil || dc.cfg == nil {
		return nil
	}

	newCFG := &gjson.Result{
		Type:    dc.cfg.Type,
		Raw:     dc.cfg.Raw,
		Str:     dc.cfg.Str,
		Num:     dc.cfg.Num,
		Index:   dc.cfg.Index,
		Indexes: make([]int, len(dc.cfg.Indexes)),
	}

	copy(newCFG.Indexes, dc.cfg.Indexes)

	newDC := &DotConf{
		envPrefix:   dc.envPrefix,
		pathPrefix:  dc.pathPrefix,
		cfg:         newCFG,
		schema:      dc.schema,
		refDataType: make(map[string]jsonType),
	}

	for k, v := range dc.refDataType {
		newDC.refDataType[k] = v
	}
	return newDC
}

// Value returns one of these types:
//
//	map[string]interface{}, for JSON objects
//	[]interface{}, for JSON arrays
func (dc *DotConf) Value() interface{} {
	return dc.cfg.Value()
}

// Get extracts k-path value as common interface{}.
func (dc *DotConf) Get(k string) interface{} {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		return dc.cfg.Get(k).Value()
	}
	return envValue
}

// GetString extracts k-path value,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetString(k string) string {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		return os.ExpandEnv(dc.cfg.Get(k).String())
	}
	return os.ExpandEnv(envValue)
}

// GetStringAsIs extracts k-path value, without ${val} or $val replacement.
func (dc *DotConf) GetStringAsIs(k string) string {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		return dc.cfg.Get(k).String()
	}
	return envValue
}

// GetSliceString extracts k-path value as slice strings,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetSliceString(k string) []string {
	k = dc.pathPrefix + k
	src := dc.cfg.Get(k).Array()
	res := make([]string, 0, len(src))

	for _, x := range src {
		//nolint:gocritic
		switch v := x.Value().(type) {
		case string:
			res = append(res, os.ExpandEnv(v))
		}
	}

	return res
}

// GetBool extracts k-path value,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetBool(k string) bool {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			bv, _ := strconv.ParseBool(os.ExpandEnv(v))
			return bv
		default:
			return dc.cfg.Get(k).Bool()
		}
	}
	//nolint:errcheck
	bv, _ := strconv.ParseBool(os.ExpandEnv(envValue))
	return bv
}

// GetDuration extracts k-path value as time.Duration,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetDuration(k string) time.Duration {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		envValue = dc.cfg.Get(k).String()
	}
	//nolint:errcheck
	dur, _ := time.ParseDuration(os.ExpandEnv(envValue))
	return dur
}

const (
	x64 int = 64
)

// GetFloat64 extracts k-path value as float64,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetFloat64(k string) float64 {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			fv, _ := strconv.ParseFloat(os.ExpandEnv(v), x64)
			return fv
		default:
			return dc.cfg.Get(k).Float()
		}
	}
	//nolint:errcheck
	fv, _ := strconv.ParseFloat(envValue, x64)
	return fv
}

// GetInt extracts k-path value as int,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetInt(k string) int {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.Atoi(os.ExpandEnv(v))
			return vi
		default:
			return int(dc.cfg.Get(k).Int())
		}
	}
	//nolint:errcheck
	vi, _ := strconv.Atoi(os.ExpandEnv(envValue))
	return vi
}

const (
	dec int = 10
)

// GetInt32 extracts k-path value as int32,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetInt32(k string) int32 {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.ParseInt(os.ExpandEnv(v), dec, x64)
			return int32(vi)
		default:
			return int32(dc.cfg.Get(k).Int())
		}
	}
	//nolint:errcheck
	vi, _ := strconv.ParseInt(os.ExpandEnv(envValue), dec, x64)
	return int32(vi)
}

// GetInt64 extracts k-path value as int64,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetInt64(k string) int64 {
	k = dc.pathPrefix + k
	envValue := dc.env(k)
	if envValue == "" {
		switch v := dc.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.ParseInt(os.ExpandEnv(v), dec, x64)
			return vi
		default:
			return dc.cfg.Get(k).Int()
		}
	}
	//nolint:errcheck
	vi, _ := strconv.ParseInt(os.ExpandEnv(envValue), dec, x64)
	return vi
}

// GetIntSlice extracts k-path value as slice int,
// values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetIntSlice(k string) []int {
	k = dc.pathPrefix + k
	src := dc.cfg.Get(k).Array()
	res := make([]int, len(src))
	for i, x := range src {
		switch v := x.Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.Atoi(os.ExpandEnv(v))
			res[i] = vi
		default:
			res[i] = int(x.Int())
		}
	}
	return res
}

// GetStringMap extracts k-path value as map[string]interface{},
// values ${val} or $val will be replaced according to the values current environment variables.
//
// ATTENTION: if expected not string values with ${val} or $val replacement, it will be string!
//
// Example:
//
// math:
//
//	root: ${ROOT_PI} # export ROOT_PI=1.77245, expect 1.77245
//
// GetStringMap("math") = {"root":"1.77254"}.
func (dc *DotConf) GetStringMap(k string) map[string]interface{} {
	k = dc.pathPrefix + k
	src := dc.cfg.Get(k).Map()
	res := make(map[string]interface{}, len(src))
	for k, x := range src {
		switch v := x.Value().(type) {
		case string:
			res[k] = os.ExpandEnv(v)
		default:
			res[k] = x.Value()
		}
	}
	return res
}

// GetSliceInterface extracts k-path value as slice interface.
//
//nolint:gocritic
func (dc *DotConf) GetSliceInterface(k string) []interface{} {
	k = dc.pathPrefix + k
	src := dc.cfg.Get(k).Array()
	res := make([]interface{}, 0, len(src))
	for _, x := range src {
		// switch v := x.Value().(type) {
		// case interface{}:
		// 	res = append(res, v)
		// }
		res = append(res, x.Value())
	}
	return res
}

// GetStringMapString extracts k-path value as map[string]string,
// string values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetStringMapString(k string) map[string]string {
	k = dc.pathPrefix + k
	src := dc.cfg.Get(k).Map()
	res := make(map[string]string, len(src))
	for k, v := range src {
		res[k] = os.ExpandEnv(v.String())
	}
	return res
}

// GetSliceMap extracts k-path value as slice map[string]interface{},
// string values ${val} or $val will be replaced according to the values current environment variables.
func (dc *DotConf) GetSliceMap(k string) []map[string]interface{} {
	k = dc.pathPrefix + k
	src, ok := dc.cfg.Get(k).Value().([]interface{})
	if !ok {
		return nil
	}
	res := make([]map[string]interface{}, 0, len(src))
	for _, v := range src {
		if m, mok := v.(map[string]interface{}); mok {
			for km, xm := range m {
				switch vm := xm.(type) {
				case string:
					m[km] = os.ExpandEnv(vm)
				default:
					m[km] = xm
				}
			}
			res = append(res, m)
		}
	}
	return res
}

// helpers.

func (dc *DotConf) env(k string) string {
	envKey := dc.envPrefix + envSeparator + strings.ToUpper(strings.ReplaceAll(k, envParseSeparator, envSeparator))
	if dc.envPrefix == "" {
		envKey = strings.ToUpper(strings.ReplaceAll(k, envParseSeparator, envSeparator))
	}
	return os.Getenv(envKey)
}

func (dc *DotConf) readConfigFileToMAP(fn string) (map[string]interface{}, error) {
	f, err := os.Open(fn)
	if err != nil {
		return nil, fmt.Errorf("open file error: %w", err)
	}
	defer f.Close()
	data, err := io.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("read file error: %w", err)
	}
	_, _, ext := pathSeparator(fn)
	//
	err = formatValidate(ext)
	if err != nil {
		return nil, fmt.Errorf("formatValidate error: %w", err)
	}
	cfg := make(map[string]interface{})
	err = decoderRegistry.Decode(ext, data, &cfg)
	if err != nil {
		return nil, fmt.Errorf("decode file content with format %s error: %w", ext, err)
	}

	return cfg, nil
}

// expandMap checks map values, resolve and replace ${ENV_NAME} records.
func (dc *DotConf) expandMap(prefix string, src map[string]interface{}) {
	for k, value := range src {
		newPrefix := prefix + envParseSeparator + k
		if prefix == "" {
			newPrefix = k
		}
		switch v := value.(type) {
		case map[string]interface{}:
			dc.expandMap(newPrefix, v)
			src[k] = v
		case []interface{}:
			src[k] = dc.expandSlice(newPrefix, v)
		case string:
			expValue := os.ExpandEnv(v)
			if jst, ok := dc.refDataType[newPrefix]; ok {
				src[k] = jst.iface(expValue)
				continue
			}
			src[k] = expValue
		}
	}
}

// expandSlice iterates through slice and expand every item.
func (dc *DotConf) expandSlice(key string, src []interface{}) []interface{} {
	out := make([]interface{}, 0, len(src))
	for _, value := range src {
		switch v := value.(type) {
		case map[string]interface{}:
			dc.expandMap(key, v)
			out = append(out, v)
		case []interface{}:
			out = append(out, dc.expandSlice(key, v)...)
		case string:
			expValue := os.ExpandEnv(v)
			if jst, ok := dc.refDataType[key]; ok {
				out = append(out, jst.iface(expValue))
				continue
			}
			out = append(out, expValue)
		default:
			out = append(out, v)
		}
	}
	return out
}

// helpers

// pathSeparator...
func pathSeparator(in string) (dir, filename, ext string) {
	dir, file := path.Split(in)
	ext = strings.TrimLeft(filepath.Ext(file), ".")
	filename = strings.ReplaceAll(file, "."+ext, "")
	dir = path.Dir(dir)
	return
}

func formatValidate(format string) error {
	format = strings.ToLower(format)
	for i := range supportedFormats {
		if supportedFormats[i] == format {
			return nil
		}
	}
	return fmt.Errorf("unsupported format %s, allowed only %v", format, supportedFormats)
}

func mergeMAPs(m1, m2 map[string]interface{}) {
	for k2, v2 := range m2 {
		if v1, ok := m1[k2]; ok {
			switch v := v1.(type) {
			case map[string]interface{}:
				if vs, oks := v2.(map[string]interface{}); oks {
					mergeMAPs(v, vs)
					m1[k2] = v
				}
			default:
				m1[k2] = v2
			}
		} else {
			m1[k2] = v2
		}
	}
}

func envSanitizer(e string) string {
	e = strings.ToUpper(e)
	e = strings.TrimSpace(e)
	e = strings.Trim(e, "-")
	e = strings.TrimSpace(e)
	e = strings.Trim(e, "-")
	return strings.ReplaceAll(e, "-", "_")
}
