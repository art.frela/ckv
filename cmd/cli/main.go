package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"net"
	"os"
	"syscall"
	"time"

	"gitlab.com/art.frela/ckv/internal/controller/tcp"
	"gitlab.com/art.frela/ckv/pkg/datasize"
	"gitlab.com/art.frela/ckv/pkg/logging"
	"go.uber.org/zap"
)

func main() {
	address := flag.String("address", "localhost:3000", "Address of the ckv server")
	idleTimeout := flag.Duration("idle_timeout", time.Minute, "Idle timeout for connection")
	maxMessageSizeStr := flag.String("max_message_size", "4KB", "Max message size for connection")
	flag.Parse()

	logger := logging.DefaultLogger()

	maxMessageSize, err := datasize.Parse(*maxMessageSizeStr)
	if err != nil {
		logger.Fatal("failed to parse max message size", zap.Error(err))
	}

	reader := bufio.NewReader(os.Stdin)

	connection, err := net.Dial("tcp", *address)
	if err != nil {
		logger.Fatalf("failed to dial %s: %v", *address, err)
	}

	client, err := tcp.NewClient(connection, maxMessageSize, *idleTimeout)
	if err != nil {
		logger.Fatal("failed to connect with server", zap.Error(err))
	}

	for {
		fmt.Print("[ckv] > ")
		request, err := reader.ReadString('\n')
		if err != nil {
			if errors.Is(err, syscall.EPIPE) {
				logger.Fatal("connection was closed", zap.Error(err))
			}

			logger.Error("failed to read user query", zap.Error(err))
		}

		response, err := client.Send([]byte(request))
		if err != nil {
			if errors.Is(err, syscall.EPIPE) {
				logger.Fatal("connection was closed", zap.Error(err))
			}

			logger.Error("failed to send query", zap.Error(err))
		}

		fmt.Println(string(response))
	}
}
