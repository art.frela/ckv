package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/art.frela/ckv/internal/app"
	"gitlab.com/art.frela/ckv/internal/config"
	"gitlab.com/art.frela/ckv/pkg/logging"
)

const (
	envPrefix     = "ckv"
	envConfig     = "CKV_CONFIG_FILE"
	defConfigfile = "./configs/ckv.yaml"
)

func main() {
	cfgFile := defConfigfile
	if eCFG := os.Getenv(envConfig); len(eCFG) > 1 {
		cfgFile = eCFG
	}

	cfg, err := config.Read(envPrefix, cfgFile)
	if err != nil {
		log.Fatalf("read config: %s", err)
	}

	logger := logging.NewLogger(
		cfg.GetStringWithDefault("logger.level", config.DefLoggerLevel),
		cfg.GetBool("logger.development"),
	)

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	ctx = logging.WithLogger(ctx, logger)

	srv, err := app.New(logger, cfg)
	if err != nil {
		logger.Errorf("app new: %s", err)
		return
	}

	if err := srv.Run(ctx); err != nil {
		logger.Errorf("run server: %s", err)
		return
	}

	<-ctx.Done()

	srv.Stop()
}
